/* Anduna PBEM Host 5.7 Copyright (C) 1999-2001 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

#include "atlantis.h"

/* Liste von Regionennamen, wird nur hier eingebaut. */
#include "names.inc"
 
#define INIT_PASSWORD_LENGTH     5

/* Eine Insel pro 9x9 Feld.  Das erste Feld von (0,0) bis (8,8).  */
#define BLOCKSIZE       9
/* Wieviele Inseln gibt es ? Wird in Sphericalx() und SphericalDistance benutzt */
#define UPPERLEFT	-4
#define LOWERIGHT	5

/* Entweder eine grosse Insel (Chance 2/3) mit 31 bis 40 Felder oder eine
   kleine Insel (Chance 1/3) mit 11 bis 39 Feldern. */
#define ISLANDSIZE      ((rand()%3)?(31+rand()%10):(11+rand()%20))

/* Ozean und Grasland wird automatisch generiert (ein Ozean, darin
   eine Insel aus Grasland). Jedes Seed generiert ein Feld des
   entsprechenden Terrains, und max. 3 angrenzende Felder. Details in
   der Funktion seed () und transmute () in REGIONS.C.  Die Anzahl
   Inseln auf der Welt (falls sie sphaerisch ist) bestimmt man in
   spericalx () und sphericaly ().  Dabei climate (/) nicht vergessen,
   dort wird bestimmt, zu welcher y Koordinate welches Klima gehoert.
   Das Klima bestimmt das Verhaeltniss der einzelnen Terrains auf der
   Insel.  */

/* ------------------------------------------------------------- */

enum
  {
    C_PACIFIC,
    C_TROPIC,
    C_DRY,
    C_TEMPERATE,
    C_COOL,
    C_ARCTIC,
    MAXCLIMATES,
  };

/* OCEAN, PLAIN, FOREST, SWAMP, DESERT, HIGHLAND, MOUNTAIN, GLACIER,
   MAXTERRAINS */

char maxseeds[MAXCLIMATES][MAXTERRAINS] =
{
  {3, 0, 4, 0, 0, 1, 0, 0,},
  {0, 0, 6, 4, 0, 2, 2, 0,},
  {0, 0, 1, 0, 6, 2, 2, 0,},
  {0, 0, 3, 2, 0, 2, 3, 1,},
  {0, 0, 3, 3, 0, 1, 4, 5,},
  {0, 0, 0, 2, 0, 0, 5,12,},
};

int
climate (int y)
{

  /* Abfolge:    y/BLOCKSIZE    +2  abs %10 -5 abs

        TEMPERATE        -4     -2   2   2  -3  3
        COOL             -3     -1   1   1  -4  4
      > ARCTIC           -2      0   0   0  -5  5
        COOL             -1      1   1   1  -4  4
        TEMPERATE  (0,0)  0      2   2   2  -3  3
        DRY               1      3   3   3  -2  2
        TROPIC            2      4   4   4  -1  1
      < PACIFIC           3      5   5   5   0  0
        TROPIC            4      6   6   6   1  1 
        DRY               5      7   7   7   2  2 */

  return abs( abs( (y/BLOCKSIZE) + 2) % 10 - 5);
}

int random_monster[MAXTYPES] =
{
  0,  /* U_MAN */
  10, /* U_UNDEAD */
  0,  /* U_ILLUSION */
  30, /* U_FIREDRAGON */
  20, /* U_DRAGON */
  10, /* U_WYRM */
  30, /* U_GUARDS */
  0,  /* U_SERPENT */  /*die gibt's erstmal nicht zufaellig, weil im Meer*/
  0,  /* U_THIEF */
};

/*Fuer spawn_monsters*/
int random_land_monster[MAXTYPES] =
{
 0, /*wir wollen nur MONSTER*/
 30,
 0, /*wir wollen nur MONSTER*/
 25,
 10,
 05,
 0, /*wir wollen nur MONSTER*/
 0, /*ozean only*/
 30,
};

#define MONSTERSEEDBASE 	10000 /*Chancen in Zehntel Promille*/
#define MONSTERSEEDLAND   	100   /* 1% auf Land */
#define MONSTERSEEDOCEAN  	10    /* .03% im Ozean */
#define MONSTERSEEDMAXNUM 	100

int monsterimpact[MAXTYPES] =
{
 1, 
 1,
 1, 
 33, /* max. 3 Feuerdrachen */
 100,/* max. 1 Drachen */
 100,
 1, 
 25, /* max. 4 Serpents */
 10,  /* max. 10 Thieves */
};


/* Transformiert die Koordinaten in Koordinaten auf einer runden Welt.
   Soll Atlantis nicht rund sein, dann kann man hier einfach ein
   inline `return coord;' machen.  Die Klimazonen Verteilung im
   Origignal GA hatte Grenzen von -4 bis 5, dh. die Ecken lagen bei
   (-36,-36) und (53,53).  In diesem Fall sind lb=-36 (-4x9) und ub=54
   ((5+1)x9).  */
int
sphericalx (int coord)
{
  int lb=UPPERLEFT * BLOCKSIZE;          /* lower boundary.  */
  int ub=(LOWERIGHT +1) * BLOCKSIZE;     /* upper boundary.  */

  if (coord < lb) 
    /* coord=-37 -> (54)-[(-36)-(-37)]=53.  */
    /* Falls lb=0 und ub=9, coord=-1 -> (9)-[(0)-(-1)]=8.  */
      coord = ub - (lb - coord);
  else if (coord >= ub) 
     /* coord=54 -> (-36)+[(54)-(54)]=-36.  */
     /* coord=55 -> (-36)+[(55)-(54)]=-35.  */
     /* Falls lb=0 und ub=9, coord=9 -> (0)+[(9)-(9)]=0.  */
     /* Falls lb=0 und ub=9, coord=10 -> (0)+[(10)-(9)]=1.  */
      coord = lb + (coord - ub);
  return coord;
}

int
sphericaly (int coord)
{
  /* Die Welt ist torsoidal (dh. wenn man oben ueber den Rand faehrt
     kommt man unten wieder heraus, dasselbe mit links und rechts),
     und die gesamte Karte ist quadratisch, also sind sphericalx ()
     und sphericaly () im Moment noch gleich.  */
  return sphericalx (coord);
}

int sphericaldistance(int x1, int y1,int x2,int y2)
{
  int dx,dy,worldsize,distance;
  worldsize = BLOCKSIZE*(LOWERIGHT+1-UPPERLEFT); /* 90 im Moment */
  dx = abs(x1-x2);
  dy = abs(y1-y2);
  distance= min(dx,(worldsize-dx))+min(dy,(worldsize-dy));
  assert(distance>=0);
  return distance;
}

/* ------------------------------------------------------------- */

unit *
inputunit()
{
  unit *u;
  int id;
  printf ("Nummer der Einheit: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  u = findunitglobal (id);
  if (!u)
    {
      puts ("Diese Einheit existiert nicht.");
      return 0;
    }
  return u;
}

building *
inputbuilding()
{
  building *b;
  int id;
  printf ("Nummer des Gebaeudes: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  b = findbuilding (id);
  if (!b)
    {
      puts ("Dieses Gebaeude existiert nicht.");
      return 0;
    }
  return b;
}

ship *
inputship()
{
  ship *s;
  int id;
  printf ("Nummer des Schiffs: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  s = findship(id);
  if (!s)
    {
      puts ("Dieses Schiff existiert nicht.");
      return 0;
    }
  return s;
}

region *
inputregion (void)
{
  region *r;
  int x, y;

  printf ("X? ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  x = atoi (buf);

  printf ("Y? ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  y = atoi (buf);

  r = findregion (x, y);

  if (!r)
    {
      puts ("Diese Region existiert nicht.");
      return 0;
    }

  return r;
}

/* ------------------------------------------------------------- */

void
createmonsters (void)
{
  faction *f;

  if (findfaction (0))
    {
      puts ("Die Monster Partei gibt es schon.");
      return;
    }

  f = cmalloc (sizeof (faction));
  memset (f, 0, sizeof (faction));

  /* alles ist auf null gesetzt, ausser dem folgenden. achtung - partei no 0
     muss keine orders einreichen! */

  mnstrcpy (&f->name, "Monster", NAMESIZE);
  f->alive = 1;
  f->options = pow (2, O_REPORT);
  addlist (&factions, f);
}

/* ------------------------------------------------------------- */

char 
random_letter (void)
{
  return 'a' + rand () % 26;
}

void 
generate_password (faction *f)
{
  int i;
  for (i = 0; i != INIT_PASSWORD_LENGTH; i++)
    buf[i] = random_letter ();
  buf[i] = 0;
  mnstrcpy (&f->passw, buf, INIT_PASSWORD_LENGTH);
}

void
addaplayerat (region * r)
{
  faction *f;
  unit *u;

  f = cmalloc (sizeof (faction));
  memset (f, 0, sizeof (faction));

  /* Die Adresse steht in buf (vgl. fkt. addplayers) */

  mnstrcpy (&f->addr, buf, DISPLAYSIZE);

  /* Passwort auf ein Zufallscode setzen, letzter Befehl diese Runde
     und am leben */

  generate_password (f);
  f->lastorders = turn;
  f->alive = 1;
  f->newbie = 1;
  f->options = pow (2, O_REPORT) + pow (2, O_STATISTICS)
    + pow (2, O_ZINE) + pow (2, O_COMMENTS) + pow (2,O_MISC);

  /* Setze die Nummer und den Namen fuer die Partei auf die erste freie
     Nummer */

  do
    f->no++;
  while (findfaction (f->no));

  sprintf (buf, "Nummer %d", f->no);
  mnstrcpy (&f->name, buf, NAMESIZE);
  addlist (&factions, f);

  /* erzeuge zugehoerige Einheit */

  u = createunit (r);
  u->number = 1;
  u->money = STARTMONEY+turn*STARTMONEY_INCREMENT;
  u->faction = f;
}

void
addplayers (void)
{
  region *r;
  faction *f;
  unit *u;
  int n;

  r = inputregion ();

  /* Die Spieler erscheinen auf dieser Stelle im Spiel */
  /* falls keine Region eingegeben wurde */
  if (!r)
    return;

  /* falls die Region ein Ozean ist */
  if (r->terrain == T_OCEAN)
    {
      puts ("Diese Gegend liegt unter dem Meeresspiegel!");
      return;
    }

  printf ("Datei mit den Spieleradressen? ");
  afgets (buf, MAXLINE);

  /* wenn buf nirgendswohin zeigt, wurde keine Datei angegeben. */
  if (!buf[0])
    {
      /* und deswegen fragen wir nun einfach direkt nach dem Namen. */
      printf ("OK. Name bzw. Adresse des Spielers? ");
      afgets (buf, MAXLINE);

      /* wieder ohne Antwort, beenden wir die Funktion */
      if (!buf[0])
        return;
      else
        addaplayerat (r);
    }
  else
    {
      if (!cfopen (buf, "r"))
        return;
      for (;;)
        {
          getbuf ();

          if (buf[0] == 0 || buf[0] == EOF)
            {
              fclose (F);
              break;
            }

          addaplayerat (r);
        }
      	puts ("Es empfiehlt sich, hier noch Wachen aufzustellen!");
//      addunit (U_GUARDS);
      	printf ("Anzahl: ");
	afgets (buf, MAXLINE);
  	n = atoi (buf);
  	if (!n) return;

  /* partei - auch fuer die funktionen (fakultativ vorher schon) vorher schon
     pruefen, ob es die Monster (0) schon gibt, wenn nicht, erschaffen */

  	if (!(f = findfaction (0)))
    	{
      	  createmonsters ();
      	  if (!(f = findfaction (0)))
            puts ("* Fehler! Die Monsterpartei konnte nicht erzeugt "
              "werden");
    	}
        u = make_guards_unit (r, f, n);

    }
}

/* ------------------------------------------------------------- */

void
connecttothis (region * r, int x, int y, int from, int to)
{
  region *r2;

  r2 = findregion (x, y);

  if (r2)
    {
      r->connect[from] = r2;
      r2->connect[to] = r;
    }
}

void
connectregions (void)
{
  region *r;
  int i;

  for (r = regions; r; r = r->next)
    for (i = 0; i != MAXDIRECTIONS; i++)
      r->connect[i]=0;
 
  for (r = regions; r; r = r->next)
    for (i = 0; i != MAXDIRECTIONS; i++)
      if (!r->connect[i])
        connecttothis (r, sphericalx (r->x + delta_x[i]), 
		       sphericaly (r->y + delta_y[i]),
                       i, back[i]);
}

/* ------------------------------------------------------------- */

void
listnames (void)
{
  region *r;
  int i;

  puts ("Die Liste der benannten Regionen ist:");

  i = 0;
  for (r = regions; r; r = r->next)
    if (r->terrain != T_OCEAN)
      {
        printf ("%s,", r->name ? r->name : "Das unbenannte Land");
        i += strlen (r->name) + 1;

        while (i % 15)
          {
            printf (" ");
            i++;
          }

        if (i > 60)
          {
            printf ("\n");
            i = 0;
          }
      }
  printf ("\n");
}

int
regionnameinuse (char *s)
{
  region *r;

  for (r = regions; r; r = r->next)

    /* hier darf es case-insensitive sein, da die Namen nie vom Benutzer
       eingegeben werden. */

    if (r->name && !strcmp (r->name, s))
      return 1;

  return 0;
}

void
nameregion (region * r)
{
  /* Zaehlt die benutzten Namen */

  int i, n;
  region *r2;

  n = 0;
  for (r2 = regions; r2; r2 = r2->next)
    if (r2->name)
      n++;

  /* Vergibt einen Namen */

  i = rand () % (sizeof regionnames / sizeof (char *));

  /* Falls es noch genug unbenutzte Namen gibt, wird solange ein weiterer
     genommen, bis einer der unbenutzten gefunden wurde. */

  if (n < 0.8 * sizeof regionnames / sizeof (char *))
    while (regionnameinuse (regionnames[i]))
        i = rand () % (sizeof regionnames / sizeof (char *));

  mnstrcpy (&r->name, regionnames[i], NAMESIZE);
}

int
initresources (int terrain)
{
  /* noch nicht implementiert */
  if (terrain == T_OCEAN) return 0;
  return 0;
}

void
terraform (region * r, int terrain)
{
  int i;
  /* defaults: */

  r->terrain = terrain;
  r->trees = 0;
  r->horses = 0;
  r->peasants = 0;
  r->money = 0;
  r->buekaz = INIT_BUEKAZ;
  r->touched = 0;
  r->specialresource = initresources(terrain);

  if (terrain != T_OCEAN)
    {
      /* jede r ausser ozean hat einen namen. */

      nameregion (r);

      for(i=0;i!=MAXHERBS;i++) {
        r->herbs[i] = rand() % INIT_HERBS;
      }

      switch (terrain)
        {
        case T_PLAIN:

          /* 0-200 pferde in Ebenen */

          r->horses = (rand () % (production[terrain] / 5));
          break;

        case T_FOREST:

          /* waelder per se gibt es nicht, das ist eine Ebene mit vielen
             baeumen. 600-900 baueme max. */

          r->terrain = T_PLAIN;
          r->trees = production[T_PLAIN] * (60 + rand () % 30) / 100;
          break;

        }

      /* 50-80% der maximalen Anzahl an Bauern, in einer Ebene also
         5000-8000. */

        r->peasants = MAXPEASANTS_PER_AREA * production[terrain] *
                      (50 + rand () % 30) / 100;
    }
}

/* ------------------------------------------------------------- */

char newblock[BLOCKSIZE][BLOCKSIZE];

void
transmute (int from, int to, int n, int count)
{
  int i, x, y;

  /* insgesamt werden also n felder geaendert */

  do
    {
      i = 0;

      do
        {
          x = rand () % BLOCKSIZE;
          y = rand () % BLOCKSIZE;
          i += count;
        }
      while (i <= 10 && !(newblock[x][y] == from &&
                          ((x != 0 && newblock[x - 1][y] == to) ||
                         (x != BLOCKSIZE - 1 && newblock[x + 1][y] == to) ||
                           (y != 0 && newblock[x][y - 1] == to) ||
                        (y != BLOCKSIZE - 1 && newblock[x][y + 1] == to))));

      /* wenn von seed aus gerufen: zehn versuche, um ein feld zu finden,
         welches =from ist, und neben einem feld liegt, das =to ist. wenn von
         makeblock aus gerufen, hat man unendlich lange zeit, um danach zu
         suchen. aufpassen, dass ISLANDSIZE n nie groesser als BLOCKSIZE^2
         wird! */

      /* wurde 10 mal erfolglos probiert, ein seed zu machen, brechen wir ab */

      if (i > 10)
        break;

      /* ansonsten aendern wir ein feld */

      newblock[x][y] = to;
    }
  while (--n);
}

void
seed (int to, int n)
{
  int x, y, i = 0;

  /* hier setzen wir ein T_PLAIN feld =to (20 Versuche), und rufen
     nachher transmute auf, um neben eines der =to felder n weitere =to
     felder zu setzen. Wird hier also ein wald gesaeht, wird ein neues
     wald-feld gesezt, und entweder an diesem oder an anderen werden n
     neue waldfelder angehaengt. um diese n zu setzten, hat man nur 10
     versuche (4. parameter 1) */

  do
    {
      x = rand () % BLOCKSIZE;
      y = rand () % BLOCKSIZE;
      i++;
    }
  while (newblock[x][y] != T_PLAIN && i < 20);

  newblock[x][y] = to;
  transmute (T_PLAIN, to, n, 1);
}

int
blockcoord (int x)
{
  return (x / BLOCKSIZE) * BLOCKSIZE;
}

void
upper_left (int *x1, int *y1)
{
  int x = *x1, y = *y1;

  /* negative Koordinaten: Runterzaehlen */

  if (x < 0)
    while (x != blockcoord (x))
      x--;

  if (y < 0)
    while (y != blockcoord (y))
      y--;

  /* positive Koordinaten: Runden.  Negative Koord. stimmen schon.  */

  *x1 = blockcoord (x);
  *y1 = blockcoord (y);
}

void
makeblock (int x1, int y1)
{
  int i, j, x, y, p1, p2, local_climate;
  region *r;

  /* Zuerst werden die Koordinaten auf eine Sphaere projeziert.  */
  x1 = sphericalx (x1);
  y1 = sphericaly (y1);

  local_climate = climate (y1);

  /* Links-Oben Koordinate des Quadrates, in dem sich (x1,y1)
     befinden.  */
  upper_left (&x1, &y1);

  /* ein Kontinent wird erzeugt und in newblock abgespeichert die Mitte von
     newblock wird zu Weideland. Diese Insel wird vergroessert, und dann
     werden Terrains "gesaeht" */
  memset (newblock, T_OCEAN, sizeof newblock);
  newblock[BLOCKSIZE / 2][BLOCKSIZE / 2] = T_PLAIN;
  transmute (T_OCEAN, T_PLAIN, ISLANDSIZE, 0);

  for (i = 0; i != MAXTERRAINS; i++)
    for (j = 0; j != maxseeds[local_climate][i]; j++)
      seed (i, 3);

  /* newblock wird innerhalb der Rahmen in die Karte kopiert, die Landstriche
     werden benannt und bevoelkert, und die produkte p1 und p2 des kontinentes
     werden gesetzt. */
  p1 = rand () % MAXLUXURIES;
  do
    p2 = rand () % MAXLUXURIES;
  while (p2 == p1);

  for (x = 0; x != BLOCKSIZE; x++)
    for (y = 0; y != BLOCKSIZE; y++)
      {
        r = cmalloc (sizeof (region));
        memset (r, 0, sizeof (region));

        r->x = sphericalx (x1 + x);
        r->y = sphericaly (y1 + y);

        terraform (r, newblock[x][y]);

        for (i = 0; i != MAXLUXURIES; i++)
          r->demand[i] = MINDEMAND + rand () % 500;
	r->produced_good = (rand () & 1) ? p1 : p2;
        r->demand[r->produced_good] = MINDEMAND;

        addlist (&regions, r);
      }

  connectregions ();
}

/* Drachen werden ausgesetzt, wenn die Region automatisch (durch
   einreisende Spieler) erzeugt wurde.  Dies geschieht bei Bewegungen.
   Wenn der Spielleiter Inseln erzeugt, werden Drachen *nicht*
   automatisch ausgesetzt.  */
void 
seed_monsters (int x1, int y1) 
{
  region *r;
  int i, m, x, y;

  /* Zuerst werden die Koordinaten auf eine Sphaere projeziert.  */
  x1 = sphericalx (x1);
  y1 = sphericaly (y1);

  upper_left (&x1, &y1);

  for (i=0; i != MAXTYPES; i++)
    {
      if (rand () % 100 < random_monster[i])
	{
	  do
	    {
	      x = rand () % BLOCKSIZE;
	      y = rand () % BLOCKSIZE;
	      r = findregion (x1 + x, y1 + y);
	    }
	  while (r->terrain == T_OCEAN);
	  printf ("   %s in %s ausgesetzt.\n", 
		  strings[typenames[1][i]][0], regionid(r));
	  switch (i)
	    {
	    case U_UNDEAD:
	      make_undead_unit (r, findfaction (0), lovar (20));
	      break;
	    case U_FIREDRAGON:
	      make_firedragon_unit (r, findfaction (0), 1);
	      break;
	    case U_DRAGON:
	      make_dragon_unit (r, findfaction (0), 1);
	      break;
	    case U_WYRM:
	      make_wyrm_unit (r, findfaction (0), 1);
	      break;
	    case U_GUARDS:
	      m = lovar (20);
	      make_guarded_tower_unit (r, findfaction (0), m, m);
	      break;
	    case U_SERPENT:
	      break;
	    case U_THIEF:
	      break;
	    default:
	      assert (0);
	    }
	  break;
	}
    }
}

/* ------------------------------------------------------------- */

char
factionsymbols (region * r)
{
  faction *f;
  unit *u;
  int i = 0;
  int onlyguards = 1;

  for (f = factions; f; f = f->next)
    for (u = r->units; u; u = u->next)
      {
      	if (u->type != U_GUARDS) onlyguards = 0;
      	if (u->faction == f)
        {
          i++;
          break;
        }
      }
  assert (i);

  if (onlyguards)
    return '#';
  if (i > 9)
    return 'x';
  else
    return '1' - 1 + i;
}

char
armedsymbols (region * r)
{
  unit *u;
  int i = 0;

  for (u = r->units; u; u = u->next)
    if (armedmen (u))
      return 'X';
    else
      i += u->number;
  assert (i);

  if (i > 9)
    return 'x';
  else
    return '1' - 1 + i;
}

void
output_map (int mode)
{
  int x, y, p, MaxZiffern,minx, miny, maxx, maxy;
  region *r;

  minx = INT_MAX;
  maxx = INT_MIN;
  miny = INT_MAX;
  maxy = INT_MIN;

  for (r = regions; r; r = r->next)
    {
      minx = min (minx, r->x);
      maxx = max (maxx, r->x);
      miny = min (miny, r->y);
      maxy = max (maxy, r->y);
    }

  fprintf (F, "Koordinaten von (%d,%d) bis (%d,%d):\n\n",
           minx, miny, maxx, maxy);

// Fuer eine Nummerierung der Karte: (TP 13.7.99)
// Logarithmus (dekadisch) bestimmt Anzahl der Stellen

  MaxZiffern = max( log10(maxx) , minx==0?0:log10(-minx) ); // minx ist immer <= 0

// Erst das Vorzeichen...

  fputs("      ",F);
  for (x=minx;x<=maxx;x++)
    {
      fputc((x<0?'-':' '),F);
      fputc(' ',F);
    }
  fputc ('\n', F);

// ... und dann die Zahlen, schoen Ziffer fuer Ziffer.
//     Schleife ueber alle Stellen und dann je Zeile die Zahl /(10^entspr. Stelle) ausgeben

  MaxZiffern = max( log10(maxx) , minx==0?0:log10(-minx) ); // minx ist immer <= 0
  p=(int)pow(10,MaxZiffern);
  while (p>0)
    {
      fputs("      ",F);
      for (x=minx;x<=maxx;x++)
        {
          char c='1'-1+ (abs(x)/p) % (10*p) ;
          fputc(c,F);
          fputc(' ',F);
        }
      p /= 10;
      fputc ('\n', F);
    }

  for (y = miny; y <= maxy; y++)
    {
      memset (buf, ' ', sizeof buf);
      buf[maxx - minx + 1] = 0;         

      for (r = regions; r; r = r->next)
        if (r->y == y)
          {
            if (r->units && mode == M_FACTIONS)
              buf[r->x - minx] = factionsymbols (r);
            else if (r->units && mode == M_UNARMED)
              buf[r->x - minx] = armedsymbols (r);
            else                /* default: terrain */
              buf[r->x - minx] = terrainsymbols[mainterrain (r)];
          }

      fprintf (F,"%5d",y);
      for (x = 0; buf[x]; x++)
        {
          if (y == 0 && x == -minx)
            fputc ('(', F);
          else if (y == 0 && x == -minx + 1)
            fputc (')', F);
          else
            fputc (' ', F);

          fputc (buf[x], F);
        }

      fputc ('\n', F);
    }

  fputs ("\nLegende:\n\n", F);
  switch (mode)
    {
    case M_TERRAIN:
      for (y = 0; y != MAXTERRAINS; y++)
        fprintf (F, "%c - %s\n", terrainsymbols[y], strings[terrainnames[y]][DEFAULT_LANGUAGE]);
      break;

    case M_FACTIONS:
      for (y = 0; y != MAXTERRAINS; y++)
        fprintf (F, "%c - %s\n", terrainsymbols[y], strings[terrainnames[y]][DEFAULT_LANGUAGE]);
      fputs ("x - mehr als 9 Parteien\n", F);
      fputs ("1-9 - Anzahl Parteien\n", F);
      break;

    case M_UNARMED:
      for (y = 0; y != MAXTERRAINS; y++)
        fprintf (F, "%c - %s\n", terrainsymbols[y], strings[terrainnames[y]][DEFAULT_LANGUAGE]);
      fputs ("X - mindestens eine bewaffnete Person\n", F);
      fputs ("x - mehr als 9 Personen\n", F);
      fputs ("1-9 - Anzahl unbewaffneter Personen\n", F);
      break;

    }
}

void
showmap (int mode)
{
  F = stdout;
  output_map (mode);
}

void
writemap (int mode)
{
  if (!cfopen ("karte", "w"))
    return;
  puts ("Schreibe Karte (karte)...");
  output_map (mode);
  fclose (F);
}

/* ------------------------------------------------------------- */

void
createcontinent (void)
{
  int x, y;

  /* Kontinente werden automatisch generiert! Dies ist nur so zum Testen
     gedacht. Da man die X und Y Koordinaten braucht, kann man leider nicht
     inputregion () verwenden. */

  printf ("X? ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return;
  x = atoi (buf);

  printf ("Y? ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return;
  y = atoi (buf);

  if (!findregion (x, y))
    {
      makeblock (x, y);
      showmap (M_TERRAIN);
    }
  else
    puts ("Dort gibt es schon Regionen.");
}

/* ------------------------------------------------------------- */

void
longgets (char *s)
{
  int l;
  char q = 0;

  /* so lange gets, bis nichts mehr eingegeben wird, oder die moegliche
     laenge ueberschritten wurde. */

  printf ("$ ");
  afgets (s, strlen(s));
  while (!q && (l = strlen (s)) < DISPLAYSIZE)
    {
      s[l] = ' ';
      printf ("$ ");
      afgets (s + l + 1, DISPLAYSIZE-l-1);
      q = !((s + l + 1)[0]);
    }

  /* wurde verlaengert und mit einer leerzeile abgeschlossen, so wurde
     unguterweise ein ' ' angehaengt */

  if (q)
    s[strlen (s) - 1] = 0;

}

/* ------------------------------------------------------------- */

void
changeterrain (void)
{
  region *r;
  int i;

  r = inputregion ();
  if (!r)
    return;

  puts ("Terrain?");
  for (i = 0; i != MAXTERRAINS; i++)
    printf ("%d - %s\n", i, strings[terrainnames[i]][DEFAULT_LANGUAGE]);
  afgets (buf, MAXLINE);
  if (buf[0])
    {
      i = atoi (buf);
      if (i >= 0 && i < MAXTERRAINS)
        terraform (r, i);
    }
  F = stdout;
  describe (r, findfaction(0));
}

/* ------------------------------------------------------------- */

void
addbuilding (void)
{
  region *r;
  building *b;

  r = inputregion ();
  if (!r)
    return;
  if (r->terrain == T_OCEAN) 
    printf ("Im Ozean haben Burgen nichts zu suchen.");
    afgets (buf, MAXLINE);
    return;  /* Wir wollen doch wohl nicht im Ozean bauen ? */
  b = cmalloc (sizeof (building));
  memset (b, 0, sizeof (building));

  do
    b->no++;
  while (findbuilding (b->no));

  printf ("Groesse: ");
  afgets (buf, MAXLINE);
  b->size = max (atoi (buf), 1);

  printf ("Name: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    sprintf (buf, "Burg %d", b->no);
  mnstrcpy (&b->name, buf, NAMESIZE);

  printf ("Beschreibung: ");
  longgets (buf);
  if (buf[0])
    mnstrcpy (&b->display, buf, DISPLAYSIZE);

  b->kind=BD_CASTLE;
  
  addlist (&r->buildings, b);

  printf ("\n%s, Groesse %d, %s", buildingid (b), b->size,
          buildingtype (b));
  if (b->display)
    printf ("; %s", b->display);

  putc ('\n', stdout);
}

/* ------------------------------------------------------------- */

/* Der type kann -1 sein, dann wird neu nach dem type gefragt. */
void
addunit (int type)
{
  region *r;
  unit *u;
  faction *f;
  int i, n;

  F = stdout;

  r = inputregion ();
  if (!r)
    return;

  /* typ */

  if (type == -1)
    {
      puts ("Typ?");
      for (i = 0; i != MAXTYPES; i++)
	printf ("%d - %s\n", i, strings[typenames[0][i]][0]);
      afgets (buf, MAXLINE);
      if (buf[0])
	{
	  i = atoi (buf);
	  if (i >= 0 && i < MAXTYPES)
	    type = i;
	}
    }
  if (type == -1)
    {
      puts ("Diesen Typ gibt es nicht.");
      return;
    }
  
  /* anzahl - vor make_illsionary_unit, damit die Anzahl erschaffener
     Gegenstaende pro Unit auch stimmt */
  
  printf ("Anzahl: ");
  afgets (buf, MAXLINE);
  n = atoi (buf);
  if (!n)
    return;

  /* partei - auch fuer die funktionen (fakultativ vorher schon) vorher schon
     pruefen, ob es die Monster (0) schon gibt, wenn nicht, erschaffen */

  if (!(f = findfaction (0)))
    {
      createmonsters ();
      if (!(f = findfaction (0)))
        puts ("* Fehler! Die Monsterpartei konnte nicht erzeugt "
              "werden");
    }

  printf ("Partei: ");
  afgets (buf, MAXLINE);
  f = findfaction (atoi (buf));
  if (!f)
    {
      puts ("Diese Partei gibt es nicht.");
      return;
    }

  /* erschaffe unit und setze ein paar defaults */

  switch (type)
    {
    case U_FIREDRAGON:
      u = make_firedragon_unit (r, f, n);
      break;
    case U_DRAGON:
      u = make_dragon_unit (r, f, n);
      break;
    case U_WYRM:
      u = make_wyrm_unit (r, f, n);
      break;
    case U_UNDEAD:
      u = make_undead_unit (r, f, n);
      break;
    case U_ILLUSION:
      u = make_illsionary_unit (r, f, n);
      break;
    case U_GUARDS:
      u = make_guards_unit (r, f, n);
      break;
    case U_SERPENT:
      u = make_serpent_unit (r, f, n);
      break;
    case U_THIEF:
      u = make_thief_unit (r, f, n);
      break;
    default:
      u = createunit (r);
      u->type = type;
      u->number = n;
      u->money = income[u->type] * u->number;
      u->faction = f;
      break;
    }

  puts ("Bis jetzt:");
  rpunit (f, r, u, 4);

  printf ("Neuer Name:\n"
	  "$ ");
  afgets (buf, MAXLINE);
  if (buf[0])
    mnstrcpy (&u->name, buf, NAMESIZE);

  puts ("Neue Beschreibung: ");
  longgets (buf);
  if (buf[0])
    mnstrcpy (&u->display, buf, DISPLAYSIZE);

  rpunit (f, r, u, 4);

  printf ("Akzeptieren? ");
  afgets (buf, MAXLINE);
  if (!(buf[0] == 'j' ||
        buf[0] == 'J' ||
        buf[0] == 'y' ||
        buf[0] == 'Y'))
    u->number = 0;              /* automatisch geloescht */
  return;

}

/* ------------------------------------------------------------- */

void
writeregioninfo (region * r, FILE * F)
{
  unit *u;
  building *b;
  ship *sh;
  faction *f;
  int d;
  int i;

  fprintf (F, "%s\n", regionid (r));
  fprintf (F, "%s, %d Baeume, %d Bauern, $%d, %d Pferde\n",
           strings[terrainnames[mainterrain (r)]][DEFAULT_LANGUAGE], r->trees, r->peasants, r->money,
           r->horses);

  for (i = 0; i < MAXLUXURIES; i+=2)
    {
      if (i < MAXLUXURIES-1)
	fprintf (F, "%20s: %5d   %20s: %5d\n", strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], r->demand[i], strings[itemnames[0][FIRSTLUXURY+i+1]][L_DEUTSCH], r->demand[i+1]);
      else
	fprintf (F, "%20s: %5d\n", strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], r->demand[i]);
    }

  for (i = 0; i < MAXHERBS; i++)
	fprintf (F, "Kraut %2d: %-5d   ", i, r->herbs[i]);
  fprintf (F, "\n");

  fprintf (F, "Gebaeude:\n");

  if (!r->buildings)
    fprintf (F, " keine\n");
  else
    {
      for (b = r->buildings; b; b = b->next)
        {
          fprintf (F, "%s, Groesse %d, %s\n",
                   buildingid (b), b->size, buildingtype (b));
          fprintf (F, " (Besitzer: ");
          for (u = r->units; u; u = u->next)
            if (u->building == b && u->owner)
              {
                fprintf (F, "%s, Partei %d",
                         unitid (u), u->faction->no);
                break;
              }
          if (!u)
            fprintf (F, "niemand");
          fprintf (F, ")\n");
        }

      b = largestbuilding (r);
      fprintf (F, "Groesste Burg: %s\n", buildingid (b));
    }
//  d = free_building_number ();
//  fprintf (F, "Naechste freie Burgnummer: %d\n", d );

  fprintf (F, "Schiffe:\n");

  if (!r->ships)
    fprintf (F, " keine\n");
  else
    for (sh = r->ships; sh; sh = sh->next)
      {
        fprintf (F, "%s, %s", shipid (sh), shiptypes[0][sh->type]);
        if (sh->left)
          fprintf (F, ", im Bau");
        fprintf (F, "\n (Besitzer: ");
        for (u = r->units; u; u = u->next)
          if (u->ship == sh && u->owner)
            {
              fprintf (F, "%s, Partei %d",
                       unitid (u), u->faction->no);
              break;
            }
        if (!u)
          fprintf (F, "niemand");
        fprintf (F, ")\n");
      }

  fprintf (F, "Parteien:\n");

  if (!r->units || !factions)
    fprintf (F, " keine\n");
  else
    for (f = factions; f; f = f->next)
      for (u = r->units; u; u = u->next)
        if (u->faction == f)
          {
            fprintf (F, " %s\n", factionid (u->faction));
            break;
          }

  for (d = 0; d != MAXDIRECTIONS; d++)
    {
      fprintf (F, "Im %s: %s, %s\n", directions[d], 
	       regionid (r->connect[d]), 
	       strings[terrainnames[mainterrain (r->connect[d])]][DEFAULT_LANGUAGE]);

      if (!r->connect[d] || !r->connect[d]->units || !factions)
        fprintf (F, " keine\n");
      else
        for (f = factions; f; f = f->next)
          for (u = r->connect[d]->units; u; u = u->next)
            if (u->faction == f)
              {
                fprintf (F, " %s\n", factionid (u->faction));
                break;
              }
    }
}

void
regioninfo (void)
{
  region *r;

  r = inputregion ();
  if (!r)
    return;

  writeregioninfo (r, stdout);

  if (!cfopen ("info", "w"))
    return;
  puts ("Schreibe Info...");

  writeregioninfo (r, F);

  fclose (F);
}
/* ------------------------------------------------------------- */
void
writeunitinfo (unit * u, FILE * F)
{
  int d;
  fprintf (F, "%s von Partei %s\n", unitid (u), factionid (u->faction));
  fprintf (F, "%d %s (%d)\n", u->number, strings[typenames[1][u->type]][L_DEUTSCH], u->type);
  /*TPWORK*/
  d = free_unit_number ();
  fprintf (F, "Naechste freie Einheitennummer: %d  Hoechte Nummer: %d\n", d, highest_unit_no);
}

void
unitinfo (void)
{
  unit * u;

  u = inputunit ();
  if (!u)
    return;

  writeunitinfo (u, stdout);

  if (!cfopen ("info", "w"))
    return;
  puts ("Schreibe Info...");

  writeunitinfo (u, F);

  fclose (F);
}

/*-------------------------------------------------------------*/
int
getrandomlandmonster (void)
{
  int i;
  int irnd;
  int isum;
  irnd = rand () % 100;
  isum = 0;
  for (i=0;i!=MAXTYPES;i++)
  {
    isum += random_land_monster[i];
    if (isum > irnd) return i;
  }
  return -1;
}
/*-------------------------------------------------------------*/
void
spawn_monsters (void)
{
  region *r;
  unit *u;
  faction *f;
  int n;
  int count;
  int type;

  count=0;
  
  if (!(f = findfaction (0)))
  {
      createmonsters ();
      if (!(f = findfaction (0)))
        puts ("* Fehler! Die Monsterpartei konnte nicht erzeugt "
              "werden/n");
      return;
  }
  puts ("- zufaellige Monster ausgesetzt: ");
  for (r = regions;r;r=r->next)
  {
   if (!r->units)
   {
    type = -1;
    if (r->terrain == T_OCEAN)
    {
     if (rand () % MONSTERSEEDBASE < MONSTERSEEDOCEAN )
     {
    	type = U_SERPENT;
    	n = rand () % (MONSTERSEEDMAXNUM / monsterimpact[type]) +1;
     }
    }
    else /*Land*/
    {
     if (rand () % MONSTERSEEDBASE < MONSTERSEEDLAND )
     {
	type = getrandomlandmonster ();
    	n = rand () % (MONSTERSEEDMAXNUM / monsterimpact[type]) +1;
     }
    }
   
    if (type >=0) {
     count++;  
     sprintf(buf,"   in %s %d %s,",regionid(r),n,strings[typenames[((n==1)?0:1)][type]][0]);
     puts (buf);
     switch (type) {
     case U_FIREDRAGON:
       u = make_firedragon_unit (r, f, n);
       break;
     case U_DRAGON:
       u = make_dragon_unit (r, f, n);
       break;
     case U_WYRM:
       u = make_wyrm_unit (r, f, n);
       break;
     case U_UNDEAD:
       u = make_undead_unit (r, f, n);
       break;
     case U_ILLUSION:
       u = make_illsionary_unit (r, f, n);
       break;
     case U_GUARDS:
       u = make_guards_unit (r, f, n);
       break;
     case U_SERPENT:
       u = make_serpent_unit (r, f, n);
       break;
     case U_THIEF:
       u = make_thief_unit (r, f, n);
       break;
     default:
       break;
     } /*switch*/
    }  /*if type >=0*/
   }   /*if !r->units*/
  }    /*for*/
  sprintf(buf,"   in %d Regionen.\n",count);
  puts (buf);
}
