#include "atlantis.h"

faction *inputfaction()
{
  faction *f;
  int id;
  printf ("Nummer der Partei: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  f = findfaction (id);
  if (!f)
    {
      puts ("Diese Partei existiert nicht.");
      return 0;
    }
  return f;
}


int 
is_magician (unit *u)
/*Hier muss noch etwas dran getan werden, wenn die einzelnen Schulen 
  eingefuehrt werden.*/
{
	if(u->number) return ((u->skills[SK_MAGIC] > 0) ||
			      (u->skills[SK_SCHOOL_OF_FIRE] > 0) ||
			      (u->skills[SK_SCHOOL_OF_WATER] > 0) ||
			      (u->skills[SK_SCHOOL_OF_AIR] > 0) ||
			      (u->skills[SK_SCHOOL_OF_EARTH] > 0) ||
			      (u->skills[SK_SCHOOL_OF_LIFE] > 0) ||
			      (u->skills[SK_SCHOOL_OF_DEATH] > 0) ||
			      (u->skills[SK_SCHOOL_OF_ASTRAL] > 0)
			      );
	else return 0;
}

int 
get_used_ve (faction *f)
{
	return f->nunits + f->nmagicians*VE_COST_MAGICIAN;
}

int 
get_free_ve (faction *f)
{
	return VE_LIMIT - get_used_ve(f);
}

int 
can_make_new_unit (faction *f)
{
	return (get_free_ve(f) >= 1);
}

int 
can_study_magic (unit *u, faction *f)
{
	if (is_magician(u)) return 1;
	if (get_free_ve(f) >= VE_COST_MAGICIAN * u->number) return 1;
	return 0;
}

int
can_recruit_magicians (faction *f, int n)
{
	return (get_free_ve(f) >= VE_COST_MAGICIAN * n);
}


int 
can_give_magicians (faction *fd, unit *us, unit *ud, int n)
/* Diese Funktion wird nur aufgerufen, wenn die zu uebergebenden
   Personen Magie beherrschen. us ist die Einheit, die uebergeben werden
   soll, ud die angegebene Zieleinheit. Die Zielpartei fd wird mit angegeben,
   falls keine Zieleinheit da ist.
   
   Wenn eine Zieleinheit ud angegeben ist, dann bekommt diese die Magier.
   Damit lernt auch die empfangende Einheit automatisch Magie - wenn
   sie sie nicht schon kannte - was natuerlich abgefangen werden muss.
   
   Wird keine Einheit ud angegeben, so bekommt die Partei der Zieleinheit die Magier,
   und es muss nur gegen das Limit getestet werden, das die Partei
   selbst hat.*/
{
  // nichts zu uebergeben ist kein Problem
  if (n==0) return 1;
  // wenn keine Zieleinheit angegeben ist, dann wird eine Einheit an eine Partei
  // uebergeben. Dann sind auch zwei Parteien beteiligt, also muessen
  // wir nur die Parteien unterscheiden, wenn ud angegeben ist.
	if (ud) 
	{
	    if (us->faction == fd) // Personen werden innerhalb der Partei uebergeben
	    // TODO: Wenn eine Partei sich selbst Magier in eine TEMP uebergeben will 
	    // und am Limit ist wird das verboten (Mail Senger vom 5.9.2002)
	    {
		if (is_magician(us) && is_magician(ud)) 
		  // Wenn alle schon Magier sind, kein Problem
		  return 1; 
		if (is_magician(ud)) 
		  // Alle uebergebenen Personen werden Magier
		  return (get_free_ve(fd) >= (VE_COST_MAGICIAN * n));
		if (is_magician(us))
		  // Alle Personen der Zieleinheit werden Magier
		  return(get_free_ve(fd) >= (VE_COST_MAGICIAN * ud->number)); 
		assert(0); // boese
	    }
	    else // Personen werden an fremde Partei uebergeben
	    {
		if (is_magician(us) && is_magician(ud)) 
		  // Alle uebergebenen Personen sind neue Magier
		  return (get_free_ve(fd) >= (VE_COST_MAGICIAN * n));
		if (is_magician(ud)) 
		  // Alle uebergebenen Personen werden Magier
		  return (get_free_ve(fd) >= (VE_COST_MAGICIAN * n));
		if (is_magician(us))
		  // Alle Personen der Zieleinheit werden Magier
		  return(get_free_ve(fd) >= (VE_COST_MAGICIAN * (n+ud->number)) ); 
		assert(0); // boese
	    }
	}
	
	else  //Hier muessen wir auch noch eine Einheit abrechnen, deswegen +1
	{
		assert (us->faction != fd); // Muss vorher geprueft worden sein
		return (get_free_ve(fd) >= (VE_COST_MAGICIAN * us->number +1) );
	}
	
}

void 
give_magicians (faction *fd, unit *us, unit *ud, int n)
{
  // nichts zu uebergeben ist kein Problem
  if (n==0) return;
  // wenn keine Zieleinheit angegeben ist, dann wird eine Einheit an eine Partei
  // uebergeben. Dann sind auch zwei Parteien beteiligt, also muessen
  // wir nur die Parteien unterscheiden, wenn ud angegeben ist.
	if (ud) 
	{
	    if (us->faction == fd) // Personen werden innerhalb der Partei uebergeben
	    {
		if (is_magician(us) && is_magician(ud)) 
		  // Wenn alle schon Magier sind, kein Problem
		  return; 
		if (is_magician(ud)) 
		  // Alle uebergebenen Personen werden Magier
		  fd->nmagicians+=n;
		  return;
		if (is_magician(us))
		  // Alle Personen der Zieleinheit werden Magier
		  fd->nmagicians+=ud->number;
		  return; 
		assert(0); // boese
	    }
	    else // Personen werden an fremde Partei uebergeben
	    {
		if (is_magician(us) && is_magician(ud)) 
		  // Alle uebergebenen Personen sind neue Magier
		  us->faction->nmagicians -= n;
		  fd->nmagicians += n;
		  return;
		if (is_magician(ud)) 
		  // Alle uebergebenen Personen werden Magier
		  fd->nmagicians += n;
		  return;
		if (is_magician(us))
		  // Alle Personen der Zieleinheit werden Magier
		  us->faction->nmagicians -= n;
		  fd->nmagicians += (n+ud->number);
		  return;
		assert(0); // boese
	    }
	}
}


	
