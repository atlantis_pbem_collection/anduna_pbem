#include "atlantis.h"

int get_building_effect(const int WhatSkill,
			 building *pBuilding,
			 region *pRegion,
                         unit *pUnit,
                         int *TypeOfEffect
                         )
{

 int AllowOrNot,Bonus,i;
 int TerrainIsCoast = 0;

 switch(WhatSkill)
 {
  case SK_TRADE :
  {
   /* Jede Burg > 1 Stein erlaubt Handel. */
   AllowOrNot = buildingeffsize(pBuilding);
   if( TypeOfEffect ) *TypeOfEffect = ALLOW;
   return AllowOrNot;
  }
  case SK_LUMBERJACK :
  {
   /*Burg in einem Wald =>
     Festung (250 Steine) : S�gewerk (schlage ein Baum, bekomme 2 Holz)
     Zitadelle (1250 Steine) : verdoppele Wachstum
   */
   if( pUnit )
   { /* der Aufruf bezieht sich auf Schlagen von Holz. */
     if( (buildingeffsize(pBuilding) >= B_FORTRESS) &&
         (pRegion->terrain == T_FOREST)
        )
     {
      if( TypeOfEffect ) *TypeOfEffect = BONUS_PRODUCTION;
      Bonus = effskill(pUnit,SK_TRADE);
      return Bonus;
     }
     else return 0;
   }
   /* der Aufruf bezieht sich auf Wachsen von Holz.
      also wird der R�ckgabewert dieser Funktion zu dem Wert
      dazuaddiert, gegen den f�r jeden Baum gew�rfelt wird.
   */
   if( (buildingeffsize(pBuilding) >= B_CITADEL) &&
       (pRegion->terrain == T_FOREST)
      )
   {
    if( TypeOfEffect ) *TypeOfEffect = GROWTH_RATE_INCREASE;
    return FORESTGROWTH;/* wird dadurch verdoppelt. */
   }
   else return 0;
  }
  case SK_HORSE_TRAINING :
  {
   /* hier wird nur gewachsen, nicht gez�hmt */
   if( pRegion->terrain != T_HIGHLAND ) return 0;
   if( pUnit )
   {
    if( buildingeffsize(pBuilding) >= B_FORTRESS )
    return effskill(pUnit,SK_HORSE_TRAINING);	
   }
   if( buildingeffsize(pBuilding) >= B_FORTRESS )
   {
    if( TypeOfEffect ) *TypeOfEffect = GROWTH_RATE_INCREASE;
    return HORSEGROWTH;/* wird dadurch verdoppelt. */
   }
   else return 0;
  }
  case SK_SHIPBUILDING :
  {

   /* Eine Festung gilt als Hafen (-1 Holzverbrauch)
      Eine Zitadelle gilt als Grosswerft (-2 Holzverbrauch),
      das nat�rlich nur in einer K�stenregion.
   */
   for(i=0;i<MAXDIRECTIONS;++i)
   {
    if( pRegion->connect[i] )
    { if( pRegion->connect[i]->terrain == T_OCEAN )
      { TerrainIsCoast = 1; break; }
    }
   }
   if( ! TerrainIsCoast ) return 0;
   if( buildingeffsize(pBuilding) == B_FORTRESS )
   {
    if( TypeOfEffect ) *TypeOfEffect = LESS_RESOURCES_USED;
    /* Die zur�ckgegebene Zahl weniger abziehen :
       u->n -= min(n,(n - get_building_effect(SK_SHIPBUILDING,u->building,r,u,0));
    */
    return -1;
   }
   if( buildingeffsize(pBuilding) == B_CITADEL )
   {
    if( TypeOfEffect ) *TypeOfEffect = LESS_RESOURCES_USED;
    /* Die zur�ckgegebene Zahl weniger abziehen :
       u->n -= min(n,(n - get_building_effect(SK_SHIPBUILDING,u->building,r,u,0));
    */
    return -2;
   }
   /* default : Nichts ver�ndern. */
   return 0;
  }
 }




 return 0;
}


/*
void get_building_functions(
{

 erst : if( TerrainIsCoast ) Hafen/Werft einbauen

 switch(pRegion->terrain)
 Pferdezucht, Forsthaus, Saegewerk, Universitaet


}
*/
