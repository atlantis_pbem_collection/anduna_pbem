/* Anduna PBEM Host 5.7 Copyright (C) 1999-2001 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

#ifndef ATLANTIS_H
#define ATLANTIS_H

/* Fuer FILE braucht man hier eigentlich nur stdio.h; da aber andere Funktionen auch in fast allen .o files
   gebraucht werden, werden hier die meisten libraries schon eingebunden.  assert.h fuer assert (), stdlib.h fuer
   rand (), string.h fuer strcat (), etc.  */

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

/* Indicator soll einigermassen unabhaengig sein. */
#include "indicato.h"

/* Translation soll unabhaengig sein.  */
#include "translat.h"

/*Spruchdaten ausgelagert. */
#include "magic.h"

/* Zuerst defines, welche gebraucht werden. */

/* Folgende Konstanten werden fuer alte Versionen gebraucht (Vxx).
   Sie werden nur readgame () verwendet werden, um Datenfiles alter
   Versionen richtig lesen zu koennen.  Aendert sich der source code,
   aber nicht der Datenfile (wie so oft), muss nur RELEASE_VERSION
   erhoeht werden.  Alte Versionen koennen periodisch geloescht
   werden.  */

#define V20                     20
#define V24                     24
#define V25                     25
#define V32                     32
#define V40                     40
#define V50                     50
#define V55                     55 /* TP: Fuer neues Datenfile mit mehr Platz*/
#define V56                     56 /* TP: Bei aelteren Versionen als dieser werden 
					  alle Gebaeude als Burgen interpretiert 
					  (save.c) */
#define V57                     57 /*TP: Fuer Spielerkommentar*/
#define V58                     58 /*TP: Fuer Datenfiles ohne Slots fuer Skillz'n'Items*/
#define V59                     59 /*TP: Mana und neue Magietalente ins Datenfile*/
#define V60                     60 //TP: Combatspellmana[3] und Bauernansehen der Partei, 4*Helfe, Spruchliste geaendert
#define V61                     61 //TP: Regions haben jetzt auch effect, Burgen und Schiffe und Parteien effect und enchanted

#define RELEASE_VERSION         61

/* Default fuer die Auswertung, falls kein ZAT angegeben wird. */

#define NOZAT  "??? (siehe Kommentar)"

/* Fuer das Lesen der Befehle.  */

#define MAXSTRING               1500
#define MAXLINE                 5000
#define SPACE_REPLACEMENT       '_'
#define SPACE                   ' '
#define ESCAPE_CHAR             '\\'

/* Laengenbeschraenkungen fuer einzelne Datenfelder.  Oft ist diese Beschraenkung nicht wirklich noetig, weil die
   Text dynamisch cmalloced ("checked" malloc) werden.  Es dient sozusagen zur Reduktion des Datenfiles...  */

#define DISPLAYSIZE             400
#define NAMESIZE                80
#define STARTMONEY              5000
#define STARTMONEY_INCREMENT	50 /*pro gespielter runde dazu*/

/* Beim wievielten NMR wird die Partei geloescht. */

#define ORDERGAP                4

/* Wieviele VE's (Verwaltungseinheiten) hat eine Partei und wieviele davon verbraucht ein Magier*/

#define VE_LIMIT 		1000
#define VE_COST_MAGICIAN 	100

/* Sprache von Debug-Meldungen ist Deutsch.  Falls dort mehrsprachige Dinge wie zB. itemnames vorkommen,
   benoetigt man den Index der Default-Sprache.  Dies ist die erste Sprache im [LANGUAGES] Abschnitt in
   language.def -- Deutsch sollte 0 sein.  Da alle Debug Texte sowieso sprachunabhaengig sind, wurde dort auch
   0 hineingeschrieben (aus Faulheit).  */

/* Wieviele Tage lernt man in einem Monat bei der Anwendung eines Talentes.  Wieviel Silber braucht ein Wesen pro
   Monat, um sich zu ernaehren.  Wieviele Bauern leben in einer "Parzelle" -- eine Region hat 1000 Parzellen, in
   denen je ein Baum oder ein Pferd oder (i.A.) 10 Bauern leben koennen.  Wieviel Grundlohn hat man beim ARBEITE
   Befehl.  Wieviel Bonus gibt es bei hoeheren Burgengroessen.  */

#define PRODUCEEXP              10
#define MAINTENANCE             10
#define MAXPEASANTS_PER_AREA    10
#define WAGE                    11
#define BONUS                   1
#define TAXINCOME               20  // mit dem Talent Steuereintreiben. Ohne Talent: income[u->type]

// Wieviel ein Rekrut kostet, und welcher Teil (1/20) der Bauern rekrutiert
//   werden kann.

#define RECRUITCOST             50
#define RECRUITCOST_TO_PEASANTS 20 // soviel bekommen die Bauern vom Rekrutiergeld
#define RECRUITFRACTION         20 // war 4


// Welchen Teil des Silbers die Bauern fuer Unterhaltung ausgeben (1/20), und
// wieviel Silber ein Unterhalter pro Talentpunkt bekommt.

#define ENTERTAININCOME         20
#define ENTERTAINFRACTION       20

// Wieviele Silbermuenzen jeweils auf einmal "getaxed" werden.

#define TAXFRACTION             10

// Wieviel Silber pro Talentpunkt geklaut wird.

#define STEALINCOME             50


/* Maximale Geschwindigkeiten -- die doppelte Geschwindigkeit des schnellsten Schiffes, wegen dem Sturmwind-Zauber.
   Wird benoetigt fuer einen array von Regionen, durch den Schiffe segeln.  Im Moment ist das die
   Bootsgeschwindigkeit mit Kompass, Sturmwind und Helm der Sieben Winde: (3+1) * 2 * 3 = 24; auch sehr hoch: Trireme mit
   Kompass und Sturmwind: (8+1) * 2 = 18.  */

#define MAXSPEED                25

enum
  {
    M_TERRAIN,
    M_FACTIONS,
    M_UNARMED,
    MAXMODES,
  };

enum
  {
    ST_FIGHT,
    ST_BEHIND,
    ST_AVOID,
    MAXSTATUS
  };


/* bislang nur fuer building_effect */
enum { ALLOW,
       FORBID,
       BONUS_PRODUCTION,
       LESS_RESOURCES_USED,
       GROWTH_RATE_INCREASE
      };

enum
  {
    K_ADDRESS,
    K_WORK,
    K_ATTACK,
    K_NAME,
    K_STEAL,
    K_BESIEGE,
    K_DISPLAY,
    K_ENTER,
    K_GUARD,
    K_MAIL,
    K_END,
    K_FIND,
    K_FOLLOW,
    K_RESEARCH,
    K_GIVE,
    K_ALLY,
    K_STATUS,
    K_COMBAT,
    K_BUY,
    K_PLAYERCOMMENT,
    K_CONTACT,
    K_TEACH,
    K_STUDY,
    K_DELIVER,
    K_MAKE,
    K_MOVE,
    K_PASSWORD,
    K_RECRUIT,
    K_COLLECT,
    K_SEND,
    K_QUIT,
    K_TAX,
    K_ENTERTAIN,
    K_SELL,
    K_LEAVE,
    K_FORGET,
    K_CAST,
    K_RESHOW,
    K_DESTROY,
    K_USE,
    MAXKEYWORDS,
  };

extern char *keywords[MAXKEYWORDS];

enum
  {
    P_ALL,
    P_PEASANT,
    P_LOOT,
    P_BUILDING,
    P_UNIT,
    P_BEHIND,
    P_CONTROL,
    P_MAN,
    P_NOT,
    P_NEXT,
    P_FACTION,
    P_PERSON,
    P_REGION,
    P_SHIP,
    P_SILVER,
    P_ROAD,
    P_TEMP,
    P_AND,
    P_SPELLBOOK,
    P_ALLBUILDINGS,
    P_HERBS,
    P_FIGHT,
    P_GIVE,
    P_GUARD,
    MAXPARAMS,
  };

extern char *parameters[MAXPARAMS];

enum
  {
    O_REPORT,   // 1
    O_COMPUTER, // 2
    O_ZINE,     // 4
    O_COMMENTS, // 8
    O_STATISTICS, // 16
    O_DEBUG,      // 32
    O_COMPRESS,   // 64
    O_TAL_IN_TEMPLATE, // 128
    O_MISC,            // 256
    MAXOPTIONS,
  };

extern char *options[MAXOPTIONS];

enum
  {
    SK_CROSSBOW,	//  0
    SK_LONGBOW,		//  1
    SK_CATAPULT,
    SK_SWORD,
    SK_SPEAR,
    SK_RIDING,
    SK_TACTICS,
    SK_MINING,
    SK_BUILDING,
    SK_TRADE,
    SK_LUMBERJACK,
    SK_MAGIC,		// 11
    SK_HORSE_TRAINING,
    SK_ARMORER,
    SK_SHIPBUILDING,
    SK_SAILING,
    SK_QUARRYING,
    SK_ROAD_BUILDING,
    SK_STEALTH,
    SK_ENTERTAINMENT,
    SK_WEAPONSMITH,
    SK_CARTMAKER,	// 21
    SK_OBSERVATION,
    SK_SCHOOL_OF_FIRE,
    SK_SCHOOL_OF_WATER,
    SK_SCHOOL_OF_AIR,
    SK_SCHOOL_OF_EARTH,
    SK_SCHOOL_OF_LIFE,
    SK_SCHOOL_OF_DEATH, // 28
    SK_SCHOOL_OF_ASTRAL,
    SK_HERBALISM,
    SK_TAX,
    SK_ALCHEMY,
    MAXSKILLS
    /* Changes here will affect the syntax checker acheck.c! */
  };

#define MAXSKILLS_V55 (SK_OBSERVATION+1)
#define MAXSKILLS_V58 (SK_SCHOOL_OF_ASTRAL+1)


#define SCHOOLNUM(i) (i - SK_SCHOOL_OF_FIRE)
#define MAXSCHOOLS (SK_SCHOOL_OF_ASTRAL - SK_SCHOOL_OF_FIRE + 1)

extern char *skillnames[MAXSKILLS];
extern char *skillabbreviations[MAXSKILLS];

enum
  {
    U_MAN,
    U_UNDEAD,
    U_ILLUSION,
    U_FIREDRAGON,
    U_DRAGON,
    U_WYRM,
    U_GUARDS,
    U_SERPENT,
    U_THIEF,
    U_WATER_ELEMENTAL,
    MAXTYPES,
  };

enum
  {
    HL_GIVE,
    HL_SILVER,
    HL_FIGHT,
    HL_GUARD,
    MAXHELP,
  };

extern char *helpnames[MAXHELP];

extern int typenames[2][MAXTYPES];
extern int income[MAXTYPES];

enum
  {
    D_NORTH,
    D_SOUTH,
    D_EAST,
    D_WEST,
    MAXDIRECTIONS,
  };

extern char back[MAXDIRECTIONS];
extern char delta_x[MAXDIRECTIONS];
extern char delta_y[MAXDIRECTIONS];
extern char *directions[MAXDIRECTIONS];

/* Sollten sich diese Terrains aendern, muessen gewisse Zaubersprueche auch
   angepasst werden!  (magic.c, Segen der Goettin, Hammer der Goetter,
   Provokation der Titanen mindestens)*/

enum
  {
    T_OCEAN,
    T_PLAIN,
    T_FOREST,                   /* wird zu T_PLAIN konvertiert */
    T_SWAMP,
    T_DESERT,                   /* kann aus T_PLAIN entstehen */
    T_HIGHLAND,
    T_MOUNTAIN,
    T_GLACIER,                  /* kann aus T_MOUNTAIN entstehen */
    MAXTERRAINS,
  };

extern int terrainnames[MAXTERRAINS];
extern char terrainsymbols[MAXTERRAINS];
extern int production[MAXTERRAINS];
extern char mines[MAXTERRAINS];
extern int roadreq[MAXTERRAINS];
extern char *roadinto[MAXTERRAINS];
extern char *trailinto[MAXTERRAINS];

enum
  {
    SH_BOAT,
    SH_LONGBOAT,
    SH_DRAGONSHIP,
    SH_CARAVELL,
    SH_TRIREME,
    SH_RAFT,       /* FLOSS */
    SH_BALLOON,       /* LUFTSCHIFFE */
    MAXSHIPS,
  };

#define LASTSHIP   (SH_RAFT +1)
#define isship(i)  (0 <= i && i < LASTSHIP)
#define FIRSTAIRSHIP   (SH_BALLOON)
#define LASTAIRSHIP   (SH_BALLOON+1)
#define isairship(i)  (FIRSTAIRSHIP <= i && i < LASTAIRSHIP)



extern char *shiptypes[3][MAXSHIPS];
extern int sailors[MAXSHIPS];
extern int shipcapacity[MAXSHIPS];
extern int shipcost[MAXSHIPS];
extern char shipspeed[MAXSHIPS];
/* FLOSS */
extern char shipbuildingtalent[MAXSHIPS];
extern char captain_talent[MAXSHIPS];

enum
  {
    B_SITE,
    B_FORTIFICATION,
    B_TOWER,
    B_CASTLE,
    B_FORTRESS,
    B_CITADEL,
    MAXBUILDINGS,
  };

#define STONERECYCLE                    50

extern int buildingcapacity[MAXBUILDINGS];
extern char *buildingnames[MAXBUILDINGS];


enum
  {
    I_IRON,	/*0*/	/* start of ressources and products */
    I_WOOD,
    I_STONE,    /*2*/
    I_RES1,
    I_RES2,
    I_AIRSHIP_RES,
    I_HORSE,	/*6*/	/* end of ressources */
    I_WAGON,
    I_CATAPULT,
    I_SWORD,
    I_SPEAR,
    I_CROSSBOW,
    I_LONGBOW,  /*12*/
    I_PROD1,
    I_PROD2,
    I_PROD3,
    I_PROD4,
    I_CHAIN_MAIL, /*17*/
    I_PLATE_ARMOR,	/* end of products */
    I_BALM,		/* start of luxuries */
    I_SPICE,
    I_JEWELRY,
    I_MYRRH,
    I_OIL,
    I_SILK,
    I_INCENSE,		/* end of luxuries */
    I_HOOD,	/* start of magic items */
    I_AMULET_OF_LIFE,
    I_AMULET_OF_HEALING,
    I_AMULET_OF_TRUE_SEEING,
    I_CLOAK_OF_INVULNERABILITY,
    I_RING_OF_INVISIBILITY,
    I_RING_OF_POWER,
    I_RUNESWORD,
    I_SHIELDSTONE,
    I_WINGED_HELMET,
    I_DRAGON_PLATE,   /*36*/
    I_MAGIC_EYE,
    I_COMPASS,
    I_FLAMESWORD,
    I_MAG4,
    I_MAG5,
    I_MAG6,
    I_MAG7,
    I_MAG8,
    I_4,	/*45*/	/* end of magic items */
    I_HERB_PLAIN_1, // start of herbs
    I_HERB_PLAIN_2,
    I_HERB_PLAIN_3,
    I_HERB_FOREST_1,
    I_HERB_FOREST_2,
    I_HERB_FOREST_3,
    I_HERB_SWAMP_1,
    I_HERB_SWAMP_2,
    I_HERB_SWAMP_3,
    I_HERB_DESERT_1,
    I_HERB_DESERT_2,
    I_HERB_DESERT_3,
    I_HERB_HIGHLAND_1,
    I_HERB_HIGHLAND_2,
    I_HERB_HIGHLAND_3,
    I_HERB_MOUNTAIN_1,
    I_HERB_MOUNTAIN_2,
    I_HERB_MOUNTAIN_3,
    I_HERB_GLACIER_1,
    I_HERB_GLACIER_2,
    I_HERB_GLACIER_3, // 66 end of herbs
    I_POTION_MANA,//begin of potions
    I_POTION_HEAL,//end of potions
    MAXITEMS
    /* Changes here will affect the syntax checker acheck.c! */
  };

#define MAXITEMS_V58 (I_HERB_PLAIN_1) // For backward compatibility

extern int itemnames[2][MAXITEMS];

#define LASTBUILDINGMATERIAL (I_STONE)
#define LASTRESSOURCE   (I_HORSE +1)
#define isressource(i)  (0 <= i && i < LASTRESSOURCE)

#define LASTPRODUCT     (I_PLATE_ARMOR +1)
#define isproduct(i)    (0 <= i && i < LASTPRODUCT)

#define FIRSTLUXURY     (I_BALM)
#define LASTLUXURY      (I_INCENSE +1)
#define MAXLUXURIES     (LASTLUXURY - FIRSTLUXURY)
#define isluxury(i)     (FIRSTLUXURY <= i && i < LASTLUXURY)

#define FIRST_MAGIC_ITEM (I_HOOD)
#define LAST_MAGIC_ITEM (I_4)
#define is_magic_item(i) (FIRST_MAGIC_ITEM <= i && i < LAST_MAGIC_ITEM)

#define FIRSTHERB 	(I_HERB_PLAIN_1)
#define LASTHERB 	(I_HERB_GLACIER_3+1)
#define isherb(i)   	(FIRSTHERB <= i && i < LASTHERB)
#define herb_by_terrain(terrain,herb) (FIRSTHERB + (terrain-1)*3 + herb) 

#define FIRSTPOTION	(I_POTION_MANA)
#define LASTPOTION	(I_POTION_HEAL+1)
#define ispotion(i)   	(FIRSTPOTION <= i && i < LASTPOTION)

extern int itemskill[LASTPRODUCT];
extern char itemweight[LASTLUXURY];
extern char itemquality[LASTPRODUCT];
extern int rawmaterial[LASTPRODUCT];
extern char rawquantity[LASTPRODUCT];
extern int itemprice[MAXLUXURIES];

/*CHG BUILDINGS: Neue Aufteilung in Gebaeudetypen*/
enum
  {
    BD_CASTLE,
    BD_MONUMENT,
    BD_AIRSHIPWHARF,
    MAX_BUILDINGS,
  };
extern int building_maxsize[MAX_BUILDINGS];
extern int building_cost[MAX_BUILDINGS][LASTBUILDINGMATERIAL+1];
extern int building_upkeep[MAX_BUILDINGS];
extern int building_talent[MAX_BUILDINGS];
extern char *building_names[3][MAX_BUILDINGS];

#define MONEYWEIGHT  0.01  /*0.01 Gewicht eines St�ckes Silber*/

#define TEACHNUMBER              10
#define STUDYCOST               200

#define MAX_AIRSHIP_RES		10 /*Soviel Zukus koennen per Monat erlegt werden*/

/* Werden die folgenden defines geaendert, muessen die
   Spruchbeschreibungen auch geaendert werden. */

enum
  {
    W_NONE,
    W_BARELY,
    W_COPSE,
    W_FOREST,
    W_JUNGLE,
    MAXWOODS,
  };
  
enum // Ab Datenfilversion 6 gibt es drei verschiedene Kampfzauber
  {
	CS_BEFORE,
	CS_DURING,
	CS_AFTER,
	MAXCOMBATSPELLS,
  };


extern char woodsize[MAXWOODS];

#define DEMANDRISE          (25)
#define DEMANDFALL          (150)
#define DEMANDFACTOR        (2500)
#define MAXDEMAND           (10000)
#define MINDEMAND           (100)

#define PEASANTGROWTH       (2)   /* Prozentchancen, waren alle 5, dann alle 3 */
#define PEASANTMOVE         (1)   /* Normales Wandern in Nachbarregionen */
#define PEASANTGREED        (1)   /* Zusaetzl. zur reichsten Nachbarregion. */

#define STARVATION_SURVIVAL       (3)  /* 1/x = Chance zu ueberleben */
#define OVERPOPULATION_FRACTION   (10) /* derzeit unbenutzt? */

#define HORSEGROWTH         (4)   /* Prozentchancen, waren alle 5 */
#define HORSEMOVE           (10)

#define FORESTGROWTH        (2)   /* Prozentchancen, waren 2 und 1 */
#define FORESTSPREAD        (1)
#define HERBSGROWTH 5 /*Prozentchance, dass sich Kraeuter vermehren, gedeckelt auf MAXHERBS_PER_REGION*/
#define MAXHERBS_PER_REGION 100 /* Bis zu dieser Menge wachsen Kraeuter*/
#define MAXHERBS 3 /*Aendert Datenfile!*/
#define INIT_BUEKAZ 50
#define INIT_HERBS 50
#define INIT_PEASANTS_LIKE_FACTION 50

/*WINNIE*/
#define ADMIN_PER_FACTION       1000 /* max. Vorsorgungseinheitn pro Partei
                   							 NICHT direkt benutzen
                                        -> get_maxadmin_for_faction(faction *)
                                     */

#define ADMIN_USED_PER_MAGICIAN 100  /* benutzte Versorgungseinheiten pro Magier
                   							 NICHT direkt benutzen
                                        -> get_admin_used_per_magician(faction *)
                                     */

/* Defines for items and movement.  */

/* Die Tragekapaz. ist hardcodiert mit defines, da es bis jetzt sowieso nur 2 Objekte gibt, die
   etwas tragen. Ein Mensch wiegt 10, traegt also 5, ein Pferd wiegt 50, traegt also 20.  ein Wagen
   wird von zwei Pferden gezogen und traegt total 140, davon 40 die Pferde, macht nur noch 100, aber
   samt eigenem Gewicht (40) macht also 140. */

#define PERSONWEIGHT    10

#define HORSECAPACITY   70
#define WAGONCAPACITY  140

#define HORSESNEEDED    2

extern int carryingcapacity[MAXTYPES];

/* Verschiedene kleine Funktionen. */

#define min(a,b)                ((a) < (b) ? (a) : (b))
#define max(a,b)                ((a) > (b) ? (a) : (b))

#define addptr(p,i)             ((void *)(((char *)p) + i))

#define addlist2(l,p)           (*l = p, l = &p->next)

/* Structs, welche fuer die Datenstruktur wichtig sind. */

typedef struct list
  {
    struct list *next;
  }
list;

typedef struct strlist
  {
    struct strlist *next;
    char s[1];
  }
strlist;

struct unit;
typedef struct unit unit;

typedef struct building
  {
    struct building *next;
    int no;
    char *name;
    char *display;
    char besieged;
    int size;
    int sizeleft;
    int kind;
    int effect;
    int enchanted;
  }
building;

typedef struct ship
  {
    struct ship *next;
    int no;
    char *name;
    char *display;
    int effect;
    int enchanted;
    int type;
    int left;
  }
ship;

typedef struct region
  {
    struct region *next;
    int x, y;
    char *name;
    char *display;
    struct region *connect[MAXDIRECTIONS];
    int terrain;
    int trees;
    int horses;
    int peasants;
    int money;
    int road;
    int demand[MAXLUXURIES];
    int produced_good;
    int effect;
    int enchanted;
    char blocked;
    strlist *comments;
    strlist *debug;
    building *buildings;
    ship *ships;
    unit *units;
    int newpeasants;
    int newhorses;
    int newtrees;
    int buekaz; /* Buergerkriegsanzeiger, Bauernzufriedenheit: 0=Tollwut, 100=Paradies*/
    int specialresource; /* Fuer spezielle Resourcen, die zuende gehen koennen */
    int herbs[MAXHERBS]; /* Fuer Kraeuter, anfangs 3 je Region, sollen langsam wachsen */
    int touched; /* Wird auf true gesetzt, wenn jemand die Region betreten hat */
    int seen; /* Wird auf true gesetzt, wenn eine Partei die Region gesehen (Nachbarregion betreten) hat*/
  }
region;

struct faction;

typedef struct rfaction
  {
    struct rfaction *next;
    struct faction *faction;
    int factionno;
  }
rfaction;

typedef struct runit
  {
    struct runit *next;
    struct unit *unit;
  }
runit;

typedef struct faction
  {
    struct faction *next;
    int no;
    char *name;
    char *addr;
    char *passw;
    int lastorders;
    char newbie;
    int options;
    char seendata[MAXSPELLS];
    char showdata[MAXSPELLS];
    rfaction *allies[MAXHELP];
    strlist *mistakes;
    strlist *warnings;
    strlist *messages;
    strlist *battles;
    strlist *events;
    strlist *income;
    strlist *commerce;
    strlist *production;
    strlist *movement;
    strlist *debug;
    char alive;
    char attacking;
    char seesbattle;
    char dh;
    int nregions;
    int nunits;
    int nmagicians;
    int number;
    int money;
    int old_value;
    int language;
    int peasants_like_faction; // Startwert 50, zwischen 0 (Bauern hassen Partei) und 100 (Bauern lieben Partei)
    int value_magic;
    int value_sea;
    int value_battle;
    int rank_value;
    int rank_growth;
    int rank_magic;
    int rank_sea;
    int rank_battle;
    int effect;
    int enchanted;
  }
faction;

struct unit
  {
    struct unit *next;
    int no;
    char *name;
    char *display;
    char *playercomment;
    int number;
    int type;
    int money;
    int mana; // Hier wird die magische Energie gespeichert
    int hits; // Fuer spaeteren Gebrauch, erstmal ins Datenfile.
    int effect;
    int enchanted;
    faction *faction;
    runit *contacts[MAXHELP];
    runit *contacts_person;
    unit *target;
    building *building;
    ship *ship; 
    char owner;
    int status;
    char guard;
    char attacking;
    building *besieging; 
    /* Unterschied von thisorder und lastoder macht es moeglich, beim NACH Befehl den alten Default Befehl zu
       verwenden.  */
    char *thisorder;
    char *lastorder;
    char *thisorder2;        /* Fuer den LIEFERE Befehl.  */
    int combatspell[MAXCOMBATSPELLS];
    int combatspellmana[MAXCOMBATSPELLS]; /*Fuer die Manabegrenzung beim Kampfzauber*/
    int skills[MAXSKILLS];
    int items[MAXITEMS];
    char spells[MAXSPELLS];
    strlist *orders;
    int alias;
    int dead;
    int learning;
    int n;
    int wants;               /* Bei der Produktion: Anzahl der gewuenschten Produkte.  */
    int wants2;              /* Beim Handel:  Anzahl Produkte, die zu kaufen sind.  */
    int wants_recruits;      /* Beim Rekrutieren: Anzahl gewuenschter Rekruten -- da REKRUTIERE ein kurzer Befehl ist.  */
    int *litems;
    int side;
    struct unit *collector;  /* Falls gesetzt erhaelt diese Einheit alle Beute nach dem Kampf.  */
  };

typedef struct order
  {
    struct order *next;
    unit *unit;
    int qty;
    int type;
  }
order;

/* globale Variablen */

extern int turn;
extern int highest_unit_no;
extern char buf[MAXLINE];
extern char zat[NAMESIZE];
extern FILE *F;
extern FILE *LOGF;

extern region *regions;
extern faction *factions;

/* Funktionen fuer Speicher und Listen-Management */

void *cmalloc (int n);
void *crealloc (int n, void *ptr);

void addlist (void *l1, void *p1);
void translist (void *l1, void *l2, void *p);
void removelist (void *l, void *p);
void promotelist (void *l, void *p);
void freelist (void *p1);
int listlen (void *l);

strlist *makestrlist (char *s);
void addstrlist (strlist **SP, char *s);

/* Fehler, Warnungen, Meldungen */

void mistake2 (unit *u, strlist *S, char *comment);
void mistakeu (unit *u, char *comment);

void addwarning (faction *f, char *s);
void addevent (faction *f, char *s);
void addmessage (faction *f, char *s);
void addcomment (region *r, char *s);
void addbattle (faction *f, char *s);
void adddebug (region *r, char *s);
void addincome (faction *f, char *s);
void addcommerce (faction *f, char *s);
void addproduction (faction *f, char *s);
void addmovement (faction *f, char *s);

/* Manipulation von buf */

void scat (char *s);
void icat (int n);

/* String Manipulation allgemein */

int findstr (char **v, char *s, int n);
int _findstr (int v[], char *s, int n); /* Testversion als Ersatz */

void nstrcpy (char *to, char *from, int n);
void mnstrcpy (char **to, char *from, int n);
void mstrcpy (char **to, char *from);
char *strupr(char *c);

int geti (void);
int atoip (char *s);

/* Verteilung von vielen Zufallszahlen, meist die Anzahl Opfer von Zauberspruechen (zB. werden 2-50 getoetet). */

int lovar (int n);

/* Funktionen fuer Talente */

int distribute (int old, int new, int n);

int cansee (faction *f, region *r, unit *u);
int effskill (unit *u, int i);
int is_magic_talent (int i);

/* Parteien verwalten */
int is_magician (unit *u);
int get_free_ve (faction *f);
int get_used_ve (faction *f);
int can_make_new_unit (faction *f);
int can_study_magic (unit *u, faction *f);
int can_recruit_magicians (faction *f, int n);
int can_give_magicians (faction *f, unit *u, unit *u2, int n);
void give_magicians (faction *f, unit *u, unit *u2, int n);
void remove_unit (unit *u, region*r, int give);


/* Befehle lesen */

char *afgets (char *c, int size);

char *getstr (void);
char *agetstr (void);
char *igetstr (char *init);
char *aigetstr (char *init);

int finditem (char *s);
int getitem (void);
int findparam (char *s);
int igetparam (char *s);
int getparam (void);
int getspell (void);
int findkeyword (char *s);
int igetkeyword (char *s);

// region *findregion (int x, int y);

extern int getunit0;
extern int getunitpeasants;
extern int getship0;
extern char getunit_text[20];
unit *getunit (region *r, unit *u);
unit *getunitglobal (region *r, unit *u);
unit *findunitglobal (int n);
faction *findfaction (int n);
faction *getfaction (void);

region *findregion (int x, int y);

int isallied (unit *u, unit *u2, int t);

/* Namen von Objekten */

char *factionid (faction *f);
char *regionid (region *r);
char *unitid (unit *u);
char *unitid2 (unit *u);
char *buildingid (building *b);
char *buildingtype (building *b);
char *shipid (ship *sh);

building *largestbuilding (region *r);

/* Burgen und Schiffe */

int armedmen (unit * u);

void siege (void);

int buildingeffsize (building * b);
void build_road (region * r, unit * u);

/*void build_building (region * r, unit * u);*/
int build_building (unit * u, building * b);
building * set_building (region * r);
building * make_building (region * r, unit * u);
void create_building (region *r, unit *u, int newtype);
void continue_building (region * r, unit * u, building *b);
int castle_in_region (region * r);

ship * set_ship (region * r);
ship * make_ship (region * r, unit * u);
void create_ship (region * r, unit * u, int newtype);
void continue_ship (region * r, unit * u);
int cansail (region * r, ship * sh);
/* FLOSS */
int get_raft_capacity(ship *sh);

building *getbuilding (region * r, int n);
ship *getship (region * r);

building *findbuilding (int n);
ship *findship (int n);
int free_building_number (void);
int free_ship_number (void);

unit *buildingowner (region * r, building * b);
unit *shipowner (region * r, ship * sh);

void enter (void);
void givecommand (void);
void leaving (void);
void destroy (void);

/* Kampf */

void combat (void);

/* Kontakte */

int besieged (unit * u);
int slipthru (region * r, unit * u, building * b);
int can_contact (region * r, unit * u, unit * u2);
int contacts (region * r, unit * u, unit * u2, int t);
int contacts_person (region * r, unit * u, unit * u2);
void docontact (void);

/* Erschaffen von Monstern und Inseln */

void createmonsters (void);
void addunit (int type);
void addplayers (void);
void connectregions (void);
void makeblock (int x1, int y1);
void seed_monsters (int x1, int y1);
void createcontinent (void);
void listnames (void);
void changeterrain (void);
void addbuilding (void);
void regioninfo (void);  // in creation.c
void unitinfo (void);  // in creation.c
void shipinfo (void);  // in build.c
void buildinginfo (void); // in build.c
void showmap (int mode);
void writemap (int mode);
int sphericalx (int coord);
int sphericaly (int coord);

/* Wirtschaft */

extern int income[MAXTYPES];
extern int itemprice[MAXLUXURIES];

int findshiptype (char *s);
int findbuildingtype (char *s);

void scramble (void *v1, int n, int width);
void recruiting (void);
void giving (void);
void produce (void);
void stealing (void);
int collectmoney (region * r, unit * collector, int n);

/* Diverse */

void quit (void);
int getoption (void);
int wanderoff (region * r, int p);
void demographics (void);
void instant_orders (void);
void last_orders (void);
void set_passw (void);
void mail (void);
int walkingcapacity (unit * u);
void setdefaults (void);
region * inputregion (void);
unit * inputunit (void);
ship * inputship (void);
building * inputbuilding (void);
faction * inputfaction (void);

/* Magie */

int magicians (faction *f);
int bestmage (faction *f);
void listmagic (void);
void writelistmagic (void);
void showlistmagic (void);
int findspell (char *s);
int cancast (unit *u, int i);

void magic (void);
void research (unit *u);
void fog (region * r, unit * magician); /*Fuer die Seeschlangen*/
void volcano_setoff (region * r);


/* Monster */

int free_unit_number (void);
unit *make_undead_unit (region * r, faction * f, int n);
unit *make_illsionary_unit (region * r, faction * f, int n);
unit *make_guards_unit (region * r, faction * f, int n);
unit *make_guarded_tower_unit (region *r, faction *f, int n, int m);
unit *make_firedragon_unit (region * r, faction * f, int n);
unit *make_dragon_unit (region * r, faction * f, int n);
unit *make_wyrm_unit (region * r, faction * f, int n);
unit *make_serpent_unit (region * r, faction * f, int n);
unit *make_thief_unit (region * r, faction * f, int n);

unit *createunit (region *r1);
void new_units (void);

int richest_neighbour (region * r);
void plan_monsters (void);
void age_unit (region * r, unit * u);

void spawn_monsters (void);

/* Bewegung */

int getdirection (void);

int weight (unit *u);
int capacity (region *r, ship *sh);
void leave (region *r, unit *u);
void movement (void);
void drowning_men (region *r);
int sphericaldistance(int x1, int y1,int x2,int y2); /*creation.c*/

/* Reports */

extern char *directions[MAXDIRECTIONS];
extern char *roadinto[MAXTERRAINS];
extern char *trailinto[MAXTERRAINS];

void rparagraph (char *s, int indent, int mark);
char *spunit (faction *f, region *r, unit *u, int battle);
void rpunit (faction *f, region *r, unit *u, int indent);

void describe (region *r, faction *f);
int roadto (region * r, region * r2);
char *gamedate (faction *f);

void reports (void);
void report (faction * f);
void report_computer (faction * f);

/* Speichern und Laden */

int cfopen (char *filename, char *mode);
void getbuf (void);
int readorders (void);
int writeorders (void);
void initgame (int lean);
void showaddresses (void);
void writeaddresses (void);
void writegame (void);
void writesummary (void);
int netaddress (char *s);
void writenetaddresses (void);

/* Lernen und Lehren */

void learn (void);
void teach (region * r, unit * u);
int findskill (char *s);

/* Terrain */

void makeblock (int x1, int y1);
int mainterrain (region * r);


int init_logfile(char* mode);
int close_logfile(void);
int log_to_file(char* buffer);

/*WINNIE*/
/* Verwaltungseinheiten */

/* PUBLIC ("gib Einheit") */
int faction_may_accept_unit_admin(faction *f, unit *u);
/* PUBLIC ("rekrutiere") */
int unit_may_recruit_person_admin(unit *u);
/* PUBLIC ("gib Person", auch zwischen Parteien) */
int unit_may_add_person_admin(unit *u, int *skills);

  /* Testet, ob eine weitere Person mit "skills" Talenten in diese Einheit
     gestopft werden darf (geht nur mit "gib Personen").
  */
/* PRIVATE */
int faction_may_add_person_admin(faction *f, int *skills);
  /* Gibt die maximal verwaltbare Anzahl an Einheiten fuer diese Partei zurueck */
/* PUBLIC (wg. Reports), sollte aber ausserhalb obiger drei Publics irrelevant sein. */
int get_maxadmin_for_faction(faction *f);

  /* Ermittelt, wieviele Verwaltungseinheiten ein Magier fuer diese Partei benoetigt */
/* PRIVATE, benutze "faction_may_accept_unit_admin()" und faction_may_add_person_admin() */
int get_admin_used_per_magician(faction *f);

  /* Wie viele Personen passen fuer das Zieltalent noch in die Partei ? */
/* PRIVATE */
int n_allowed_persons_for_faction_and_skill(faction *f,const int skill_index);

#endif
