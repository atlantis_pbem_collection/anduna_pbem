/* Anduna PBEM Host 6.1 Copyright (C) 1999-2003 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

#include "atlantis.h"
 
#define COMBATEXP               10

#define WEAPON_BONUS             2
#define BUILDING_BONUS           2

#define CATAPULT_HITS           20
//#define DEBUG_COMBAT

/* Mit #define DEBUG_COMBAT kann man das Schreiben von Kampf-Debugs 
   ermoeglichen. Diese erhalten alle Spieler, welche SENDE DEBUG gesetzt 
   haben. Der Debug enthaelt auch die Kampftalente der Gegner, so dass 
   damit Infos, welche missbraucht werden koennen, an die Spieler 
   abgegeben werden.  */

enum
{
  A_NONE,
  A_CHAIN,
  A_PLATE,
  A_DRAGON_PLATE,
  A_INVULNERABLE,
  A_FIREDRAGON,
  A_DRAGON,
  A_WYRM,
  MAXARMOR
};

/* Der Wert ist ein Teil von 10000.  */
#define MAX_ARMOR_VALUE      10000
#define ARMOR_BASE_MODIFICATION 500 /*Magie peppt Armor in 5%-Schritten auf*/
int armor_value[MAXARMOR] =
{
  0,
  3333,   /* 1/3        */
  6667,   /* 2/3        */
  9500,   /* 19/20      */
  9990,   /* 9990/10000 */
  9500,   /* 19/20      */
  9800,   /* 49/50      */
  9900,   /* 99/100     */
};

char attack_default[MAXTYPES] =
{
  -2,                           /* mensch */
  0,                            /* untote */
  0,                            /* illusionen - kaempfen nie! */
  3,                            /* feuerdrachen */
  5,                            /* drachen */
  7,                            /* wyrm */
  -2,				/* guards */
  4,				/* serpent */
  -2,				/* thief */
  3,                            // water elemental
};

#define MAX_ADVANTAGE           5

char attack_table[MAX_ADVANTAGE] =
{
   1,
  10,
  25,
  40,
  49,
};

enum
  {
    SI_DEFENDER,
    SI_ATTACKER,
  };

void
battlerecord (char *s)
{
  faction *f;

  for (f = factions; f; f = f->next)
    if (f->seesbattle)
      addstrlist (&f->battles, s);
      /*sparagraph (&f->battles, s, 0, 0);*/
}

#ifdef DEBUG_COMBAT
#define battledebug(a) _battledebug(a)
#else
#define battledebug(a)
#endif

void
_battledebug (char *s)
{
  faction *f;

  for (f = factions; f; f = f->next)
    if (f->seesbattle)
      addstrlist (&f->debug, s);
}

void
battlepunit (region * r, unit * u)
{
  faction *f;

  for (f = factions; f; f = f->next)
    if (f->seesbattle)
      addstrlist (&f->battles, spunit (f, r, u, 1));
}

typedef struct troop
  {
    struct troop *next;
    unit *unit;
    int lmoney;
    char status;
    int side;
    int weapon;
    char attacked;
    char missile;
    char skill;
    char armor;
    char behind;
    char inside;
    char riding;
    char reload;
    char canheal;
    char runesword;
    char flamesword;
    char power;
    char shieldstone;
    char demoralized; // Anhaltender Malus
    char dazzled;  // Malus fuer eine Runde
    char paused; /*Setzt soviele Runden aus, wie hierin steht*/
    char skill_modification; /*Wird von der Magie beeinflusst*/
    char armor_modification; /*Wird von der Magie beeinflusst*/
    char dead; /*Cannot be healed*/
    char amulet_of_life;
    int manaleft; /*The mana the magician can still spend*/
  }
troop;

char buf2[NAMESIZE + 20];
int ntroops;
troop *ta;
troop attacker, defender;
int initial[2];
int left[2];
int infront[2];
int toattack[2];
int shields[2];
int runeswords[2];
int flameswords[2];

troop **
maketroops (troop ** tp, unit * u, int mainterrain)
{
  int i;
  troop *t;
  static int skills[MAXSKILLS];
  static int items[MAXITEMS];
  int mana_of_unit;

  /* Kopiere Daten der unit. skills ist nun immer effskill(), und items
     koennen nun auf jede einzelne troop verteilt werden */
  for (i = 0; i != MAXSKILLS; i++)
    skills[i] = effskill (u, i);
  memcpy (items, u->items, sizeof items);
  mana_of_unit = u->mana;

  /* Einteilung der Einheit in der Schlacht */
  left[u->side] += u->number;
  if (u->status == ST_FIGHT)
    infront[u->side] += u->number;

  /* Erzeugung von jedem einzelnen troop (ein Kaempfer) */
  for (i = u->number; i; i--)
    {
      t = cmalloc (sizeof (troop));
      memset (t, 0, sizeof (troop));
      addlist2 (tp, t);

      /* Einheitsinformation wird kopiert. Als Kampftalent wird der Wert aus
         der attack_default[] Tabelle genommen. Dies ist das Kampftalent aller
         nicht-menschlichen Einheiten (Zombies, Drachen etc).  */
      t->unit = u;
      t->side = u->side;
      t->behind = (u->status != ST_FIGHT);
      t->skill = attack_default[u->type];
      t->armor = A_NONE;

      /* Verteidiger in BURGEN setzen bekommen keinen Pferdebonus, sondern einen Burgenbonus. Greifen die
         Einwohner einer Burg selber an, bekommen sie keinen Burgenbonus, sondern den Pferdebonus.  Dies ist
         wichtig, weil der Pferdebonus wiederum bestimmt, was mit den Speeren geschieht - vgl. hits
         (). t->inside wird dort gebraucht, um zu sehen, ob die jeweiligen Gegner den Pferdebonus bekommen
         oder nicht.  */
/*CHG BUILDING !*/
      if (u->building && u->building->sizeleft && (u->building->kind == BD_CASTLE)
          && u->side == SI_DEFENDER)
        {
          t->inside = 1;
          u->building->sizeleft--;
        }

      /* PFERDE kann man gebrauchen, wenn man ein Pferd hat, ein Reiten Talent von 2 hat, in einer Ebene
         (vgl. mainterrain) kaempft, und sich nicht in einer Burg verteidigt (!t->inside).  Mit Pferden kann
         man Runenschwerter, Schwerter und Speere verwenden!  Als Angreifer aus einer Burg heraus ist
         t->inside==0 und somit kann ein Pferd verwendet werden!  */

      /* WAFFEN - in der Reihenfolge ihrer Prioritaeten. Nur eine Waffe pro Mann! Also werden Schuetzen nicht
         mit Schwertern ausgeruestet.  Hier wird t->skill neu gesetzt, dh. alle Boni darf man erst nachher
         addieren! */

      /* MAGISCHE GEGENSTAENDE */
      if (items[I_AMULET_OF_HEALING] > 0)
        {
          t->canheal = 1;
          items[I_AMULET_OF_HEALING]--;
        }

      if (items[I_RING_OF_POWER])
        {
          t->power = 1;
          items[I_RING_OF_POWER]--;
        }

      if (items[I_SHIELDSTONE])
        {
          t->shieldstone = 1;
          items[I_SHIELDSTONE]--;
        }
      if (items[I_AMULET_OF_LIFE])
        {
          t->amulet_of_life = 1;
          items[I_AMULET_OF_LIFE]--;
        }

      if (u->combatspell[CS_DURING] >= 0)
        {
          /* Magier ohne Waffen! */
          t->missile = 1;
          /* Maximal zu verbratendes Mana wird gesetzt */
          if (u->combatspellmana[CS_DURING] > 0) {
            t->manaleft = min (mana_of_unit, u->combatspellmana[CS_DURING]);
            mana_of_unit -= t->manaleft;
          } else {
            t->manaleft = u->mana / max(1, u->number);
          }
        }
      else if (items[I_RUNESWORD] && skills[SK_SWORD])
        {
          /* Runenschwerter */
          t->weapon = I_SWORD;
          t->skill = skills[SK_SWORD] + 2 * (1 + t->power);      /* Bonus fuer Runenschwert */
          t->runesword = 1;
          items[I_RUNESWORD]--;
          runeswords[u->side]++;
	  /* ... mit Pferd */
          if (items[I_HORSE] && skills[SK_RIDING] >= 2
              && mainterrain == T_PLAIN
              && !t->inside)
            {
              t->riding = 1;
              items[I_HORSE]--;
            }
        }
      else if (items[I_FLAMESWORD] && skills[SK_SWORD])
        {
          /* Flammenschwerter */
          t->weapon = I_SWORD;
          t->skill = skills[SK_SWORD] + 2 * (1 + t->power);      /* Bonus fuer Flammenschwert */
          t->flamesword = 1;
          items[I_FLAMESWORD]--;
          flameswords[u->side]++;
	  /* ... mit Pferd */
          if (items[I_HORSE] && skills[SK_RIDING] >= 2
              && mainterrain == T_PLAIN
              && !t->inside)
            {
              t->riding = 1;
              items[I_HORSE]--;
            }
        }
      else if (items[I_CATAPULT] && skills[SK_CATAPULT])
        {
          /* Katapult */
          t->weapon = I_CATAPULT;
          t->missile = 1;
          t->skill = skills[SK_CATAPULT];
          items[I_CATAPULT]--;
        }
      else if (items[I_LONGBOW] && skills[SK_LONGBOW])
        {
          /* Langbogen */
          t->weapon = I_LONGBOW;
          t->missile = 1;
          t->skill = skills[SK_LONGBOW];
          items[I_LONGBOW]--;
        }
      else if (items[I_CROSSBOW] && skills[SK_CROSSBOW])
        {
          /* Armbrust */
          t->weapon = I_CROSSBOW;
          t->missile = 1;
          t->skill = skills[SK_CROSSBOW];
          items[I_CROSSBOW]--;
        }
      else if (items[I_SPEAR] && skills[SK_SPEAR])
        {
          /* Speer */
          t->weapon = I_SPEAR;
          t->skill = skills[SK_SPEAR];
          items[I_SPEAR]--;
	  /* ... mit Pferd */
          if (items[I_HORSE] && skills[SK_RIDING] >= 2
              && mainterrain == T_PLAIN
              && !t->inside)
            {
              t->riding = 1;
              items[I_HORSE]--;
            }
        }
      else if (items[I_SWORD] && skills[SK_SWORD])
        {
          /* normales Schwert */
          t->weapon = I_SWORD;
          t->skill = skills[SK_SWORD];
          items[I_SWORD]--;
	  /* ... mit Pferd */
          if (items[I_HORSE] && skills[SK_RIDING] >= 2
              && mainterrain == T_PLAIN
              && !t->inside)
            {
              t->riding = 1;
              items[I_HORSE]--;
            }
        }

      /* RUESTUNG: */
      if (items[I_CLOAK_OF_INVULNERABILITY])
        {
          t->armor = A_INVULNERABLE;
          items[I_CLOAK_OF_INVULNERABILITY]--;
        }
      else if (u->type == U_FIREDRAGON)
        {
          t->armor = A_FIREDRAGON;
        }
      else if (u->type == U_DRAGON)
        {
          t->armor = A_DRAGON;
        }
      else if (u->type == U_WYRM)
        {
          t->armor = A_WYRM;
        }
      else if (u->type == U_SERPENT)
        {
          t->armor = A_FIREDRAGON;
        }
      else if (items[I_DRAGON_PLATE])
        {
          t->armor = A_DRAGON_PLATE;
          items[I_DRAGON_PLATE]--;
        }
      else if (items[I_PLATE_ARMOR])
        {
          t->armor = A_PLATE;
          items[I_PLATE_ARMOR]--;
        }
      else if (items[I_CHAIN_MAIL])
        {
          t->armor = A_CHAIN;
          items[I_CHAIN_MAIL]--;
        }

    }

  return tp;
}

troop **
maketroops_water_elemental (troop ** tp, unit * u, region*r, int number)
{
  int i;
  troop *t;
  unit* uw; // dummy Einheit fuer die Elementare

  if (!tp) return 0; // tough luck
  if (!u || number <= 0) return tp; // make sure we're OK

  // Dummy Einheit fuer Elementare erzeugen
  uw = createunit(r);
  uw->number = number;
  uw->type = U_WATER_ELEMENTAL;
  uw->side = u->side;
  uw->faction = u->faction;
  uw->name = strings[(number = 1 ? ST_WATER_ELEMENTAL:ST_WATER_ELEMENTALS)][u->faction->language];

  left[u->side] += number;
  // infront[u->side] += number; // Elementare kaempfen hinten

  /* Erzeugung von jedem einzelnen troop (ein Kaempfer) */
  for (i = number; i; i--)
    {
      t = cmalloc (sizeof (troop));
      memset (t, 0, sizeof (troop));
      addlist2 (tp, t);

      /* Einheitsinformation wird kopiert. Als Kampftalent wird der Wert aus
         der attack_default[] Tabelle genommen. Dies ist das Kampftalent aller
         nicht-menschlichen Einheiten (Zombies, Drachen etc).  */
      t->unit = uw;
      t->side = u->side;
      t->behind = 1;
      t->skill = attack_default[u->type];

      /* Verteidiger in BURGEN setzen bekommen keinen Pferdebonus, sondern einen Burgenbonus. Greifen die
         Einwohner einer Burg selber an, bekommen sie keinen Burgenbonus, sondern den Pferdebonus.  Dies ist
         wichtig, weil der Pferdebonus wiederum bestimmt, was mit den Speeren geschieht - vgl. hits
         (). t->inside wird dort gebraucht, um zu sehen, ob die jeweiligen Gegner den Pferdebonus bekommen
         oder nicht.  */
/*CHG BUILDING !*/
      if (u->building && u->building->sizeleft && (u->building->kind == BD_CASTLE)
          && u->side == SI_DEFENDER)
        {
          t->inside = 1;
          u->building->sizeleft--;
        }

      t->missile = 1; // Wasserelementare kaempfen hinten
      t->armor = A_DRAGON_PLATE;

    }

  return tp;
}

int
contest (int attacker, int defender)
{
  int attacker_advantage;

  /* Der kleinste Wert ist -2 ("um mehr als 1 schlechter"). Um den array
     attack_table[] verwenden zu koennen, addieren wir also +2, und
     beschraenken die moeglichen Loesungen.  */

  attacker_advantage = attacker - defender + 2;

  attacker_advantage = max (attacker_advantage, 0);
  attacker_advantage = min (attacker_advantage, MAX_ADVANTAGE - 1);
  sprintf (buf, "  A:%d->V:%d R:%d.",
           attacker, defender, attacker_advantage
          );
  battledebug (buf);

  return rand () % 100 < attack_table[attacker_advantage];
  
}

int
hits (void)
{
  int k=-1;

  /* Truppen mit Schusswaffen, die sich ohne Waffen verteidigen muessen.  */
  if (defender.weapon == I_CROSSBOW
      || defender.weapon == I_LONGBOW
      || defender.weapon == I_CATAPULT)
    defender.skill = attack_default[U_MAN];

  /* Ist der Angegriffene nicht in einer Burg und wird von einem Berittenen angegriffen, so erhaelt der
     berittene Angreifer den Pferdebonus.  Verwendet er zudem noch einen Speer, erhaelt er noch zusaetzlich
     den Lanzenbonus.  Verwender der Verteidiger allerdings einen Speer, erhaelt dieser den Pikenbonus.  Dies
     alles ist fuer Schuetzen unwesentlich, so dass defender.skill nachher einfach nicht gebraucht wird.  */
  if (!defender.inside && attacker.riding)
    {
      attacker.skill += 2;      /* Pferdebonus fuer Angreifer */
      if (attacker.weapon == I_SPEAR)
        attacker.skill += 1;    /* Lanzenbonus fuer berittene Angreifer */
      if (defender.weapon == I_SPEAR)
        defender.skill += 1;    /* Pikenbonus gegen berittene Angreifer */
    }

  /* Berittene Verteidiger erhalten auch den Pferdebonus, es sei denn der Angreifer verteidigt eine Burg und
   greift von dort her an. Das bedeuted, dass Pferde beim Sturm auf eine Burg unnuetz sind.  Dies hilft nichts
   gegen Schusswaffen, deswegen kann defender.skill bei Schusswaffen weggelassen werden.  */
  if (!attacker.inside && defender.riding)
    defender.skill += 2;

  /* Effekte von Magie - Verwirrte sind nicht einfacher mit Schusswaffen zu treffen, deswegen wird
     defender.skill hier erhoeht, und bei Schusswaffen nicht verwendet.  */
  attacker.skill -= (attacker.demoralized + attacker.dazzled);
  attacker.skill += attacker.skill_modification;
  defender.skill -= (defender.demoralized + defender.dazzled);
  defender.skill += defender.skill_modification;

  /* Effekte der Waffen: k zeigt an, ob getroffen wurde oder nicht.  */
  switch (attacker.weapon)
    {
      /* Der Angegriffene erhaelt den Burgenbonus, falls er in einer Burg ist.
         Burginsassen erhalten bei einem Angriff keinen Burgenbonus, da 
         defender.inside nicht gesetzt ist!  */
    case 0:
    case I_SWORD:
    case I_SPEAR:
      k = contest (attacker.skill, defender.skill + (defender.inside ? 2 : 0));
      break;

      /* Die Opfer von Schusswaffen koennen defender.skill nicht verwenden!
        Burgen schuetzen vor Schuetzen. :) */
    case I_CROSSBOW:
      k = contest (attacker.skill, 0 + (defender.inside ? 2 : 0));
      break;

    case I_CATAPULT:
      k = contest (attacker.skill, 1 + (defender.inside ? 2 : 0));
      break;

    case I_LONGBOW:
      k = contest (attacker.skill, 2 + (defender.inside ? 2 : 0));
      break;

    default:
      assert (0);
      break;
    }

  /* Ein Treffer (k==1) kann durch eine Ruestung annuliert werden; 
     die Ruestung schuetzt vor einem Treffer mit einer gewissen Chance.
     Ein Mantel der Unverletzlichkeit schuetzt allerdings nicht vor einem
     Runenschwert.  */
     
  /* Die Panzerung kann magisch verstaerkt werden, wird allerdings nie
     staerker als der Mantel der Unverwundbarkeit */
  if (k && !(attacker.runesword && defender.armor == A_INVULNERABLE)
      && !(attacker.flamesword && defender.armor == A_INVULNERABLE)
      && rand () % MAX_ARMOR_VALUE < min(armor_value[(int) defender.armor]
      + defender.armor_modification * ARMOR_BASE_MODIFICATION, A_INVULNERABLE))
    k = 0;
  
  /* Eine getroffene Illusion loest sich immer auf.  */
  if (!k && defender.unit && defender.unit->type == U_ILLUSION)
    k = 1;
  /* Eine Illusion trifft nie.  */
  if (k && attacker.unit && attacker.unit->type == U_ILLUSION)
    k = 0;

  assert (k >= 0);

  /* debug */
  sprintf (buf, "A:%d(%s %+d)->V:%d(%s %+d, R:%dI:%s) und %s.",
           attacker.unit ? attacker.unit->no : 0,
           attacker.weapon ? strings[itemnames[0][attacker.weapon]][0] : "unbewaffnet",
           attacker.skill,
           defender.unit ? defender.unit->no : 0,
           defender.weapon ? strings[itemnames[0][defender.weapon]][0] : "unbewaffnet",
           defender.skill,
           defender.armor,defender.inside ? "t":"f",
           k ? "*trifft*" : "trifft NICHT");
  battledebug (buf);
  return k;
}

int
validtarget (int i)
{
  return !ta[i].status &&
    ta[i].side == defender.side &&
    (!ta[i].behind || !infront[defender.side]);
}

int
canbedemoralized (int i)
{
  return validtarget (i) && !ta[i].demoralized;
}

int
canbepaused (int i)
{
  return validtarget (i) && !ta[i].paused;
}

int
canbedazzled (int i)
{
  return validtarget (i) && !ta[i].dazzled;
}

int
canbeheavyarrowed (int i)
{
  return !ta[i].status && ta[i].side == defender.side &&
  ta[i].behind && !ta[i].dazzled && ta[i].missile && ta[i].weapon != I_CATAPULT;
}

int
hasironwares (int i)
{
  int n = 0;
  if (!ta[i].status && ta[i].side == defender.side) 
  {
    if (ta[i].weapon == I_SWORD) n += rawquantity[I_SWORD];
    if (ta[i].armor == A_CHAIN) n += rawquantity[I_CHAIN_MAIL];
    if (ta[i].armor == A_PLATE) n += rawquantity[I_PLATE_ARMOR];
    return n;
  }
  else return 0;
}

int
canberemoralized (int i)
{
  return !ta[i].status &&
    ta[i].side == attacker.side &&
    ta[i].demoralized;
}

int
selecttarget (void)
{
  int i;

  assert (ntroops);
  do
    i = rand () % ntroops;
  while (!validtarget (i));

  return i;
}

void
terminate (int i)
{
  if (!ta[i].attacked)
    {
      ta[i].attacked = 1;
      toattack[defender.side]--;
    }

  ta[i].status = 1;
  left[defender.side]--;
  if (infront[defender.side])
    infront[defender.side]--;
  if (ta[i].runesword)
    runeswords[defender.side]--;
  if (ta[i].flamesword)
    flameswords[defender.side]--;
}

/* Kampfzauber, wie Blitzschlag, Feuerball, Hand des Todes, Schwarzer Wind,
   		Sonnenfeuer machen alle DOZAP */

void
dozap (int n, int z)
{
  int di, d=0;

  n = lovar (n * (1 + attacker.power));
  n = min (n, left[defender.side]);

  while (--n >= 0)
    {
      di = selecttarget ();
      /* Entweder sind sie draussen, oder sie sind drinnen und haben eine 
      Chance, den Spruch zu ueberleben. - Ausser Blitzschlag */
      if ((!ta[di].inside         || z == SP_LIGHTNING_BOLT || rand () % 100 > ZAP_SURVIVAL) && 
          (!ta[di].amulet_of_life || rand () % 100 > ZAP_SURVIVAL_AMULET))
	{
	  d++;
	  terminate (di);
	}
    }
  
  sprintf (buf2, "; %d Gegner %s", d, (d != 1) ? "starben" : "starb");
  scat (buf2);
}

void
dofreeze (int n)
{
  int di, d=0, f=0;

  n = rand() % n + 1; // lineare Verteilung
  // n = lovar (n * (1 + attacker.power)); // Kein Ring der Macht beim Einfrieren, nicht der Magier zaubert
  n = min (n, left[defender.side]);

  while (--n >= 0)
    {
      di = selecttarget ();
      if (!ta[di].inside || rand () % 100 > ZAP_SURVIVAL) // Gebaeude schuetzen
	{
	  if (rand() % 2)  {// 50/50 tot oder eingefroren
	    d++;
	    terminate (di);
	  } else {
	    f++;
	    ta[di].paused = rand() % 3 + 2; // 2-4 Runden aussetzen
	  }
	}
    }
  
  sprintf (buf2, "; %d Gegner %s, %d wurde%s zeitweise eingefroren", d, (d != 1) ? "starben" : "starb", f, (f!=1) ? "n":"");
  scat (buf2);
}

/* Abwicklung der Kampfzauber */


// Testen, ob Zauber durchkommt, Manabuchhaltung
// Wenn der Zauber NICHT durchkommt, wird hier schon battlerecord geschrieben, sonst nicht!!
int
combatspellsuccess (int i, int z)
{

  int j;
  
  // Manabuchhaltung hier: Nur wenn wirklich gezaubert wurde, wird das Mana
  // abgezogen --> schlaue Magier.

  if (ta[i].unit->type == U_MAN) // Nur Spieler-Personen verbrauchen Mana
  {
    ta[i].manaleft -= spellmanacost[z];
    ta[i].unit->mana -= spellmanacost[z];
  }

  // Initialisieren der Meldung:
  strcpy (buf, 
	  translate (ST_CASTS_IN_COMBAT, ta[i].unit->faction->language, 
		     unitid (ta[i].unit), 
		     strings[spellnames[z]][ta[i].unit->faction->language]));

  // Testen, ob Schilde vorhanden sind und ein offensiver Zauber gesprochen wird
  if (shields[defender.side] && z!=SP_SHIELD && z!=SP_INSPIRE_COURAGE)
  {
    if (rand () & 1)
      {
        scat (strings[ST_BREAKS_THROUGH_MAGIC_SHIELD][ta[i].unit->faction->language]);
        shields[defender.side] -= 1 + attacker.power;
      }
    else
      {
        scat (strings[ST_BUT_CANCELLED_BY_MAGIC_SHIELD][ta[i].unit->faction->language]);
        battlerecord (buf);
        return 0;
      }
  }
  
  // Testen, ob ein Gegner den Zauber "Negieren" gesetzt hat und das noetige Mana hat.
  
  if (ta[i].unit->type == U_MAN)  // Nur die Zauber von Magiern kann man negieren, 
                                  // nicht aber Drachenfeuer und Untotenpanik
    for (j = 0; j != ntroops; j++)
      if (ta[j].unit->combatspell[CS_DURING] == SP_NEGATE && 
          ta[j].side == defender.side &&
          ta[j].manaleft >= spellmanacost[z]) {
        scat (strings[ST_BUT_NEGATED_BY_OPPONENT_MAGE][ta[i].unit->faction->language]);
        ta[j].manaleft -= spellmanacost[z];
        ta[j].unit->mana -= spellmanacost[z];
        battlerecord (buf);
        return 0;
  }
  
  return 1;
}


void
docombatspell (int i)
{
  int j;
  int z;
  int n, m;

  z = ta[i].unit->combatspell[CS_DURING];
//  puts("Zauber\n");
  
  // Manabuchhaltung: Wenn das Mana hier abgezogen wuerde, waeren die Magier bloed, d.h.
  // wuerden zaubern, auch wenn es nichts bringt. Also erst weiter unten!!
  if (ta[i].unit->type == U_MAN) // Nur Spieler-Personen brauchen Mana
  {
    if (ta[i].manaleft < spellmanacost[z]) return;
  }

  switch (z)
    {
    case SP_NEGATE: break; //wurde oben schon abgehandelt (in combatspellsuccess)
    case SP_HEAL: break; //wird spaeter abgehandelt (nach dem Kampf)
    case SP_SUMMON_WATER_ELEMENTAL: break; // vor dem Kampf abgehandelt
    
    case SP_FREEZE:
      if (combatspellsuccess(i,z)) {
      	dofreeze (5);
      }
      break;

    case SP_LIGHTNING_BOLT:
      if (combatspellsuccess(i,z)) dozap (10,z);
      break;

    case SP_FIREBALL:
      if (combatspellsuccess(i,z)) dozap (50,z);
      break;

//    case SP_HAND_OF_DEATH:
  //    if (combatspellsuccess(i,z)) dozap (50,z);
 //     break;

    case SP_BLACK_WIND:
      if (combatspellsuccess(i,z)) dozap (100,z);
      break;

//    case SP_SUNFIRE:
//      if (combatspellsuccess(i,z)) dozap (500,z);
//      break;

    case SP_CAUSE_FEAR:
      /* anzahl pot. Gegner dank der magie */
      n = lovar (20 * (1 + attacker.power));

      /* anzahl undemoralisierter gegner */
      m = 0;
      for (j = 0; j != ntroops; j++)
        if (canbedemoralized (j))
          m++;

      /* wenn es keine Gegner mehr gibt, zurueck */
      if (!m)
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      if (runeswords[defender.side] && (rand () & 1))
        break;

       
      /* effektive Gegner */
      n = min (n, m);

      sprintf (buf2, "; %d Gegner %s eingeschuechtert", n, 
	       (n != 1) ? "wurden" : "wurde"); 
      scat (buf2);

      while (--n >= 0)
        {
          do
            j = rand () % ntroops;
          while (!canbedemoralized (j));

          ta[j].demoralized = 1;
        }

      break;

    case SP_SEARCH_FOR_SENSE:
      n = lovar (20 * (1 + attacker.power));

      m = 0;
      for (j = 0; j != ntroops; j++)
        if (canbepaused (j))
          m++;

      /* wenn es keine Gegner mehr gibt, return */

      if (!m)
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      /* effektive Gegner */

      n = min (n, m);

      sprintf (buf2, "; %d Gegner %s eine Runde nach, statt zu kaempfen", n, 
	       (n != 1) ? "denken" : "denkt");
      scat (buf2);

      while (--n >= 0)
        {
          do
            j = rand () % ntroops;
          while (!canbepaused (j));

          ta[j].paused = 1;
        }

      break;

    case SP_DAZZLING_LIGHT:
      n = lovar (100 * (1 + attacker.power));

      m = 0;
      for (j = 0; j != ntroops; j++)
        if (canbedazzled (j))
          m++;

      /* wenn es keine Gegner mehr gibt, return */

      if (!m)
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      /* effektive Gegner */

      n = min (n, m);

      sprintf (buf2, "; %d Gegner %s geblendet", n, 
	       (n != 1) ? "wurden" : "wurde");
      scat (buf2);

      while (--n >= 0)
        {
          do
            j = rand () % ntroops;
          while (!canbedazzled (j));

          ta[j].dazzled = 1;
        }

      break;

    case SP_HEAVY_ARROWS:
      n = lovar (20 * (1 + attacker.power));

      m = 0;
      for (j = 0; j != ntroops; j++)
        if (canbeheavyarrowed (j))
          m++;

      /* wenn es keine Gegner mehr gibt, return */

      if (!m)
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      /* effektive Gegner */

      n = min (n, m);

      sprintf (buf2, "; %d gegnerisch%s Schuetz%s schiess%s oft in den Boden", n, 
	       (n != 1) ? "e" : "er",
	       (n != 1) ? "en" : "e",
	       (n != 1) ? "en" : "t");
      scat (buf2);

      while (--n >= 0)
        {
          do
            j = rand () % ntroops;
          while (!canbeheavyarrowed (j));

          ta[j].dazzled = 2;
        }

      break;

    case SP_INSPIRE_COURAGE:
      n = lovar (50 * (1 + attacker.power));

      m = 0;
      for (j = 0; j != ntroops; j++)
        if (canberemoralized (j))
          m++;

      n = min (n, m);

      if (!m) 
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      sprintf (buf2, "; %d Verzweifelte %s ermutigt", n, 
	       (n != 1) ? "wurden" : "wurde");
      scat (buf2);

      while (--n >= 0)
        {
          do
            j = rand () % ntroops;
          while (!canberemoralized (j));

          ta[j].demoralized = 0;
        }

      break;

    case SP_SHIELD:
      if (!combatspellsuccess(i,z)) 
        return;
      shields[attacker.side] += 1 + attacker.power;
      break;

    case SP_RUST:
      n = lovar (50 * (1 + attacker.power));
      m = 0;
      for (j = 0; j != ntroops; j++)
        m += hasironwares (j);
        
      n = min (n,m);
      
      if (!n)
        return;

      if (!combatspellsuccess(i,z)) 
        return;

      sprintf (buf2, "; Waffen und Ruestungen fuer %d Eisenbarren zerfielen zu Rost", n);
      scat (buf2);

      while (n > 0)
        {
          do
            j = rand () % ntroops;
          while (!hasironwares (j));
 
          if (ta[j].weapon == I_SWORD) 
          {
            n -= rawquantity[I_SWORD];
            ta[j].weapon = 0;
            ta[j].unit->items[I_SWORD] -= 1;
          }
          if (ta[j].armor == A_CHAIN)
          {
            n -= rawquantity[I_CHAIN_MAIL];
            ta[j].armor = A_NONE;
            ta[j].unit->items[I_CHAIN_MAIL] -= 1;
          }
          if (ta[j].armor == A_PLATE)
          {
            n -= rawquantity[I_PLATE_ARMOR];
            ta[j].weapon = A_NONE;
            ta[j].unit->items[I_PLATE_ARMOR] -= 1;
          }
        }

        
      break;

    default:
      assert (0);
    }

  /* achtung: sprueche, die abgebrochen werden, weil es keine Gegner
     gibt, erreichen das ende der funktion nicht, sondern machen ein 
     return. Sonst erledigt ein assert(0) das Programm. */

  scat (".");
  battlerecord (buf);
}

void
domonsters (int i)
{
  /* alles ia. progressiv - keine breaks! - also wyrme machen alle 
     sprueche! nachher spruch wieder loeschen, sonst kaempft das 
     monster weiter wie ein magier...  */

  switch (ta[i].unit->type)
    {
    case U_WYRM:

      ta[i].unit->combatspell[CS_DURING] = SP_SHIELD;
      docombatspell (i);
      ta[i].unit->combatspell[CS_DURING] = SP_CAUSE_FEAR;
      docombatspell (i);

    case U_DRAGON:

      ta[i].unit->combatspell[CS_DURING] = SP_FIREBALL;
      docombatspell (i);
      ta[i].unit->combatspell[CS_DURING] = SP_CAUSE_FEAR;
      docombatspell (i);

    case U_FIREDRAGON:
    case U_UNDEAD:

      ta[i].unit->combatspell[CS_DURING] = SP_CAUSE_FEAR;
      docombatspell (i);

      ta[i].unit->combatspell[CS_DURING] = -1;
      break;
      
    case U_WATER_ELEMENTAL:
      ta[i].unit->combatspell[CS_DURING] = SP_FREEZE;
      docombatspell (i);
      ta[i].unit->combatspell[CS_DURING] = -1;
    }
}

void
docatapult (int i)
{
  int n, d, di;

  /* n Leute werden beschossen (normalerweise 20), d Leute davon sterben.  */
  n = min (CATAPULT_HITS, left[defender.side]);
  d = 0;

  while (--n >= 0)
    {
      /* Select defender */
      di = selecttarget ();
      defender = ta[di];
      assert (defender.side == 1 - attacker.side);

      /* If attack succeeds */
      if (hits ())
        {
          d++;
          terminate (di);
        }
    }

  /* buf darf erst nach hits () verwendet werden, weil dort buf
     verwendet wird um battledebug () messages zu erzeugen.  */
  sprintf (buf, "%s feuert ein Katapult ab", unitid (ta[i].unit));
  if (d)
    {
      sprintf (buf2, "; %d Gegner %s", d, (d != 1) ? "starben" : "starb");
      scat (buf2);
    }
  scat (".");
  battlerecord (buf);
}

void
doshot (void)
{
  int ai, di;

  /* Select attacker.  */
  do
    ai = rand () % ntroops;
  while (ta[ai].attacked);

  attacker = ta[ai];
  ta[ai].attacked = 1;
  toattack[attacker.side]--;
  defender.side = 1 - attacker.side;

  ta[ai].dazzled = 0;

  if (attacker.unit)
    {
      /* Nur Bogenschuetzen koennen aus den hinteren Reihen angreifen.  */
      if (attacker.behind &&
          infront[attacker.side] &&
          !attacker.missile)
        return;

      if (attacker.shieldstone)
        shields[attacker.side] += 1 + attacker.power;

      if (attacker.unit->combatspell[CS_DURING] >= 0)
        {
          docombatspell (ai);
          return;
        }

      /* Bei Monstern kann es geschehen, dass sie durch ihre
         Faehigkeiten (Sprueche) alle Feinde toeten, so dass man hier
         nicht weiter gehen darf - sonst bleibt selecttarget ()
         haengen!  Der weitere code zielt nur noch darauf ab, ein
         weiteres target mit konventionellen Waffen zu toeten.  */
      if (attacker.unit->type)
        {
          domonsters (ai);
          if (!left[defender.side])
            return;
        }

      /* Wenn noch nachgeladen werden muss...  */
      if (attacker.reload)
        {
          ta[ai].reload--;
          return;
        }

      /* Falls Schusswaffen geladen sind, werden sie abgeschossen, und
         eine neue Nachladezeit bestimmt.  */
      if (attacker.weapon == I_CATAPULT)
        {
          docatapult (ai);
          ta[ai].reload = 5;
          return;
          /* Schuss mit docatapult () schon gemacht.  */
        }

      /* Langbogen werden nicht geladen.  */
      if (attacker.weapon == I_CROSSBOW)
        ta[ai].reload = 1;
      /* Aber dieser Schuss wird noch geschossen!  */
    }

  /* Select defender.  */
  di = selecttarget ();
  defender = ta[di];
  assert (defender.side == 1 - attacker.side);

  /* If attack succeeds.  */
  if (hits ())
    terminate (di);
}

int
ispresent (faction *f, region *r)
{
  unit *u;

  for (u = r->units; u; u = u->next)
    if (u->faction == f)
      return 1;

  return 0;
}

int
is_defender (unit *u, region *r, unit *attacker, unit *defender) 
{
  /* Einheit defender ist immer angegriffen, auch wenn sie nicht
     identifiziert ist.  */
  if (u == defender)
    return 1;

  /* Wenn es Wachen gibt, werden sie immer auf der Seite der Verteidiger
     kaempfen.  */
  if (u->type == U_GUARDS)
    return 1;

  /* Verteidiger im Kampf sind alle Einheiten welche der Partei des Opfers (Einheit defender) helfen, es sei
     denn, sie helfen gleichzeitig den Angreifern.  In dem Fall sind sie neutral.  Ob sie einer Partei helfen,
     sieht man in isallied ().  */
  if (isallied (u, defender, HL_FIGHT) && !isallied (u, attacker, HL_FIGHT))
    {
      /* Wenn der Status !ST_AVOID ist, kaempfen sie immer mit (es verteidigen so viele wie moeglich).  */
      if (u->status != ST_AVOID)
	return 1;
      else
	{
	  /* Wenn die Einheit (u) und das Opfer (defender) identifiziert und von der selben Partei sind, wird
             u mit angegriffen.  */
	  if (cansee (attacker->faction, r, defender) == 2 
	      && cansee (attacker->faction, r, u) == 2 
	      && defender->faction == u->faction)
	    return 1;

	  /* Allierte kaempfe-nicht Einheiten werden nicht in den Kampf gezogen, auch wenn sie identifiziert
             wurden, da der Angreifer wahrscheinlich nicht weiss, welche Parteien mit seinem Opfer alliert
             sind.  Genauso geht es, wenn defender nicht identifiziert oder andere Einheiten nicht
             identifiziert werden - sie werden nicht in den Kampf verwickelt.  */
	}
    }
  return 0;
}     

int reportcasualtiesdh;
void
reportcasualties (unit * u)
{
  if (!u->dead)
    return;

  if (!reportcasualtiesdh)
    {
      battlerecord ("");
      reportcasualtiesdh = 1;
    }

  if (u->dead == u->number)
    sprintf (buf, "%s wurde ausgeloescht.", unitid (u));
  else
    sprintf (buf, "%s verlor %d.", unitid (u), u->dead);
  battlerecord (buf);
}

void
combat (void)
{
  int i, j, k, n;
  int nfactions;
  int fno;
  int deadpeasants;
  int maxtactics[2];
  int leader[2];
  int lmoney;
  int dh;
  int tacticsdiff;
  int cansomebodyshoot;
  int cansomebodyloot;
  int undeadstronger = 0;
  static int litems[MAXITEMS];
  faction *f, *f2, **fa;
  region *r;
  building *b;
  unit *u, *u2, *u3, *u4;
  troop *t, *troops, **tp;
  strlist *S, *S2;
  char buffer[255];

  puts ("- attackieren... ");

  nfactions = listlen (factions);
  fa = cmalloc (nfactions * sizeof (faction *));

  for (r = regions; r; r = r->next)
    {

      /* Create randomly sorted list of factions */

      for (f = factions, i = 0; f; f = f->next, i++)
        fa[i] = f;
      scramble (fa, nfactions, sizeof (faction *));

      /* ATTACKIERE: u - Angreifer, u2 - Opfer, u3 - Mitangreifer, u4 - die
         Einheiten, welche die Mitangreifer angreifen. 0 - defending, 1 -
         attacking */
      for (fno = 0; fno != nfactions; fno++)
        {
          f = fa[fno];
          for (u = r->units; u; u = u->next)
            if (u->faction == f)
              for (S = u->orders; S; S = S->next)
                if (igetkeyword (S->s) == K_ATTACK)
                  {
		    indicator_tick ();
		    sprintf(buffer,"   %s (%s) attackiert in Region %s.\n",unitid(u),factionid(f),regionid(r));
                    log_to_file(buffer);

                    if (u->type == U_ILLUSION)
                      {
                        mistake2 (u, S, "Illusionen attackieren nicht");
                        continue;
                      }
                    /* Einheit u greift Einheit u2 an.  */
		    u2 = getunit (r, u);

                    if (!u2 && !getunitpeasants)
                      {
                        mistake2 (u, S, "Die Einheit wurde nicht gefunden");
                        continue;
                      }

                    if (u2 && u2->faction == f)
                      {
                        mistake2 (u, S, "Die Einheit ist eine der Unsrigen");
                        continue;
                      }

                    if (isallied (u, u2 ,HL_FIGHT))
                      {
                        mistake2 (u, S, "Die Einheit ist mit uns alliert");
                        continue;
                      }

// Folgende Abfrage faengt ueberfluessige attackiere Befehle ab (oft greifen
// Spieler zur Sicherheit alle Einheiten einer Partei an, die sie sehen 
// koennen, nach dem ersten Befehl sind dann oft die anderen hinfaellig.
// Bislang keine Fehlermeldung.

                    if (u2 && u2->number == 0)
                      {
//                        mistake2 (u, S, "Die Einheit wurde bereits vernichtet");
                        continue;
                      }

                    /* Draw up troops for the battle */
		    if (getunitpeasants) {
		      log_to_file("   Der Kampf gegen die Bauern findet statt.\n");
		    } else {
		      sprintf(buffer,"   Der Kampf gegen %s findet statt.\n",unitid(u2));
                      log_to_file(buffer);
                    }
                    
                    for (b = r->buildings; b; b = b->next)
                      b->sizeleft = b->size;
                    troops = 0;
                    tp = &troops;
                    left[0] = left[1] = 0;
                    infront[0] = infront[1] = 0;
                    runeswords[0] = 0;
                    runeswords[1] = 0;
                    flameswords[0] = 0;
                    flameswords[1] = 0;
                    undeadstronger=0;


                    /* If peasants are defenders */
                    if (!u2)
                      {
                        for (i = r->peasants; i; i--)
                          {
                            t = cmalloc (sizeof (troop));
                            memset (t, 0, sizeof (troop));
                            addlist2 (tp, t);
                          }
                        left[0] = r->peasants;
                        infront[0] = r->peasants;
                      }

                    /* ANGREIFENDE PARTEIEN WERDEN MARKIERT

                       u3 sind alle Einheiten, die den ATTACKIERE Befehl
                       gegeben haben.  Fuer sie ist der u->status
                       unwesentlich.

                       u3 ist ein Mitangreifer, falls u2 eine 0, also Bauern
                       sind, und u3 auch Bauern angreift, also u4 eine 0 ist,
                       oder falls u4 zur selben Partei wie das Opfer u2
                       gehoert, u3 also dieselbe Partei angreift, wie u.

                       u3 ist kein Mitangreifer, falls u3 mit u4 verbuendet
                       ist (der ATTACKIERE Befehl von u3 also ungueltig ist.)

                       Alle Parteien, die nun eine mitangreifende Einheit
                       haben, zaehlen unten als angreifer! */

		    /* Falls die Einheiten oder Parteien diese Runde und in dieser Region schon in Kaempfe
                       verwickelt worden sind, wurde u->attacking / f->attacking nicht geloescht...  */
                    for (f2 = factions; f2; f2 = f2->next)
                      f2->attacking = 0;
                    for (u3 = r->units; u3; u3 = u3->next)
                      u3->attacking = 0;

                    for (u3 = r->units; u3; u3 = u3->next)
                      for (S2 = u3->orders; S2; S2 = S2->next)
                        if (igetkeyword (S2->s) == K_ATTACK && u3->type != U_ILLUSION)
                          {
                            u4 = getunit (r, u3);

                            if ((getunitpeasants && !u2) ||
                                (u4 && u2 && u4->faction == u2->faction &&
                                 !isallied (u3, u4, HL_FIGHT)))
                              {
                                u3->faction->attacking = 1;
                                u3->attacking = 1;
                                S2->s[0] = 0;
                                break;
                              }
                          }

                    /* ALLE EINHEITEN WERDEN ZU ANGREIFERN ODER VERTEIDIGERN
                       EINGETEILT

		       Wenn es Wachen gibt, werden sie immer auf der Seite der
		       Verteidiger kaempfen.

                       Zu den Angreifern gehoeren alle Einheiten u3 der oben
                       markierten Parteien. Einheiten, die nicht kaempfen,
                       kaempfen nur, wenn sie selber den ATTACKIERE Befehl
                       gegeben haben.

                       Zu den Verteidigern gehoeren alle Einheiten u3,
                       fuer die is_defender () den Wert 1 hat.  */

                    for (u3 = r->units; u3; u3 = u3->next)
                      {
                        u3->side = -1;

                        if (!u3->number)
                          continue;

                        if (u3->type != U_GUARDS && u3->type != U_ILLUSION
			    && (u3->attacking 
				|| (u3->faction->attacking
				    && u3->status != ST_AVOID)))
                          {
                            u3->side = SI_ATTACKER;
			    assert (u3->type != U_ILLUSION);
                            tp = maketroops (tp, u3, mainterrain (r));
                          }
                        else if (is_defender (u3, r, u, u2))
                          {
			    u3->side = SI_DEFENDER;
			    tp = maketroops (tp, u3, mainterrain (r));
                          }
                      }

                    /* INITIALISIERUNG */

                    /* If only one side shows up, cancel */
                    if (!left[SI_DEFENDER] || !left[SI_ATTACKER])
                      {
                        *tp = 0;
                        freelist (troops);
                        log_to_file ("    Eine Seite war ohne Personen.\n");
                        continue;
                      }

                    log_to_file ("    Initial attack message wird kreiert.\n");

                    /* Initial attack message */
                    for (f2 = factions; f2; f2 = f2->next)
                      {
                        f2->seesbattle = ispresent (f2, r);
                        if (f2->seesbattle && f2->battles)
			  {
			    addstrlist (&f2->battles, "");
			    addstrlist (&f2->battles, "");
			    addstrlist (&f2->battles, "");
			  }
                      }
                    if (u2) 
                      strcpy (buf2, unitid (u2));
                    else /* if peasants */
                      strcpy (buf2, "die Bauern");
                    sprintf (buf, "%s attackiert %s in %s:", 
			     unitid (u), buf2, regionid (r));
                    battlerecord (buf);
                    battledebug (buf);

                    /* List sides */
                    battlerecord ("");
                    battlepunit (r, u);
                    for (u3 = r->units; u3; u3 = u3->next)
                      if (u3->side == SI_ATTACKER && u3 != u)
                        battlepunit (r, u3);
                    battlerecord ("");
                    if (u2)
                      battlepunit (r, u2);
                    else
                      {
                        sprintf (buf, "%d %s.", r->peasants, 
				 (r->peasants != 1) ? "Bauern" : "Bauer");
                        for (f2 = factions; f2; f2 = f2->next)
                          if (f2->seesbattle)
                            addstrlist (&f2->battles, buf);
                      }
                    for (u3 = r->units; u3; u3 = u3->next)
                      if (u3->side == SI_DEFENDER && u3 != u2)
                        battlepunit (r, u3);
                    battlerecord ("");
                    if(undeadstronger) battlerecord("Die Untoten sind unter dem Bann von Runenschwertern staerker als gewoehnlich.");

// Kampfzauber "Erschaffe Wasserelementare" <mana> wird an dieser Stelle 
// behandelt. Die Gegenseite hat zunaechst keine Moeglichkeit, einzugreifen.
// Ueberhaupt sollte hier alle Prae-Kampfzauber gezaubert werden, denn es ist
// klar, wer wen attackiert und die Taktik ist noch nicht ueberprueft (koennte
// ja durch einen Prae-Kampfzauber auch noch veraendert werden!
                    
                    log_to_file ("    Prae-Kampfzauber.\n");
                    
// Hier die Truppenliste schon einmal temporaer schliessen um ntroops bestimmen zu koennen
                    *tp = 0; // schliessen der Truppenliste (setzt den letzten next zeiger auf 0)

                    ntroops = listlen (troops);
                    ta = cmalloc (ntroops * sizeof (troop));
                    for (t = troops, i = 0; t; t = t->next, i++) {
                      ta[i] = *t;
                    }
                    
                    sprintf(buffer,"   Vor Prae-Kampfzaubern %d Truppen.\n",ntroops);
                    log_to_file(buffer);

// Wiederfinden des Listenendes
//                    tp = (&(ta[ntroops]))->next;
                    
                    for (i = 0; i != ntroops; i++)
                      if (ta[i].unit && ta[i].unit->combatspell[CS_DURING] >= 0) {
// CS_BEFORE ist noch nicht implementiert, wohl aber schon im Datenfile vorgesehen
// wenn es drin ist, dann muss auch der assert(0) rein, um sicherzugehen, dass
// kein Zauber uebersehen wird!
//                      	 switch (ta[i].unit->combatspell[CS_BEFORE]) {
                      	 switch (ta[i].unit->combatspell[CS_DURING]) {
                      	   case SP_SUMMON_WATER_ELEMENTAL:
                      	       // manaleft enthaelt schon die maximal zu verbratende menge mana, j ist Anzahl Elementare
                      	       // Beruecksichtigen der max. Anzahl nach magietalent
                               j = (ta[i].manaleft/spellmanacost[SP_SUMMON_WATER_ELEMENTAL]); 
                               j = min (j, effskill(ta[i].unit,SK_SCHOOL_OF_WATER)*10);
                               // tatsaechlich verbrauchtes Mana abziehen
                               ta[i].unit->mana -= j*spellmanacost[SP_SUMMON_WATER_ELEMENTAL];
                               ta[i].manaleft -= j*spellmanacost[SP_SUMMON_WATER_ELEMENTAL];
                               // Truppen machen
                               tp = maketroops_water_elemental (tp, ta[i].unit,r, j);
                               // melden
                               log_to_file ("Wasserelementare erzeugt!\n");
                               sprintf(buf, "%s beschwoert %d Wasserelementare.\n", unitid(ta[i].unit),j);
                               battlerecord(buf);
                               
                      	     break;
//                      	 case default:
//                      	   assert(0);
                      	 }
                      }
                

// Die original Truppenliste wird durcheinandergewuerfelt, ab jetzt koennen keine
// Truppen mehr hinzugefuegt werden!

                    *tp = 0; // schliessen der Truppenliste

                    log_to_file ("    Truppenarray wird kreiert.\n");

                    /* Set up array of troops */
                    ntroops = listlen (troops);
                    ta = cmalloc (ntroops * sizeof (troop));
                    for (t = troops, i = 0; t; t = t->next, i++)
                      ta[i] = *t;
                    freelist (troops);
                    scramble (ta, ntroops, sizeof (troop));

                    sprintf(buffer,"   Endgueltig %d Truppen.\n",ntroops);
                    log_to_file(buffer);


                    initial[SI_DEFENDER] = left[SI_DEFENDER];
                    initial[SI_ATTACKER] = left[SI_ATTACKER];
                    shields[SI_DEFENDER] = 0;
                    shields[SI_ATTACKER] = 0;
                    if (runeswords[SI_DEFENDER]||runeswords[SI_ATTACKER]) undeadstronger = 1;
                    
                    /* set up loot array */
                    lmoney = 0;
                    memset (litems, 0, sizeof litems);

// Ab hier beginnt der Kampf

                    log_to_file ("    Taktik wird ueberprueft.\n");

                    /* Does one side have an advantage in tactics? */
                    // Also enhance undead if runesword is present
                    maxtactics[SI_DEFENDER] = 0;
                    maxtactics[SI_ATTACKER] = 0;
                    for (i = 0; i != ntroops; i++)
                      if (ta[i].unit)
                        {
                          if (ta[i].unit->type == U_UNDEAD && undeadstronger) ta[i].skill+=1;
                          j = effskill (ta[i].unit, SK_TACTICS);

                          if (maxtactics[ta[i].side] < j)
                            {
                              leader[ta[i].side] = i;
                              maxtactics[ta[i].side] = j;
                            }
                        }
                    if (maxtactics[0]==0) maxtactics[0]=-2;
                    if (maxtactics[1]==0) maxtactics[1]=-2;
                    tacticsdiff=abs (maxtactics[0]-maxtactics[1]);
                    attacker.side = -1;
                    if (maxtactics[SI_DEFENDER] > maxtactics[SI_ATTACKER])
                      attacker.side = 0; /*SI_DEFENDER*/
                    if (maxtactics[SI_ATTACKER] > maxtactics[SI_DEFENDER])
                      attacker.side = 1;  /*SI_ATTACKER*/

                    /* Better leader gets free round of attacks */
                    if (attacker.side >= 0)
                      {
                        /* Note the fact in the battle report */
                        if (attacker.side)
                          sprintf (buf, "%s hat einen taktischen Vorteil!", 
				   unitid (u));
                        else if (u2)
                          sprintf (buf, "%s hat einen taktischen Vorteil!", 
				   unitid (u2));
                        else
                          sprintf (buf, "Die Bauern haben einen "
				   "taktischen Vorteil!");
                        battlerecord (buf);
                        log_to_file("    ");
                        log_to_file(buf);
                        log_to_file("\n");

                        /* Number of troops to attack */
                        toattack[attacker.side] = 0;
                        cansomebodyshoot = 0;
                        for (i = 0; i != ntroops; i++)
                          {
			    /* Mark troops as having attacked, unless
                               they are of the attackers side, in
                               which case they are allowed to attack.  */
                            ta[i].attacked = 1;
                            /* TP: Taktik wurde entschaerft, der Freischlag
                               wird nur noch mit einer Wahrscheinlichkeit von
                               10%*Differenz der besten Taktik-Talente gegeben*/
                            if ( (ta[i].side == attacker.side) &&
                             (rand () % 10 < tacticsdiff ) )
                              {
                                ta[i].attacked = 0;
                                toattack[attacker.side]++;
                                cansomebodyshoot = 1;
                              }
                          }

                        /* Do round of attacks.  */
                        sprintf(buffer,"     Taktikerrunde mit %d tatsaechlichen Angreifern und %d Verteidigern\n",toattack[attacker.side], left[defender.side]);
                        log_to_file(buffer);
                        if (cansomebodyshoot)
                         do {
                          doshot ();
                         } while (toattack[attacker.side] && left[defender.side]);
                      }

                    /* Handle main body of battle.  Reset the counters
                       on the attacks yet to happen.  */
                    toattack[SI_DEFENDER] = 0;
                    toattack[SI_ATTACKER] = 0;
                    while (left[defender.side])
                      {
                        /* Ende einer Kampfrunde? */
                        if (toattack[SI_DEFENDER] == 0
                            && toattack[SI_ATTACKER] == 0)
                          {
                            /* debug */
                            sprintf (buf, "Angreifer: %d - Verteidiger: %d.",
                                     left[SI_ATTACKER],
                                     left[SI_DEFENDER]);
                            battledebug (buf);
                            log_to_file("    ");
                            log_to_file(buf);
                            log_to_file("\n");
                            /* markiere Angreifer */
                            for (i = 0; i != ntroops; i++)
                              {
				/* status != 0 verhindert Attacken!
                                   Alle Einheiten haben angegriffen,
                                   ausser die Einheiten, welche
                                   status==0 haben.  */
                                ta[i].attacked = 1;
                                if (!ta[i].status)
                                  {
                                    ta[i].attacked = 0;
                                    toattack[ta[i].side]++;
				/* Einheiten, die aussetzen, werden markiert*/
                                    if (ta[i].paused > 0) 
                                      {
                                      	ta[i].paused--;
                                      	ta[i].attacked = 1;
                                      	toattack[ta[i].side]--;
                                      }
                                  }
                              }
                          }

                        /* Naechster Kaempfer attackiert, solange bis
                           alle Einheiten angegriffen haben.  */
                        doshot ();
                      }

                    /* Report on winner */
                    if (attacker.side == SI_ATTACKER) /*TPWORK*/
                      sprintf (buf, "%s gewinnt die Schlacht!", unitid (u));
                    else if (u2)
                      sprintf (buf, "%s gewinnt die Schlacht!", unitid (u2));
                    else
                      strcpy (buf, "Die Bauern gewinnen die Schlacht!");
                    battlerecord (buf);
                    battledebug ("");
                    battledebug (buf);
                    battledebug ("");
                    log_to_file ("\n     ");
                    log_to_file (buf);
                    log_to_file ("\n");

                    /* Can wounded be healed? */
                    n = 0;
                    for (i = 0; i != ntroops &&
                         n != initial[attacker.side] -
                         left[attacker.side]; i++)
                      if (!ta[i].status && ta[i].canheal)
                        {
                         // erst die amulette! (die verbrauchen kein mana)
                         if (ta[i].unit && ta[i].unit->items[I_AMULET_OF_HEALING]) {
                          k = lovar (50 * (1 + ta[i].power));
                          k = min (k, initial[attacker.side] -
                                   left[attacker.side] - n);

                          sprintf (buf, "%s heilt mit seinem Amulett %d toedlich Verwundete.",
                                   unitid (ta[i].unit), k);
                         }
                         else if (ta[i].unit && ta[i].unit->spells[SP_HEAL] && ta[i].manaleft) {
                          k = ta[i].manaleft / spellmanacost[SP_HEAL];
                          k = min (k, initial[attacker.side] -
                                   left[attacker.side] - n);
                          ta[i].unit->mana -= (k * spellmanacost[SP_HEAL]);
                          sprintf (buf, "%s zaubert und heilt %d toedlich Verwundete.",
                                   unitid (ta[i].unit), k);
                         }
                                                        
                          battlerecord (buf);

                          n += k;
                        }

                    while (--n >= 0)
                      {
                        do
                          i = rand () % ntroops;
                        while (!ta[i].status || ta[i].side != attacker.side);

                        ta[i].status = 0;
                        /* unheilbar tote verbrauchen Heilung, werden aber nicht
                           wiederbelebt */
                        if(ta[i].dead) ta[i].status = 1; 
                      }

                    /* Count the casualties */
                    deadpeasants = 0;
                    for (u3 = r->units; u3; u3 = u3->next)
                      u3->dead = 0;

                    for (i = 0; i != ntroops; i++)
                      if (ta[i].unit)
                        ta[i].unit->dead += ta[i].status;
                      else
                        deadpeasants += ta[i].status;

                    /* Report the casualties */
                    reportcasualtiesdh = 0;
		    if (attacker.side)
                      {
                        reportcasualties (u);

                        for (u3 = r->units; u3; u3 = u3->next)
                          if (u3->side == 1 && u3 != u)
                            reportcasualties (u3);
                      }
                    else
                      {
                        if (u2)
                          reportcasualties (u2);
                        else if (deadpeasants)
                          {
                            battlerecord ("");
                            reportcasualtiesdh = 1;
                            sprintf (buf, "Es sterben %d Bauern.", deadpeasants);
                            battlerecord (buf);
                          }

                        for (u3 = r->units; u3; u3 = u3->next)
                          if (u3->side == 0 && u3 != u2)
                            reportcasualties (u3);
                      }

                    /* Dead peasants */
                    k = r->peasants - deadpeasants;
                    j = distribute (r->peasants, k, r->money);
                    lmoney += r->money - j;
                    r->money = j;
                    r->peasants = k;

                    /* Adjust units */
                    for (u3 = r->units; u3; u3 = u3->next)
                      {
                        k = u3->number - u3->dead;
                        if (u3->side == defender.side)
                          {
                            j = distribute (u3->number, k, u3->money);
                            lmoney += u3->money - j;
                            u3->money = j;
			    for (i = 0; i != MAXITEMS; i++)
                              {
                                j = distribute (u3->number, k, u3->items[i]);
                                litems[i] += u3->items[i] - j;
                                u3->items[i] = j;
                              }
                          }
                        for (i = 0; i != MAXSKILLS; i++)
                          u3->skills[i] = distribute (u3->number, 
						      k, u3->skills[i]);
                        /* Adjust unit numbers */
                        u3->number = k;
                        /* Need this flag cleared for reporting of loot */
                        u3->n = 0;
                      }

                    /* Beute (loot) verteilen.  u3 ist eine temporaere Einheit, welche entweder die Einheit
                       einer zufaellig ausgewaehlten Person der Siegerseite ist, oder die Einheit, die von
                       dieser Partei in dieser Region zum Empfaenger der Beute bestimmt wurde (mittels SAMMEL
                       BEUTE (laws.c: instant_orders) wird u->collector bestimmt).  */
                    // Wenn auf der Gewinner-Seite nur Monstereinheiten sind, die nicht der 
                    // Partei 0 gehoeren, dann muss hier gestoppt werden, sonst gibt es unten
                    // Endlosschleifen
                    
                    cansomebodyloot = 0;
//                    sprintf(buf,"  attacker: %d\n",attacker.side);
//                    log_to_file(buf);
                    for (i = 0; i != ntroops ; i++) {
                      if (ta[i].unit && (!ta[i].status) && (ta[i].side == attacker.side) && 
                          ((ta[i].unit->faction->no == 0) || (ta[i].unit->type == U_MAN))) cansomebodyloot = 1;
//                      sprintf(buf,"  unit: %s faction: %s side: %d status: %d type %d\n",unitid(ta[i].unit),factionid(ta[i].unit->faction), ta[i].side, ta[i].status, ta[i].unit->type);
//                      log_to_file(buf);
                    }
                    
                    if (cansomebodyloot) {
                     log_to_file("     Beute kann eingesammelt werden.\n");
                     for (n = lmoney; n; n--)
                      {
                        // Vorsicht vor Endlosschleife, wenn bei den Angreifern nur Monster
                        // uebrig bleiben!
                        do
                          j = rand () % ntroops;
                        while (ta[j].status || ta[j].side != attacker.side || 
                               (ta[i].unit && ta[j].unit->faction->no != 0 && ta[j].unit->type != U_MAN));
                        // Monster sammeln nur, wenn sie in Partei 0 sind!
			
                        if (ta[j].unit)
                          {
			    u3 = ta[j].unit->collector ? ta[j].unit->collector : ta[j].unit;
			    u3->money++;
			    u3->n++;
                          }
                        else
                          r->money++;
                      }
                     for (i = 0; i != MAXITEMS; i++)
                      for (n = litems[i]; n; n--)
                        if (i <= I_STONE || rand () & 1)
                          {
                            do
                              j = rand () % ntroops;
                            while (ta[j].status || ta[j].side != attacker.side || 
                                   (ta[i].unit && ta[j].unit->faction->no != 0 && ta[j].unit->type != U_MAN));
                            // Monster sammeln nur, wenn sie in Partei 0 sind!
                            if (ta[j].unit)
                              {
				u3 = ta[j].unit->collector ? ta[j].unit->collector : ta[j].unit;
                                if (!u3->litems)
                                  {
                                    u3->litems = cmalloc (MAXITEMS * sizeof (int));
                                    memset (u3->litems, 0, MAXITEMS * sizeof (int));
                                  }
                                u3->items[i]++;
                                u3->litems[i]++;
                              }
                          }

                    /* Report loot */
                    for (f2 = factions; f2; f2 = f2->next)
                      f2->dh = 0;

                    for (u3 = r->units; u3; u3 = u3->next)
                      if (u3->n || u3->litems)
                        {
                          sprintf (buf, translate (ST_RECEIVES_LOOT, u3->faction->language, unitid (u3)));
                          dh = 0;

                          if (u3->n)
                            {
                              scat ("$");
                              icat (u3->n);
                              dh = 1;
                            }

                          if (u3->litems)
                            {
                              for (i = 0; i != MAXITEMS; i++)
                                if (u3->litems[i])
                                  {
                                    if (dh)
                                      scat (", ");
                                    dh = 1;

				    scat (translate (ST_QUANTITIY, u3->faction->language, u3->litems[i],
						     strings[itemnames[u3->litems[i] != 1][i]][u3->faction->language]));
                                  }

                              free (u3->litems);
                              u3->litems = 0;
                            }

                          if (!u3->faction->dh)
                            {
                              addbattle (u3->faction, "");
                              u3->faction->dh = 1;
                            }

                          scat (".");
                          addbattle (u3->faction, buf);
                        }
                      } else {// end of if (cansomebodyloot)
                        log_to_file("     Beute kann von niemandem eingesammelt werden.\n");
                        r->money += lmoney;
                      }
                      // clean up the elementals
                      for (u3 = r->units; u3; u3 = u3->next)
                        if (u3->type == U_WATER_ELEMENTAL) {
                          u3->number = 0; // Unit will be eliminated in next cleanup
                        }
                    free (ta);
                  }
        }
    }
  free (fa);
  putchar ('\n'); /* nach dem Indikator */
}
