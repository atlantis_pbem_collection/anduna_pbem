/* Anduna PBEM Host 6.0 Copyright (C) 1999-2001 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

/* Definitions for all strings that players have contact with for the
   atlantis host. Since a lot of data types are thus defined here,
   associated information is also defined here. */

#include "atlantis.h"
 
char *keywords[MAXKEYWORDS] =
{
  "ADRESSE",
  "ARBEITEN",
  "ATTACKIERE",
  "BENENNE",
  "BEKLAUE",
  "BELAGERE",
  "BESCHREIBE",
  "BETRETE",
  "BEWACHE",
  "BOTSCHAFT",
  "ENDE",
  "FINDE",
  "FOLGE",
  "FORSCHEN",
  "GIB",
  "HELFE",
  "KAEMPFE",
  "KAMPFZAUBER",
  "KAUFE",
  "//",
  "KONTAKTIERE",
  "LEHRE",
  "LERNE",
  "LIEFERE",
  "MACHE",
  "NACH",
  "PASSWORT",
  "REKRUTIERE",
  "SAMMEL",
  "SENDE",
  "STIRB",
  "TREIBE",
  "UNTERHALTEN",
  "VERKAUFE",
  "VERLASSE",
  "VERGESSE",
  "ZAUBERE",
  "ZEIGE",
  "ZERSTOEREN",
  "BENUTZE",
};

char *parameters[MAXPARAMS] =
{
  "ALLES",
  "BAUERN",
  "BEUTE",
  "BURG", 
  "EINHEIT",
  "HINTEN",
  "KOMMANDO",
  "MANN",
  "NICHT",
  "NAECHSTER",
  "PARTEI",
  "PERSONEN",
  "REGION",
  "SCHIFF",
  "SILBER",
  "STRASSEN",
  "TEMP",
  "UND",
  "ZAUBERBUCH",
  "GEBAEUDE", /* CHG BUILDING !*/
  "KRAEUTER", // P_HERBS
  "KAMPF",
  "GIB",
  "BEWACHE"
};

char *options[MAXOPTIONS] =
{
  "AUSWERTUNG",
  "COMPUTER",
  "ZEITUNG",
  "KOMMENTAR",
  "STATISTIK",
  "DEBUG",
  "ZIPPED",
  "TALENTVORLAGE",
  "VERSCHIEDENES",
};

char *skillnames[MAXSKILLS] =
{
  "Armbrustschiessen",
  "Bogenschiessen",
  "Katapultbedienung",
  "Schwertkampf",
  "Speerkampf",
  "Reiten",
  "Taktik",
  "Bergbau",
  "Burgenbau",
  "Handeln",
  "Holzfaellen",
  "Magie",
  "Pferdedressur",
  "Ruestungsbau",
  "Schiffsbau",
  "Segeln",
  "Steinbau",
  "Strassenbau",
  "Tarnung",
  "Unterhaltung",
  "Waffenbau",
  "Wagenbau",
  "Wahrnehmung",
  "Feuermagie",
  "Wassermagie",
  "Luftmagie",
  "Erdmagie",
  "Lebensmagie",
  "Nekromantie",
  "Astralmagie",
  "Kraeuterkunde",
  "Steuereintreiben",
  "Alchemie"
};

char *skillabbreviations[MAXSKILLS] =
{
  "ARM",
  "BOG",
  "KAT",
  "SWK",
  "SPK",
  "REI",
  "TAK",
  "BER",
  "BUR",
  "HAN",
  "HOL",
  "MAG",
  "PFE",
  "RUE",
  "SFB",
  "SEG",
  "STE",
  "STR",
  "TAR",
  "UNT",
  "WAF",
  "WAG",
  "WAH",
  "FEU",
  "WAS",
  "LUF",
  "ERD",
  "LEB",
  "NEK",
  "AST",
  "KRK",
  "TAX",
  "ALC",
};

char *helpnames[MAXHELP] =
{
  "Gib",
  "Silber",
  "Kampf",
  "Bewache",
};

int typenames[2][MAXTYPES] =
{
  {
    ST_PERSON,
    ST_UNDEAD,
    ST_ILLUSION,
    ST_FIREDRAKE,
    ST_DRAGON,
    ST_WYRM,
    ST_PERSON,  /* Wachen sehen immer aus wie Personen.  */
    ST_SERPENT,
    ST_PERSON,  /* Diebe auch */
    ST_WATER_ELEMENTAL,
  },
  {
    ST_PERSONS,
    ST_UNDEADS,
    ST_ILLUSIONS,
    ST_FIREDRAKES,
    ST_DRAGONS,
    ST_WYRMS,
    ST_PERSONS,  /* Wachen sehen immer aus wie Personen.  */
    ST_SERPENTS,
    ST_PERSONS,  /* Diebe auch */
    ST_WATER_ELEMENTAL,
  }
};

int income[MAXTYPES] =  /* legt Steuereinnahmen und  Startgeld fest */
{
  0,
  20,
  0,
  150,
  1000,
  5000,
  15,
  20,
  50,
  0,
};

char *directions[MAXDIRECTIONS] =
{
  "Norden",
  "Sueden",
  "Osten",
  "Westen",
};

char back[MAXDIRECTIONS] =
{
  D_SOUTH,
  D_NORTH,
  D_WEST,
  D_EAST,
};

char delta_x[MAXDIRECTIONS] =
{
  0,
  0,
  1,
  -1,
};

char delta_y[MAXDIRECTIONS] =
{
  -1,
  1,
  0,
  0,
};

int terrainnames[MAXTERRAINS] =
{
  ST_OCEAN,
  ST_PLAIN,
  ST_FOREST,
  ST_SWAMP,
  ST_DESERT,
  ST_HIGHLAND,
  ST_MOUNTAIN,
  ST_GLACIER,
};

char terrainsymbols[MAXTERRAINS] = ".+WSTHBG";

char *roadinto[MAXTERRAINS] =
{
  "in den",
  "in die Ebene von",
  "in den Wald von",
  "in den Sumpf von",
  "durch die Wueste von",
  "auf das Hochland von",
  "in die Berge von",
  "auf den Gletscher von",
};

char *trailinto[MAXTERRAINS] =
{
  "Ozean",
  "die Ebene von",
  "der Wald von",
  "der Sumpf von",
  "die Wueste von",
  "das Hochland von",
  "die Berge von",
  "der Gletscher von",
};

int production[MAXTERRAINS] =
{
  0,
  1000,
  1000,
  200,
  50,
  400,
  100,
  10,
};

char mines[MAXTERRAINS] =
{
  0,
  0,
  0,
  0,
  0,
  0,
  100,
  0,   /* TPWORK : im Gletscher 10 Eisen/Runde*/ 
};

int roadreq[MAXTERRAINS] =
{
  0,
  100,
  150,
  0,
  0,
  200,
  500,
  0,
};

char *shiptypes[3][MAXSHIPS] =
{
  {
    "Boot",
    "Langboot",
    "Drachenschiff",
    "Karavelle",
    "Trireme",
    "Floss",  /* FLOSS */
    "Ballon",
  },
  {
   "ein Boot",
   "ein Langboot",
   "ein Drachenschiff",
   "eine Karavelle",
   "eine Trireme",
   "ein Floss",  /* FLOSS */
   "einen Ballon",
  },
  {
   "des Bootes",
   "des Langbootes",
   "des Drachenschiffes",
   "der Karavelle",
   "der Trireme",
   "des Flosses",  /* FLOSS */
   "des Ballons",
  }
};

int sailors[MAXSHIPS] =
{
  2,
  10,
  50,
  30,
  120,
  1,  /* FLOSS */
  11,
};

int shipcapacity[MAXSHIPS] =
{
  50,
  500,
  1000,
  3000,
  2000,
  20,   /* FLOSS */  /* pro Holzstamm */
  33,   /* BALLOON */
};

int shipcost[MAXSHIPS] =
{
  5,
  50,
  100,
  250,
  200,
  1,   /* FLOSS */
  50,  /* BALLOON: NEEDS AIRSHIP RESOURCE */
};

char shipspeed[MAXSHIPS] =
{
  3,
  4,
  6,
  6,
  8,
  1, /* FLOSS */
  5,
};

char shipbuildingtalent[MAXSHIPS] =
{
 1,
 1,
 2,
 3,
 4,
 1, /* FLOSS */
 5, /* BALLOON */
};

char captain_talent[MAXSHIPS] =
{
 1,
 1,
 2,
 3,
 4,
 1, /* FLOSS */
 5, /* BALLOON*/
};


char *buildingnames[MAXBUILDINGS] =
{
  "Baustelle",
  "Befestigung",
  "Turm",
  "Schloss",
  "Festung",
  "Zitadelle",
};

/* Die Groesse gibt auch an, welches Talent man mindestens haben muss, um an der Burg
   weiterzuarbeiten (bei einer Burg zB. 3), sowie den Bonus-multiplikator fuer K_WORK! */

int buildingcapacity[MAXBUILDINGS] =
{
  0,
  2,
  10,
  50,
  250,
  1250,
};

/*CHG BUILDINGS: Neue Aufteilung in Gebaeudetypen*/
char *building_names[3][MAX_BUILDINGS] =
{
 {
  "Burg",
  "Monument",
  "Sumpfgasanlage",
 },
 {
  "eine Burg",
  "ein Monument",
  "eine Sumpfgasanlage",
 },
 {
  "der Burg",
  "des Monumentes",
  "der Sumpfgasanlage",
 }
};

int building_maxsize[MAX_BUILDINGS] = 
{
  0,  /* Null heisst unbegrenzt erweiterbar */
  0,
  50,
};

/* Materialkosten (iron,wood,stone) je Groessenpunkt */
int building_cost[MAX_BUILDINGS][LASTBUILDINGMATERIAL+1] = 
{
  {0,0,1},
  {1,1,1},
  {1,0,1}
};

int building_talent[MAX_BUILDINGS] =
{
  1,
  4,
  2,
};

/* Silber, dass pro Runde aufgewandt werden muss, damit das
   Gebaeude keinen Schaden nimmt */
int building_upkeep[MAX_BUILDINGS] =
{
  0,
  0,
  0,
};


int itemnames[2][MAXITEMS] =
{
  {
    ST_IRON,
    ST_WOOD,
    ST_STONE,
    ST_RES1,
    ST_RES2,
    ST_AIRSHIP_RES,
    ST_HORSE,
    ST_WAGON,
    ST_CATAPULT,
    ST_SWORD,
    ST_SPEAR,
    ST_CROSSBOW,
    ST_LONGBOW,
    ST_PROD1,
    ST_PROD2,
    ST_PROD3,
    ST_PROD4,
    ST_CHAIN_MAIL,
    ST_PLATE_ARMOR,
    ST_BALM,
    ST_SPICE,
    ST_JEWELRY,
    ST_MYRRH,
    ST_OIL,
    ST_SILK,
    ST_INCENSE,
    ST_HOOD,
    ST_AMULET_OF_LIFE,
    ST_AMULET_OF_HEALING,
    ST_AMULET_OF_TRUE_SEEING,
    ST_CLOAK_OF_INVULNERABILITY,
    ST_RING_OF_INVISIBILITY,
    ST_RING_OF_POWER,
    ST_RUNESWORD,
    ST_SHIELDSTONE,
    ST_WINGED_HELMET,
    ST_DRAGON_PLATE,
    ST_MAGIC_EYE,
    ST_COMPASS,
    ST_FLAMESWORD,
    ST_MAG4,
    ST_MAG5,
    ST_MAG6,
    ST_MAG7,
    ST_MAG8,
    ST_I4,
    ST_HERB_PLAIN_1, // start of herbs
    ST_HERB_PLAIN_2,
    ST_HERB_PLAIN_3,
    ST_HERB_FOREST_1,
    ST_HERB_FOREST_2,
    ST_HERB_FOREST_3,
    ST_HERB_SWAMP_1,
    ST_HERB_SWAMP_2,
    ST_HERB_SWAMP_3,
    ST_HERB_DESERT_1,
    ST_HERB_DESERT_2,
    ST_HERB_DESERT_3,
    ST_HERB_HIGHLAND_1,
    ST_HERB_HIGHLAND_2,
    ST_HERB_HIGHLAND_3,
    ST_HERB_MOUNTAIN_1,
    ST_HERB_MOUNTAIN_2,
    ST_HERB_MOUNTAIN_3,
    ST_HERB_GLACIER_1,
    ST_HERB_GLACIER_2,
    ST_HERB_GLACIER_3, // end of herbs
    ST_POTION_MANA,
    ST_POTION_HEAL,
  },
  {
    ST_IRONS,
    ST_WOODS,
    ST_STONES,
    ST_RES1S,
    ST_RES2S,
    ST_AIRSHIP_RESS,
    ST_HORSES,
    ST_WAGONS,
    ST_CATAPULTS,
    ST_SWORDS,
    ST_SPEARS,
    ST_CROSSBOWS,
    ST_LONGBOWS,
    ST_PROD1S,
    ST_PROD2S,
    ST_PROD3S,
    ST_PROD4S,
    ST_CHAIN_MAILS,
    ST_PLATE_ARMORS,
    ST_BALMS,
    ST_SPICES,
    ST_JEWELRIES,
    ST_MYRRHS,
    ST_OILS,
    ST_SILKS,
    ST_INCENSES,
    ST_HOODS,
    ST_AMULETS_OF_LIFE,
    ST_AMULETS_OF_HEALING,
    ST_AMULETS_OF_TRUE_SEEING,
    ST_CLOAKS_OF_INVULNERABILITY,
    ST_RINGS_OF_INVISIBILITY,
    ST_RINGS_OF_POWER,
    ST_RUNESWORDS,
    ST_SHIELDSTONES,
    ST_WINGED_HELMETS,
    ST_DRAGON_PLATES,
    ST_MAGIC_EYES,
    ST_COMPASSES,
    ST_FLAMESWORDS,
    ST_MAG4S,
    ST_MAG5S,
    ST_MAG6S,
    ST_MAG7S,
    ST_MAG8S,
    ST_I4S,
    ST_HERB_PLAIN_1S, // start of herbs
    ST_HERB_PLAIN_2S,
    ST_HERB_PLAIN_3S,
    ST_HERB_FOREST_1S,
    ST_HERB_FOREST_2S,
    ST_HERB_FOREST_3S,
    ST_HERB_SWAMP_1S,
    ST_HERB_SWAMP_2S,
    ST_HERB_SWAMP_3S,
    ST_HERB_DESERT_1S,
    ST_HERB_DESERT_2S,
    ST_HERB_DESERT_3S,
    ST_HERB_HIGHLAND_1S,
    ST_HERB_HIGHLAND_2S,
    ST_HERB_HIGHLAND_3S,
    ST_HERB_MOUNTAIN_1S,
    ST_HERB_MOUNTAIN_2S,
    ST_HERB_MOUNTAIN_3S,
    ST_HERB_GLACIER_1S,
    ST_HERB_GLACIER_2S,
    ST_HERB_GLACIER_3S, // end of herbs
    ST_POTION_MANAS,
    ST_POTION_HEALS,
  }
};

int itemskill[LASTPRODUCT] =
{
  SK_MINING,
  SK_LUMBERJACK,
  SK_QUARRYING,
  SK_MAGIC,   /*Empty Slots: No one will try MAKE with a magician*/
  SK_MAGIC,
  SK_LONGBOW,  /*AIRSHIP_RES: Zukus werden mit Boegen gejagt*/
  SK_HORSE_TRAINING,
  SK_CARTMAKER,
  SK_CARTMAKER,
  SK_WEAPONSMITH,
  SK_WEAPONSMITH,
  SK_WEAPONSMITH,
  SK_WEAPONSMITH,
  SK_MAGIC,   /*Empty Slots: No one will try MAKE with a magician*/
  SK_MAGIC,
  SK_MAGIC,
  SK_MAGIC,
  SK_ARMORER,
  SK_ARMORER,
};

char itemweight[LASTLUXURY] =
{
  5,
  5,
  60,
  199,
  199,
  2,  /*AIRSHIP_RES: Zukuhaeute sind leicht*/
  50,
  40,
  60,
  1,
  1,
  1,
  1,
  199,
  199,
  199,
  199,
  2,
  4,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
};

char itemquality[LASTPRODUCT] =   /* How much skill is needed */
{
  1,
  1,
  1,
  99,
  99,
  2,  /*AIRSHIP_RES: Zukus werden mit Bogen ab Talent 2 gejagt*/
  1,
  1,
  2,
  1,
  2,
  2,
  3,
  99,
  99,
  99,
  99,
  1,
  3,
};

int rawmaterial[LASTPRODUCT] =
{
  0,
  0,
  0,
  I_4,
  I_4,
  0,
  0,
  I_WOOD,
  I_WOOD,
  I_IRON,
  I_WOOD,
  I_WOOD,
  I_WOOD,
  I_4,
  I_4,
  I_4,
  I_4,
  I_IRON,
  I_IRON,
};

char rawquantity[LASTPRODUCT] =
{
  0,
  0,
  0,
  99,
  99,
  0,
  0,
  5,
  10,
  1,
  1,
  1,
  1,
  99,
  99,
  99,
  99,
  2,
  5,
};

int itemprice[MAXLUXURIES] =
{
  4,
  5,
  7,
  5,
  3,
  6,
  4,
};

char woodsize[MAXWOODS] =
{
  0,
  10,
  20,
  60,
  80,
};

int carryingcapacity[MAXTYPES] =
{
  15,
  20,
  50,
  150,
  450,
  15000,
  15,
  100,
  15,
  100,
};
