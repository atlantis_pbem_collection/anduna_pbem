
#include "atlantis.h"

int changed;

int wants_report, wants_computer_report, wants_zine;
int wants_comment, wants_compressed;
int wants_stats, wants_debug, wants_misc;

char *statusnames[MAXSTATUS] = {
  "Vorne",
  "Hinten",
  "Nicht"
};

char *combatspellnames[MAXCOMBATSPELLS] = {
  "Vorher",
  "Waehrend",
  "Nachher"
};

char *languagenames[MAXLANGUAGES] = {
  "Deutsch",
  "Englisch"
};

unit *inputunit()
{
  unit *u;
  int id;
  printf ("Nummer der Einheit: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  u = findunitglobal (id);
  if (!u)
    {
      puts ("Diese Einheit existiert nicht.");
      return 0;
    }
  return u;
}

faction *inputfaction()
{
  faction *f;
  int id;
  printf ("Nummer der Partei: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  f = findfaction (id);
  if (!f)
    {
      puts ("Diese Partei existiert nicht.");
      return 0;
    }
  return f;
}

building *inputbuilding()
{
  building *b;
  int id;
  printf ("Nummer des Gebaeudes: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  b = findbuilding (id);
  if (!b)
    {
      puts ("Dieses Gebaeude existiert nicht.");
      return 0;
    }
  return b;
}

ship *inputship()
{
  ship *s;
  int id;
  printf ("Nummer des Schiffs: ");
  afgets (buf, MAXLINE);
  if (!buf[0])
    return 0;
  id = atoi (buf);
  s = findship(id);
  if (!s)
    {
      puts ("Dieses Schiff existiert nicht.");
      return 0;
    }
  return s;
}

void e_show_map()
{
  printf (" t - TERRAIN\n f - FACTIONS \n a - UNARMED\n b - abort\n");
  while (1)
    {
      printf ("map: (? fuer Hilfe) > ");
      afgets (buf, MAXLINE);
      switch (buf[0])
	{
	case 't':
	  showmap(M_TERRAIN);
	  return;

	case 'f':
	  showmap(M_FACTIONS);
	  return;

	case 'a':
	  showmap(M_UNARMED);
	  return;

	case 'q':
	  return;

	case '?':
	  printf (" t - TERRAIN\n"
		  " f - FACTIONS\n"
		  " a - ARMED\n"
		  " q - abbrechen\n");
	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	}
    }
}

void e_write_faction(faction *f)
{
  printf ("Name: %s (%d)\n"
	  "Addresse: %s\n"
	  "Passwort: %s\n"
	  "Runde der letzten Befehle: %d\n"
	  "Newbie: %s\n",
	  f->name, f->no,
	  f->addr,
	  f->passw,
	  f->lastorders,
	  (f->newbie)?"ja":"nein");
  printf ("Sprache: %s\n",
	  languagenames[f->language]);
  printf ("Optionen: %s%s%s%s%s%s%s%s\n",
	  (f->options & wants_report)?"Report ":"",
	  (f->options & wants_computer_report)?"Computerreport ":"",
	  (f->options & wants_zine)?"Zeitung ":"",
	  (f->options & wants_comment)?"Kommentar ":"",
	  (f->options & wants_compressed)?"Zipped ":"",
	  (f->options & wants_stats)?"Statistik ":"",
	  (f->options & wants_debug)?"Debug ":"",
	  (f->options & wants_misc)?"Verschiedenes":"");
  printf ("Bauernansehen: %d\n",
	  f->peasants_like_faction);
}

void e_edit_faction_spells(faction *f)
{
  puts ("nicht implementiert!");
}

void e_edit_faction_allies(faction *f)
{
  int i, j;
  rfaction *rf;
  faction *f2;

  for (i = 0; i<MAXHELP; i++)
    {
      printf ("%2d: %s: ", i, helpnames[i]);
      for (rf = f->allies[i]; rf; rf = rf->next)
	{
	  printf ("%d ", rf->faction->no);
	}
      if (rf == f->allies[i]) printf ("keine");
      printf ("\n");
    }
  while (1) {
    printf ("faction - allies: (? fuer Hilfe)> ");
    afgets (buf, MAXLINE);
    
    switch (buf[0])
      {
      case 'h':
	while (1)
	  {
	    printf ("Nummer der Liste? ");
	    afgets (buf, MAXLINE);
	    i = atoi (buf);
	    if (i >= 0 && i < MAXHELP) break;
	    printf ("Die Nummer ist ungueltig!\n");
	  }
	printf ("Nummer der hinzuzufuegenden Partei? ");
	afgets (buf, MAXLINE);
	j = atoi (buf);
	f2 = findfaction (j);

	for (rf = f->allies[i]; rf; rf = rf->next)
	  if (rf->faction == f2)
	    break;
	  
	if (!rf)
	  {
	    rf = cmalloc (sizeof (rfaction));
	    rf->faction = f2;
	    addlist (f->allies[i], rf);
	    printf ("Allierter wurde hinzugefuegt!\n");
	  } else {
	    printf ("Allianz besteht bereits!\n");
	  }
	break;

      case 'e':
	while (1)
	  {
	    printf ("Nummer der Liste? ");
	    afgets (buf, MAXLINE);
	    i = atoi (buf);
	    if (i >= 0 && i < MAXHELP) break;
	    printf ("Die Nummer ist ungueltig!\n");
	  }
	printf ("Nummer der zu entfernenden Partei? ");
	afgets (buf, MAXLINE);
	j = atoi (buf);
	f2 = findfaction (j);

	for (rf = f->allies[i]; rf; rf = rf->next)
	  if (rf->faction == f2)
	    {
	      removelist (f->allies[i], rf);
	      break;
	      printf ("Allierter wurde entfernt!\n");
	    }
	if (!rf)
	  printf ("Es gab keinen Allier Nummer!\n");
	break;

      case 'q':
	return;

      case '?':
	printf (" h - Allierten hinzufuegen\n"
		" e - Allierten entfernen\n"
		" q - zurueck zum Parteienmenue\n"
		" ? - zeigt diese Hilfe an\n");
	break;

      default:
	if (buf[0])
	  printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	break;
      }
  }
}

void e_edit_faction_options(faction *f)
{
  printf ("Optionen:\n"
	  " 1: Report: %s\n"
	  " 2: Computerreport: %s\n"
	  " 3: Zeitung: %s\n"
	  " 4: Kommentar: %s\n"
	  " 5: Zipped: %s\n"
	  " 6: Statistik: %s\n"
	  " 7: Debug: %s\n"
	  " 8: Verschiedenes: %s\n",
	  (f->options & wants_report)?"ja":"nein",
	  (f->options & wants_computer_report)?"ja":"nein",
	  (f->options & wants_zine)?"ja":"nein",
	  (f->options & wants_comment)?"ja":"nein",
	  (f->options & wants_compressed)?"ja":"nein",
	  (f->options & wants_stats)?"ja":"nein",
	  (f->options & wants_debug)?"ja":"nein",
	  (f->options & wants_misc)?"ja":"nein");
  printf ("faction - options: (? fuer Hilfe)> ");
  afgets (buf, MAXLINE);
  switch (buf[0])
    {
    case '1':
      if (f->options & wants_report)
	f->options -= wants_report;
      else
	f->options += wants_report;
      printf ("Option Report wurde auf \"%s\" gesetzt!\n", (f->options & wants_report)?"ja":"nein");
      break;

    case '2':
      if (f->options & wants_computer_report)
	f->options -= wants_computer_report;
      else
	f->options += wants_computer_report;
      printf ("Option Computerreport wurde auf \"%s\" gesetzt!\n", (f->options & wants_computer_report)?"ja":"nein");
      break;

    case '3':
      if (f->options & wants_zine)
	f->options -= wants_zine;
      else
	f->options += wants_zine;
      printf ("Option Zeitung wurde auf \"%s\" gesetzt!\n", (f->options & wants_zine)?"ja":"nein");
      break;

    case '4':
      if (f->options & wants_comment)
	f->options -= wants_comment;
      else
	f->options += wants_comment;
      printf ("Option Kommentar wurde auf \"%s\" gesetzt!\n", (f->options & wants_comment)?"ja":"nein");
      break;

    case '5':
      if (f->options & wants_compressed)
	f->options -= wants_compressed;
      else
	f->options += wants_compressed;
      printf ("Option Zipped wurde auf \"%s\" gesetzt!\n", (f->options & wants_compressed)?"ja":"nein");
      break;

    case '6':
      if (f->options & wants_stats)
	f->options -= wants_stats;
      else
	f->options += wants_stats;
      printf ("Option Statistik wurde auf \"%s\" gesetzt!\n", (f->options & wants_stats)?"ja":"nein");
      break;

    case '7':
      if (f->options & wants_debug)
	f->options -= wants_debug;
      else
	f->options += wants_debug;
      printf ("Option Debug wurde auf \"%s\" gesetzt!\n", (f->options & wants_debug)?"ja":"nein");
      break;

    case '8':
      if (f->options & wants_misc)
	f->options -= wants_misc;
      else
	f->options += wants_misc;
      printf ("Option Verschiedenes wurde auf \"%s\" gesetzt!\n", (f->options & wants_misc)?"ja":"nein");
      break;

    case 'q':
      return;

    case '?':
      printf (" 1-8 - Option aendern\n"
	      " q - zurueck zum Parteienmenue\n"
	      " ? - zeigt diese Hilfe an\n");
      break;

    default:
      if (buf[0])
	printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
      break;
    }
}


void e_show_faction()
{
  int i;
  faction *f = inputfaction();
  if (!f) return;
  e_write_faction(f);

  while (1)
    {
      printf ("faction: (? fuer Hilfe)> ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	case 'n':
	  printf ("alter Name: %s\n", f->name);
	  while (1)
	    {
	      printf ("neuer Name: ");
	      afgets (buf, NAMESIZE);
	      if (buf[0]) break;
	      printf ("Der eingegebene Name ist ungueltig\n");
	    }
	  f->name = crealloc (strlen (buf), f->name);
	  strcpy (f->name, buf);
	  changed = 1;
	  printf ("Name wurde gesetzt\n");
	  break;

	case 'a':
	  printf ("alte Addresse: %s\n", f->addr);
	  while (1)
	    {
	      printf ("neue Addresse: ");
	      afgets (buf, DISPLAYSIZE);
	      if (buf[0]) break;
	      printf ("Die eingegebene Addresse ist ungueltig\n");
	    }
	  f->addr = crealloc (strlen (buf), f->addr);
	  strcpy (f->addr, buf);
	  changed = 1;
	  printf ("Addresse wurde gesetzt\n");
	  break;

	case 'p':
	  printf ("altes Passwort: %s\n", f->passw);
	  while (1)
	    {
	      printf ("neues Passwort: ");
	      afgets (buf, DISPLAYSIZE);
	      if (buf[0]) break;
	      printf ("Das eingegebene Passwort ist ungueltig\n");
	    }
	  f->passw = crealloc (strlen (buf), f->passw);
	  strcpy (f->passw, buf);
	  changed = 1;
	  printf ("Passwort wurde gesetzt\n");
	  break;

	case 'l':
	  printf ("alte Runde der letzten Befehle: %d\n", f->lastorders);
	  while (1)
	    {
	      printf ("neue Runde der letzten Befehle: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0 && i <= turn) break;
	      printf ("Runde ungueltig!\n");
	    }
	  f->lastorders = i;
	  changed = 1;
	  printf ("Runde der letzten Befehle wurde gesetzt\n");
	  break;

	case 'N':
	  f->newbie = f->newbie?0:1;
	  changed = 1;
	  printf ("Newbie-Flag wurde auf %d gesetzt\n", f->newbie);
	  break;

	case 's':
	  printf ("alte Sprache: %s\n", languagenames[f->language]);
	  for (i = 0; i < MAXLANGUAGES; i++)
	    {
	      printf ("%2d: %s\n", i, languagenames[i]);
	    }
	  while (1)
	    {
	      printf ("neue Sprache: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0 && i < MAXLANGUAGES) break;
	      printf ("Es gibt keine Sprache mit dieser Nummer\n");
	    }
	  f->language = i;
	  changed = 1;
	  break;

	case 'o':
	  e_edit_faction_options (f);
	  break;

	case 'b':
	  printf ("altes Bauernansehen: %d\n", f->peasants_like_faction);
	  while (1)
	    {
	      printf ("neues Bauernansehen: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Wert ungueltig!\n");
	    }
	  f->peasants_like_faction = i;
	  changed = 1;
	  printf ("Bauernansehen wurde gesetzt\n");
	  break;

	case 'S':
	  e_edit_faction_spells (f);
	  break;

	case 'A':
	  e_edit_faction_allies (f);
	  break;

	case 'q':
	  return;

	case '?':
	  printf (" n - Name aendern\n"
		  " a - Addresse aendern\n"
		  " p - Passwort aendern\n"
		  " l - Runde der letzten Befehle aendern\n"
		  " N - Newbie-Flag wechseln\n"
		  " s - Sprache aendern\n"
		  " o - Optionen editieren\n"
		  " b - Bauernansehen aendern\n"
		  " S - Sprueche editieren\n"
		  " A - Allierte editieren\n"
		  " q - zurueck zum Hauptmenue\n"
		  " ? - zeigt diese Hilfe an\n");
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	}
    }
}

void e_write_unit(unit *u)
{
  printf ("Name: %s (%d)\n" 
	  "Beschreibung %s\n"
	  "Kommentar: %s\n"
	  "Anzahl: %-15d        Typ: %-15s\n"
	  "Silber: %-15d        Mana: %-15d  Hits: %d\n"
	  "Zaubereffekt: %-15s  Zauberzeit: %-15d\n",
	  u->name, u->no,
	  u->display,
	  u->playercomment,
	  u->number, strings[typenames[0][u->type]][L_DEUTSCH],
	  u->money, u->mana, u->hits,
	  strings[spellnames[u->enchanted]][L_DEUTSCH], u->effect);
  if (u->faction)
    printf ("Parteinr.: %-10d   ", u->faction->no);
  else
    printf ("Parteinr.: %-10s   ", "(none)");
  if (u->building)
    printf ("Gebaeudenr.: %-10d   ", u->building->no);
  else
    printf ("Gebaeudenr.: %-10s   ", "(none)");
  if (u->ship)
    printf ("Schiffnr.: %-10d\n", u->ship->no);
  else
    printf ("Schiffnr.: %-10s\n", "(none)");
  printf ("Besitztstatus: %-5d  Kampfstatus: %-13s   Bewachtstatus: %-10d\n"
	  "Letzter Befehl: %s\n" 
	  "Befehl 2 (fuer den Liefere Befehl): %s\n",
	  u->owner, statusnames[u->status], u->guard,
	  u->lastorder,
	  u->thisorder2);
}

void e_edit_unit_type(unit *u)
{
  int i;
  printf ("Alter Typ: %s\n", strings[typenames[0][u->type]][L_DEUTSCH]);
  printf ("Typen:\n");
  for (i = 0; i<MAXTYPES; i+=2)
    {
      if (i < MAXTYPES-1)
	{
	  printf ("%2d: %-20s   %2d: %-20s\n", i, strings[typenames[0][i]][L_DEUTSCH], i+1, strings[typenames[0][i+1]][L_DEUTSCH]);
	} else {
	  printf ("%2d: %-20s", i, strings[typenames[0][i]][L_DEUTSCH]);
	}
    }
  while (1)
    {
      printf ("Neuer Typ: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXTYPES && i>=0) break;
      printf ("Es gibt keine Typ mit dieser Nummer!\n");
    }
  u->type = i;
  changed = 1;
  printf ("Der Type wurde gesetzt!\n");
}

void e_edit_unit_enchanted(unit *u)
{
  int i;
  printf ("Alter Zaubereffekt: %s\n", strings[spellnames[u->enchanted]][L_DEUTSCH]);
  printf ("Zaubereffekte:\n");
  for (i = 0; i<MAXSPELLS; i+=2)
    {
      if (i < MAXSPELLS-1)
	{
	  printf ("%2d: %-20s   %2d: %-20s", i, strings[spellnames[i]][L_DEUTSCH], i+1, strings[spellnames[i+1]][L_DEUTSCH]);
	} else {
	  printf ("%2d: %-20s", i, strings[spellnames[i]][L_DEUTSCH]);
	}
    }
  while (1)
    {
      printf ("Neuer Zaubereffekt: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXTYPES && i>=0) break;
      printf ("Es gibt keine Zaubereffekt mit dieser Nummer!\n");
    }
  u->enchanted = i;
  changed = 1;
  printf ("Der Zaubereffekt wurde gesetzt!\n");
}

void e_edit_unit_faction(unit *u)
{
  faction *f;
  if (u->faction)
    printf ("Alte Partei: %d\n", u->faction->no);
  else 
    printf ("Alte Partei: (none)\n");
  f = inputfaction();
  if (f)
    {
      u->faction = f;
      changed = 1;
      printf ("Partei wurde gesetzt\n");
    }
  else
    printf ("Partei wurde nicht gesetzt\n");
}

void e_edit_unit_building(unit *u)
{
  building *b;
  if (u->building)
    printf ("Altes Gebaeude: %d\n", u->building->no);
  else 
    printf ("Altes Gebaeude: (none)\n");
  b = inputbuilding();
  u->building = b;
  changed = 1;
  if (b)
    {
      printf ("Gebaeude wurde gesetzt\n");
    }
  else
    printf ("Gebaeude wurde auf (none) gesetzt\n");
}

void e_edit_unit_ship(unit *u)
{
  ship *s;
  if (u->ship)
    printf ("Altes Schiff: %d\n", u->ship->no);
  else 
    printf ("Altes Schiff: (none)\n");
  s = inputship();
  u->ship = s;
  changed = 1;
  if (s)
    {
      printf ("Schiff wurde gesetzt\n");
    }
  else
    printf ("Schiff wurde auf (none) gesetzt\n");
}

void e_write_unit_spells(unit *u)
{
  puts (" nicht implementiert");
}

void e_edit_unit_spells(unit *u)
{
  puts (" nicht implementiert");
}

void e_write_unit_magic(unit *u)
{
  int i;
  int j;
  printf ("Kampfsprueche:\n");
  for (i = 0; i<MAXCOMBATSPELLS; i++)
    {
      if (u->combatspell[i] >= 0)
	printf ("%d: %s: %s\n", i, combatspellnames[i], strings[spellnames[u->combatspell[i]]][L_DEUTSCH]);
      else
	printf ("%d: %s: (none)\n", i, combatspellnames[i]);
    }
  printf ("\nSprueche:\n");
  j = 0;
  for (i = 0; i<MAXSPELLS; i++)
    {
      if (u->spells[i])
	{
	  printf ("  %s\n", strings[spellnames[i]][L_DEUTSCH]);
	  j = 1;
	}
    }
  if (!j)
    printf ("  (keine)\n");
}

void e_write_unit_skills(unit *u)
{
  int i;
  for (i = 0; i<MAXSKILLS; i+=2)
    {
      if (i < MAXSKILLS-1)
	printf ("%2d: %20s: %-5d   %2d: %20s: %-5d\n", i, skillnames[i], u->skills[i], i+1, skillnames[i+1], u->skills[i+1]);
      else
	printf ("%2d: %20s: %-5d\n", i, skillnames[i], u->skills[i]);
    }
}

void e_edit_unit_magic(unit *u)
{
  int i;
  int j;
  int h;
  e_write_unit_magic(u);

  while (1)
    {
      printf ("unit - magic: (? fuer Hilfe)> ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	  i = atoi (buf);
	  if (i >= MAXCOMBATSPELLS)
	    {
	      printf ("Es gibt keinen Kampfspruch mit dieser Nummer!\n");
	      break;
	    }
	  if (u->combatspell[i] >= 0)
	    printf ("Alter %s: %s\n", combatspellnames[i], strings[spellnames[u->combatspell[i]]][L_DEUTSCH]);
	  else
	    printf ("Alter %s: (none)\n", combatspellnames[i]);
	  h = 0;
	  for (j = 0; j<MAXSPELLS; j++)
	    {
	      if (u->spells[j] && iscombatspell[j])
		{
		  printf ("%2d: %s\n", j, strings[spellnames[j]][L_DEUTSCH]);
		  h = 1;
		}
	    }
	  if (!h) {
	    printf ("Die Einheit beherrscht keine Kampfsprueche!\n");
	    break;
	  }
	  while (1)
	    {
	      printf ("Neuer %s: ", combatspellnames[i]);
	      afgets (buf, MAXLINE);
	      j = atoi (buf);
	      if (u->spells[j] && iscombatspell[j]) break;
	      printf ("Die Einheit beherrscht keinen Spruch mit dieser Nummer oder es ist kein Kampfspruch!\n");
	    }
	  break;

	case 's':
	  e_edit_unit_spells(u);
	  break;

	case 'q':
	  return;

	case '?':
	  printf (" z - Magie zeigen\n"
		  " 0 - %d Kampfspruch editieren\n"
		  " s - Sprueche editieren\n"
		  " q - zurueck zum Hauptmenue\n"
		  " ? - diese Hilfe anzeigen\n",
		  MAXCOMBATSPELLS-1);
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);	  
	}
    }
}

void e_edit_unit_skills(unit *u)
{
  int i, j;
  e_write_unit_skills(u);

  while (1)
    {
      printf ("unit - skills: (? fuer Hilfe)> ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	case 'z':
	  e_write_unit_skills(u);
	  break;

	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	  i = atoi (buf);
	  if (i >= MAXSKILLS)
	    {
	      printf ("Es gibt kein Talent mit dieser Nummer!\n");
	      break;
	    }
	  printf ("Alter Talenwert: %20s: %-5d\n", skillnames[i], u->skills[i]);
	  while (1)
	    {
	      printf ("Neuer Talentwert: ");
	      afgets (buf, MAXLINE);
	      j = atoi (buf);
	      if (j >= 0) break;
	      printf ("Der angegebene Wert ist kein zulaessiger Talentwert!\n");
	    }
	  u->skills[i]=j;
	  changed = 1;
	  printf ("Der Talenwert wurde gesetzt\n");
	  break;

	case 'q':
	  return;

	case '?':
	  printf (" z - Talente zeigen\n"
		  " 0 - %d Talent editieren\n"
		  " q - zurueck zum Hauptmenue\n"
		  " ? - diese Hilfe anzeigen\n",
		  MAXSKILLS-1);
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	}
    }
}

void e_edit_unit_status(unit *u)
{
  int i;
  printf ("0: Besitztstatus\n"
	  "1: Kampfstatus\n"
	  "2: Bewachtstatus\n");
  while (1)
    {
      printf ("Zu editierender Status: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i >= 0 && i < 3) break;
      printf ("Es gibt keine Zustand mit dieser Nummer\n");
    }
  switch (i)
    {
    case 0:
      u->owner = u->owner?0:1;
      changed = 1;
      printf ("Besitztstatus wurde auf %d gesetzt\n", u->owner);
      break;

    case 1:
      printf ("Alter Kampfstatus: %s\n", statusnames[u->status]);
      for (i = 0; i<MAXSTATUS; i+=2)
	{
	  if (i < MAXSTATUS-1)
	    printf ("%2d: %-20s   %2d: %-20s\n", i, statusnames[i], i+1, statusnames[i+1]);
	  else
	    printf ("%2d: %-20s\n", i, statusnames[i]);
	}
      while (1)
	{
	  printf ("Neuer Kampfstatus: ");
	  afgets (buf, MAXLINE);
	  i = atoi (buf);
	  if (i >= 0 && i < MAXSTATUS) break;
	  printf ("Es gibt keine Kampfstatus mit die Nummer\n");
	}
      u->status = i;
      changed = 1;
      printf ("Kampfstatus wurde gesetzt\n");
      break;

    case 2:
      u->guard = u->guard?0:1;
      changed = 1;
      printf ("Bewachtstatus wurde auf %d gesetzt\n", u->guard);
      break;
    }
}

void e_show_unit()
{
  int i;
  unit *u;
  u = inputunit();
  if (!u) return;
  e_write_unit(u);

  while (1)
    {
      printf ("unit: (? fuer Hilfe)> ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	case 'z':
	  e_write_unit(u);
	  break;

	case 'n':
	  printf ("alter Name: %s\n", u->name);
	  while (1)
	    {
	      printf ("neuer Name: ");
	      afgets (buf, NAMESIZE);
	      if (buf[0]) break;
	      printf ("Der eingegebene Name ist ungueltig\n");
	    }
	  u->name = crealloc (strlen (buf), u->name);
	  strcpy (u->name, buf);
	  changed = 1;
	  printf ("Name wurde gesetzt\n");
	  break;

	case 'B':
	  printf ("alte Beschreibung: %s\n", u->display);
	  printf ("neue Beschreibung: ");
	  afgets (buf, DISPLAYSIZE);
	  u->display = crealloc (strlen (buf), u->display);
	  strcpy (u->display, buf);
	  changed = 1;
	  printf ("Beschreibung wurde gesetzt\n");
	  break;

	case 'k':
	  printf ("alter Kommentar: %s\n", u->playercomment);
	  printf ("neuer Kommentar: ");
	  afgets (buf, DISPLAYSIZE);
	  u->playercomment = crealloc (strlen (buf), u->playercomment);
	  strcpy (u->playercomment, buf);
	  changed = 1;
	  printf ("Kommentar wurde gesetzt\n");
	  break;

	case 'a':
	  printf ("alte Anzahl: %d\n", u->number);
	  while (1)
	    {
	      printf ("neue Anzahl: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Die angegebene Anzahl ist ungueltig!\n");
	    }
	  u->number = i;
	  changed = 1;
	  printf ("Anzahl wurde gesetzt\n");
	  break;

	case 't':
	  e_edit_unit_type(u);
	  break;

	case 's':
	  printf ("altes Silber: %d\n", u->money);
	  while (1)
	    {
	      printf ("neues Silber: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Die angegebene Anzahl ist ungueltig!\n");
	    }
	  u->money = i;
	  changed = 1;
	  printf ("Silber wurde gesetzt\n");
	  break;

	case 'm':
	  printf ("altes Mana: %d\n", u->mana);
	  while (1)
	    {
	      printf ("neues Mana: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Die angegebene Anzahl ist ungueltig!\n");
	    }
	  u->mana = i;
	  changed = 1;
	  printf ("Mana wurde gesetzt\n");
	  break;

	case 'h':
	  printf ("alte Hits: %d\n", u->hits);
	  while (1)
	    {
	      printf ("neue Hits: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Die angegebene Anzahl ist ungueltig!\n");
	    }
	  u->hits = i;
	  changed = 1;

	  printf ("Hits wurde gesetzt\n");	  
	  break;

	case 'e':
	  e_edit_unit_enchanted(u);
	  break;

	case 'Z':
	  printf ("alte Zauberzeit: %d\n", u->effect);
	  while (1)
	    {
	      printf ("neue Zauberzeit: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Die angegebene Zahl ist ungueltig!\n");
	    }
	  u->effect = i;
	  changed = 1;
	  printf ("Zauberzeit wurde gesetzt\n");
	  break;

	case 'p':
	  e_edit_unit_faction(u);
	  break;

	case 'g':
	  e_edit_unit_building(u);
	  break;

	case 'S':
	  e_edit_unit_ship(u);
	  break;

	case 'A':
	  e_edit_unit_status(u);
	  break;

	case 'l':
	  printf ("alter letzter Befehl: %s\n", u->lastorder);
	  printf ("neuer letzter Befehl: ");
	  afgets (buf, MAXLINE);
	  u->lastorder = crealloc (strlen (buf), u->lastorder);
	  strcpy (u->lastorder,  buf);
	  changed = 1;
	  printf ("letzter Befehl wurde gesetzt\n");
	  break;

	case '2':
	  printf ("alter Befehl 2: %s\n", u->thisorder2);
	  printf ("neuer Befehl 2: ");
	  afgets (buf, MAXLINE);
	  u->thisorder2 = crealloc (strlen (buf), u->thisorder2);
	  strcpy (u->thisorder2,  buf);
	  changed = 1;
	  printf ("Befehl 2 wurde gesetzt\n");
	  break;

	case 'M':
	  e_edit_unit_magic(u);
	  break;

	case 'T':
	  e_edit_unit_skills(u);
	  break;

	case 'q':
	  return;

	case '?':
	  printf (" z - Daten zeigen\n"
		  " n - Name aendern\n"
		  " B - Beschreibung aendern\n"
		  " k - Kommentar aendern\n"
		  " a - Anzahl aendern\n"
		  " t - Typ aendern\n"
		  " s - Silber aendern\n"
		  " m - Mana aendern\n"
		  " h - Hits aendern\n"
		  " e - Zaubereffekt aendern\n"
		  " Z - Zauberzeit aendern\n"
		  " p - Partei aendern\n"
		  " g - Gebaeude aendern\n"
		  " S - Schiff aendern\n"
		  " A - Status editieren\n"
		  " l - letzter Befehl aendern\n"
		  " 2 - Befehl 2 aendern\n"
		  " M - Magie editieren\n"
		  " T - Talente editieren\n"
		  " q - zurueck zum Hauptmenue\n"
		  " ? - diese Hilfe anzeigen\n");
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	}
    }
}


void e_write_region(region *r)
{
  int i;
  printf ("Name: %s (%d, %d)\n"
	  "Beschreibung: %s\n"
	  "Terrain: %s\n"
	  "Baeume: %d(%d neu)   Pferde: %d(%d neu)   Bauern: %d(%d neu)\n"
	  "Geld: %d   Strassen: %d\n"
	  "Produziertes Luxusgut: %s\n",
	  r->name, r->x, r->y,
	  r->display,
	  strings[terrainnames[r->terrain]][L_DEUTSCH],
	  r->trees, r->newtrees, r->horses, r->newhorses, r->peasants, r->newpeasants,
	  r->money, r->road,
	  strings[itemnames[0][FIRSTLUXURY+r->produced_good]][L_DEUTSCH]);
  for (i = 0; i < MAXLUXURIES; i+=2)
    {
      if (i < MAXLUXURIES-1)
	printf ("%20s: %5d   %20s: %5d\n", strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], r->demand[i], strings[itemnames[0][FIRSTLUXURY+i+1]][L_DEUTSCH], r->demand[i+1]);
      else
	printf ("%20s: %5d\n", strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], r->demand[i]);
    }

  for (i = 0; i < MAXHERBS; i+=2)
    {
      if (i < MAXHERBS-1)
	printf ("Kraut %2d: %-5d   Kraut %2d: %-5d\n", i, r->herbs[i], i+1, r->herbs[i+1]);
      else
	printf ("Kraut %2d: %-5d\n", i, r->herbs[i]);
    }
}


void e_edit_herbs(region *r)
{
  int i;
  while (1)
    {
      printf ("Nummer des zu editierenden Krauts: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXHERBS) break;
      printf ("Es gibt kein Kraut mit dieser Nummer!\n");
    }
  printf ("alter Wert von Kraut %2d: %d\n", i, r->herbs[i]);
  printf ("neuer Wert: ");
  afgets (buf, MAXLINE);
  r->herbs[i] = atoi(buf);
  changed = 1;
  printf ("Wert wurde gesetzt\n");
}


void e_edit_terraintype(region *r)
{
  int i;
  printf ("alter Terraintyp: %s\n", strings[terrainnames[r->terrain]][L_DEUTSCH]);
  printf ("Typen:\n");
  for (i = 0; i<MAXTERRAINS; i+=2)
    {
      if (i < MAXTERRAINS-1)
	{
	  printf("%2d: %-20s   %2d: %-20s\n", i, strings[terrainnames[i]][L_DEUTSCH], i+1, strings[terrainnames[i+1]][L_DEUTSCH]);
	} else {
	  printf("%2d: %s", i, strings[terrainnames[i]][L_DEUTSCH]);
	}
    }
  while (1)
    {
      printf ("neuer Terraintyp: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXTERRAINS && i>=0) break;
      printf ("Es gibt keinen Terraintyp mit dieser Nummer!\n");
    }
  r->terrain = i;
  changed = 1;
  printf ("Terraintyp wurde gesetzt\n");
}


void e_edit_luxuries(region *r)
{
  int i;
  while (1)
    {
      printf ("Luxusgueter:\n");
      for (i = 0; i < MAXLUXURIES; i+=2)
	{
	  if (i < MAXLUXURIES-1)
	    printf ("%2d: %-20s   %2d: %-20s\n", i, strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], i+1, strings[itemnames[0][FIRSTLUXURY+i+1]][L_DEUTSCH]);
	  else
	    printf ("%2d: %-20s\n", i, strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH]);
	}
      printf ("Nummer des zu editierenden Luxusguts: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXLUXURIES && i>=0) break;
      printf ("Es gibt kein Luxusgut mit dieser Nummer!\n");
    }
  printf ("alter Wert des Luxusguts %2d: %d\n", i, r->demand[i]);
  printf ("neuer Wert: ");
  afgets (buf, MAXLINE);
  r->demand[i] = atoi(buf);
  changed = 1;
  printf ("Wert wurde gesetzt\n");
}


void e_edit_produced_good(region *r)
{
  int i;
  printf ("altes produziertes Gut: %s\n", strings[itemnames[0][FIRSTLUXURY+r->produced_good]][L_DEUTSCH]);
  printf ("Luxusgueter:\n");
  for (i = 0; i < MAXLUXURIES; i+=2)
    {
      if (i < MAXLUXURIES-1)
	printf ("%2d: %-20s   %2d: %20s\n", i, strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH], i+1, strings[itemnames[0][FIRSTLUXURY+i+1]][L_DEUTSCH]);
      else
	printf ("%2d: %20s\n", i, strings[itemnames[0][FIRSTLUXURY+i]][L_DEUTSCH]);
    }
  while (1)
    {    
      printf ("neues produziertes Gut: ");
      afgets (buf, MAXLINE);
      i = atoi (buf);
      if (i < MAXLUXURIES && i>=0) break;
      printf ("Es gibt keine Luxusgut mit dieser Nummer!\n");
    }
  r->produced_good = i;
  changed = 1;
  printf ("produziertes Gut wurde gesetzt\n");
}


void e_show_region()
{
  int i;
  region *r;
  r = inputregion();
  if (!r) return;
  e_write_region(r);

  while (1)
    {
      printf ("region: (? fuer Hilfe)> ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	case 'z':
	  e_write_region(r);
	  break;

	case 'n':
	  printf ("alter Name: %s\n", r->name);
	  printf ("neuer Name: ");
	  afgets (buf, NAMESIZE);
	  r->name = crealloc (strlen (buf), r->name);
	  strcpy (r->name, buf);
	  changed = 1;
	  printf ("Name wurde gesetzt\n");
	  break;

	case 'B':
	  printf ("alte Beschreibung: %s\n", r->display);
	  printf ("neue Beschreibung: ");
	  afgets (buf, DISPLAYSIZE);
	  r->display = crealloc (strlen (buf), r->display);
	  strcpy (r->display, buf);
	  changed = 1;
	  printf ("Beschreibung wurde gesetzt\n");
	  break;

	case 't':
	  e_edit_terraintype(r);
	  break;

	case 'b':
	  printf ("alte Baumzahl: %d\n", r->trees);
	  while (1)
	    {
	      printf ("neue Baumzahl: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Baumzahl ungueltig!\n");
	    }
	  r->trees = i;
	  changed = 1;
	  printf ("Baumzahl wurde gesetzt\n");
	  break;

	case 'p':
	  printf ("alte Pferdezahl: %d\n", r->horses);
	  while (1)
	    {
	      printf ("neue Pferdezahl: ");
	      afgets (buf, MAXLINE);
	      i = atoi (buf);
	      if (i >= 0) break;
	      printf ("Pferdezahl ungueltig!\n");
	    }
	  r->horses = i;
	  changed = 1;
	  printf ("Pferdezahl wurde gesetzt\n");
	  break;

	case 'P':
	  e_edit_produced_good(r);
	  break;

	case 'l':
	  e_edit_luxuries(r);
	  break;

	case 'k':
	  e_edit_herbs(r);
	  break;

	case 'q':
	  return;

	case '?':
	  printf (" z - Daten zeigen\n"
		  " n - Name aendern\n"
		  " B - Beschreibung aendern\n"
		  " t - Terraintyp aendern\n"
		  " b - Baumzahl aendern\n"
		  " p - Pferdezahl aendern\n"
		  " P - produziertes Gut aendern\n"
		  " l - Luxusgueter editieren\n"
		  " k - Kraeuter editieren\n"
		  " q - zurueck zum Hauptmenue\n"
		  " ? - diese Hilfe anzeigen\n");
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);
	}
    }
}


int main ()
{

  wants_report = pow (2, O_REPORT);
  wants_computer_report = pow (2, O_COMPUTER);
  wants_zine = pow (2, O_ZINE);
  wants_comment = pow (2, O_COMMENTS);
  wants_compressed = pow (2, O_COMPRESS);
  wants_stats = pow (2, O_STATISTICS);
  wants_debug = pow (2, O_DEBUG);
  wants_misc = pow (2, O_MISC);

  printf (
	  "\n"
	  "German Atlantis %d.%d PB(E)M Editor\n"
	  "Copyright (C) 2001 by Christoph Terwelp.\n\n"

	  "based on German Atlantis %d.%d PB(E)M host\n"
	  "Copyright (C) 1995-1998 by Alexander Schroeder.\n"
	  "Copyright (C) 1999-2001 by Tim Poepken.\n"
	  "Copyright (C) 2001 by Christoph Terwelp.\n\n"

	  "based on Atlantis v1.0\n"
	  "Copyright (C) 1993 by Russell Wallace.\n\n"

	  "German Atlantis is distributed in the hope that it will be useful,\n"
	  "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	  "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n"

	  "This program may be freely used, modified and distributed. It may\n"
	  "not be sold or used commercially without prior written permission\n"
	  "from the author.\n\n",

	  RELEASE_VERSION / 10, RELEASE_VERSION % 10, RELEASE_VERSION / 10, RELEASE_VERSION % 10);

  turn = -1;

  initgame(0);
  changed = 0;

  puts ("? zeigt die Hilfe an");
  while (1)
    {
      printf ("(? fuer Hilfe) > ");
      afgets (buf, MAXLINE);

      switch (buf[0])
	{
	  /* case 'l':
	     puts (" nicht implementiert");
	     break; */

	case 's':
	  changed = 0;
	  writesummary ();
          writegame ();
	  break;

	case 'k':
	  e_show_map();
	  break;

	case 'p':
	  e_show_faction();
	  break;

	case 'e':
	  e_show_unit();
	  break;

	case 'r':
	  e_show_region();
	  break;

	case 'q':
	  if (changed)
	    {
	      printf ("Sie haben aenderungen Vorgenommen. Sind sie sicher, dass sie beenden wollen ohne zu speichern (J/N)? ");
	      afgets (buf, MAXLINE);
	      if (buf[0] != 'J' && buf[0] != 'j') break;
	    }
	  return 0;
	  break;

	case '?':
	  printf (//" l - Runde laden\n"
		  " s - Runde speichern\n"
		  " k - Karte anzeigen\n"
		  " p - Partei bearbeiten\n"
		  " e - Einheit bearbeiten\n"
		  " r - Region bearbeiten\n"
		  " q - Beenden\n"
		  " ? - diese Hilfe anzeigen\n");
	  break;

	default:
	  if (buf[0])
	    printf (" Das Kommando \"%c\" ist unbekannt (? um die Hilfe anzuzeigen).\n", buf[0]);

	}
    }
}
