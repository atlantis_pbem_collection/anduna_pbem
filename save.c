/* Anduna PBEM Host 5.7 Copyright (C) 1999-2001 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

#include "atlantis.h"
 
#define xisdigit(c)     ((c) == '-' || ((c) >= '0' && (c) <= '9'))
#define COMMENT_CHAR		';'
#define SUMMARY_BASENAME	"parteien"
#define ADDRESS_BASENAME	"adressen"
#define LOGFILE_BASENAME	"logfile"
#define ADDRESS_MAILLISTNAME	"atlantis.pml"
//#define DEBUG_SAVED_GAME
//#define DEBUG_DATA_FILE
/* Konstanten, die man #definieren kann:

   DEBUG_SAVED_GAME: Die Daten-Datei wird beim Laden auf
   Doppeleintraege ueberprueft und alle Namen werden auf dem
   Bildschirm ausgegeben.

   DEBUG_MEMORY_USAGE: Der ungefaehre Speicherverbraucht wird mit der
   Zusammenfassung zusammen geschrieben.  
   
   DEBUG_DATA_FILE: Schreibt den Regionen-Debug und Comment ins
   Datenfile, sonst wird einfach eine 0 reingeschrieben*/

int data_version;

/* ------------------------------------------------------------- */

int
cfopen (char *filename, char *mode)
{
  F = fopen (filename, mode);

  if (F == 0)
    {
      printf ("Ich kann die Datei %s nicht im %s-Modus oeffnen.\n", filename, mode);
      return 0;
    }
  return 1;
}

int
skipline (void)
{
  int c=0;

  while (c != EOF && c != '\n')
    c = fgetc (F);
  return c;
}

void
getbuf (void)
{
  int i=0, j=0, c=0, old_c=0, quote=0;

  /* buf vorsichtshalber auf 0, i: position in buf, j: position in quote, quote: 1 wenn innerhalb eines Textes in
     "Gaensefuesschen", c: aktueller, gelesener char */
  buf[0] = 0;                   
  for (;;)
    {
      /* old_c wird verwendet, weil in buf anstelle von tabs, spaces und newlines immer ein underscore
         (SPACE_REPLACEMENT) steht (wird am ende der Schlaufe gesetzt).  */
      old_c = c;
      c = fgetc (F);

      /* Nach einem Semikolon folgen Kommentare bis zum Zeilenende.  */
      if (c == COMMENT_CHAR && !quote)
        {
          buf[i] = 0;
          c = skipline ();
        }

      /* Falls die Zeile zu lange ist, wird der Text abgebrochen, und die Zeile zuende gelesen.  Ein
         ge-quoteter Text war wahrscheinlich zu lang (Gaensefuesschen vergessen oder zuviel).  Es wird wieder
         auf normalen Zeilenmodus umgeschalten (quote=0).  Nun wird in c entweder '\n' oder EOF stehen.  Bei
         Funktionen, welche ihren eigenen buf[] definieren, muss selber auf zu grosse strings geprueft werden
         -- hier werden strings bis zur Maximallaenge eingelesen.  */
      if (i == sizeof buf - 1)
        {
          buf[i] = 0;
          c = skipline ();
	  quote = 0;
        }

      /* Bricht die Datei mitten in der Zeile ab, wird die Zeile ignoriert.  Dieser Test muss nach allen skipline
         () Aufrufen stehen, da diese auch mit EOF enden koennen.  */
      if (c == EOF)
        {
          buf[0] = EOF;
          break;
        }

      /* Befehle werden durch Zeilenende beendet.  Quotes durch eine
         Leerzeile (dh. zwei Zeilenenden hintereinander).  Die Zeile
         wird - mit oder ohne quote - durch 0 oder ';' abgebrochen
         (normalerweise werden so Kommentare ignoriert).  */
      if ((c == '\n' && !quote) ||
          (c == '\n' && quote && old_c == '\n'))
        {
          buf[i] = 0;
          if (i > 0 && buf[i - 1] == SPACE_REPLACEMENT)
            buf[i - 1] = 0;
          break;
        }

      /* quotes werden mit `"' begonnen oder beenden */
      if (c == '"')
        {
          quote = !quote;
          if (quote)
            j = 0;
          continue;
        }

      /* whitespace - alle spaces und tabs nach spaces und tabs oder
         am Anfang der Zeile oder am Anfang von strings werden
         ignoriert.  */
      if ((isspace (c) && isspace (old_c)) ||
          (j == 0 && isspace (c)))
        continue;

      j++;          /* Textlaenge seit Zeilen oder string beginn.  */

      /* In quotes werden alle spaces und tabs durch underscores
         (SPACE_REPLACEMENT) ersetzt, ansonsten wird c eingesetzt.
         Ausserhalb von quotes werden tabs durch spaces ersetzt,
         ansonsten wird auch c eingesetzt.  getstr etc operieren nur
         mit spaces. */
      if (isspace (c))
        buf[i++] = quote ? SPACE_REPLACEMENT : ' ';
      else
        buf[i++] = c;
    }
}

/* ------------------------------------------------------------- */

void
readunit (faction * f) /*zum lesen aus der Befehlsdatei*/
{
  int i;
  unit *u;
  strlist *S, **SP;

  if (!f)
    return;

  i = geti ();
  u = findunitglobal (i);

  if (u && u->faction == f)
    {
      /* SP zeigt nun auf den Pointer zur ersten StrListe.  Auf die
         erste StrListe wird mit u->orders gezeigt.  Deswegen also
         &u->orders, denn SP zeigt nicht auf die selbe stelle wie
         u->orders, sondern auf die stelle VON u->orders!  */
      SP = &u->orders;

      for (;;)
        {
          getbuf ();

          /* Erst wenn wir sicher sind, dass kein Befehl eingegeben
             wurde, checken wir, ob nun eine neue Einheit oder ein
             neuer Spieler drankommt.  */
          if (igetkeyword (buf) < 0)
            switch (igetparam (buf))
              {
              case P_UNIT:
              case P_FACTION:
              case P_NEXT:

                return;
              }

          if (buf[0] == EOF) /* Dateiende */
            return;

          if (buf[0]) /* Ab hier sind es Befehle fuer die Einheit*/
            {
              /* Nun wird eine StrListe S mit dem Inhalt buf2
                 angelegt.  S->next zeigt nirgends hin.  */
              S = makestrlist (buf);

              /* Nun werden zwei Operationen ausgefuehrt (addlist2 ist ein
                 #definiertes macro!):

                 *SP = S -- Der Pointer, auf den SP zeigte, zeigt nun
                 *auf S! Also entweder u->orders oder S(alt)->next
                 *zeigen nun auf das neue S!

                 SP = &S->next -- SP zeigt nun auf den Pointer
                 S->next.  Nicht auf dieselbe Stelle wie S->next,
                 sondern auf die Stelle VON S->next! */

              addlist2 (SP, S);

              /* Und das letzte S->next darf natuerlich nirgends mehr
                 hinzeigen, es wird auf null gesetzt.  Warum das nicht
                 bei addlist2 getan wird, ist mir schleierhaft.  So
                 wie es jetzt implementiert ist, kann man es (hier auf
                 alle Faelle) hinter addlist2 schreiben, oder auch nur
                 am Ende einmal aufrufen - das waere aber bei allen
                 returns, und am ende des for (;;) blockes.  Grmpf.
                 Dann lieber hier, immer, und dafuer sauberer... */

              *SP = 0;
            }
        }
    }
  else
    {
      sprintf (buf, "Die Einheit %d gehoert uns nicht.", i);
      addstrlist (&f->mistakes, buf);
    }
}

/* ------------------------------------------------------------- */

faction *
readfaction (void) /*zum lesen aus der Befehlsdatei*/
{
  int i;
  faction *f;
  region *r;
  unit *u;

  i = geti ();
  f = findfaction (i);

  if (f)
    {
      /* Kontrolliere, ob das Passwort richtig eingegeben wurde.  Es
         muss in "Gaensefuesschen" stehen!!  */
      if (f->passw && strcmp (f->passw, getstr ()))
        {
          addstrlist (&f->mistakes, "Das Passwort wurde falsch eingegeben");
          return 0;
        }

      /* Loesche alle alten Befehle, falls schon welche eingeben
         worden sind.  */
      for (r = regions; r; r = r->next)
        for (u = r->units; u; u = u->next)
          if (u->faction == f)
            {
              freelist (u->orders);
              u->orders = 0;
            }

      /* Die Partei hat sich zumindest gemeldet, so dass sie noch
         nicht als untaetig gilt.  */
      f->lastorders = turn;
    }
  else
    printf (" -- Befehle fuer die ungueltige Partei Nr. %d.\r\n"
	     "    Befehle:     ", i);

  return f;
}

/* ------------------------------------------------------------- */

int
readorders (void) /*zum lesen aus der Befehlsdatei*/ 
{
  faction *f;
  struct stat fs;

  /* Der Name der Befehlsdatei muss in buf drinnen stehen */

  if (!cfopen (buf, "r"))
    return 0;
  stat (buf, &fs);

  puts ("- lese Befehlsdatei...");

  getbuf ();

  /* Auffinden der ersten Partei, und danach abarbeiten bis zur
     letzten Partei.  Gleichzeitig wird ein Indicator angezeigt.  Wie
     immer kann der Indicator problemlos entfernt werden.  */
  printf ("    Befehle: ");
  indicator_reset (fs.st_size);

  f = 0;
  while (buf[0] != EOF)
    switch (igetparam (buf))
      {
	/* In readfaction wird nur eine Zeile gelesen: Diejenige mit
           dem Passwort.  Die befehle der units werden geloescht, und
           die Partei wird als aktiv vermerkt. */
      case P_FACTION:
        f = readfaction ();
        getbuf ();
        break;

        /* Falls in readunit abgebrochen wird, steht dort entweder
           eine neue Partei, eine neue Einheit oder das Fileende.  Das
           switch wird erneut durchlaufen, und die entsprechende
           Funktion aufgerufen.  Man darf buf auf alle Faelle nicht
           ueberschreiben!  Bei allen anderen Eintraegen hier, muss
           buf erneut gefuellt werden, da die betreffende Information
           in nur einer Zeile steht, und nun die naechste gelesen
           werden muss.  */
      case P_UNIT:
        if (f)
          readunit (f);
        else
          getbuf ();
        indicator_count_up (ftell (F));
	break;

      case P_NEXT:
        f = 0;
      default:
        getbuf ();
      }
  indicator_done ();
  putchar ('\n');

  fclose (F);
  return 1;
}


/* ------------------------------------------------------------- */
/* -Ab-hier-wird-aus-dem-Datenfile-gelesen---------------------- */
/* ------------------------------------------------------------- */

int nextc;

/* ------------------------------------------------------------- */

void
rc (void)
{
  nextc = fgetc (F);
}

/* Read a string from file F into array s.  No memory allocation!  */
void
rs (char *s)
{
  while (nextc != '"')
    {
      if (nextc == EOF)
        {
          puts ("Der Datenfile bricht vorzeitig ab.");
          exit (1);
        }

      rc ();
    }

  rc ();

  while (nextc != '"')
    {
      if (nextc == EOF)
        {
          puts ("Der Datenfile bricht vorzeitig ab.");
          exit (1);
        }

      *s++ = nextc;
      rc ();
    }

  rc ();
  *s = 0;
}

/* Read a string from file F to pointer s.  Allocate memory for s.  */
void
mrs (char **s)
{
  rs (buf);
  if (buf[0])
    mstrcpy (s, buf);
}

/* Read a, int from file F and return it.  */
int
ri (void)
{
  int i;
  char buf[20];

  i = 0;

  while (!xisdigit (nextc))
    {
      if (nextc == EOF)
        {
          puts ("Der Datenfile bricht vorzeitig ab.");
          exit (1);
        }

      rc ();
    }

  while (xisdigit (nextc))
    {
      buf[i++] = nextc;
      rc ();
    }

  buf[i] = 0;
  return atoi (buf);
}

/* Liest eine strlist von Diskette (zB. die Meldungen fuer eine
   Partei) und speichert sie am Ende der strliste SP ab.  */
void
rstrlist (strlist ** SP)
{
  int n;
  strlist *S;

  n = ri ();

  while (--n >= 0)
    {
      rs (buf);
      S = makestrlist (buf);
      addlist2 (SP, S);
    }

  *SP = 0;
}

/* Liest eine strlist und vergisst sie sofort wieder (lean mode).  */
void
skipstrlist ()
{
  int n;
  n = ri ();
  while (--n >= 0)
    rs (buf);
}

/*Diese Routine wird nur benutzt, wenn DEBUG_SAVED_GAME #definiert ist (s.unten)*/
void
number_space_free ()
{
  int i, m;
  
  puts ("Freie Parteien:");
  m=0;
  for (i=0; i!=5; i++)
    {
      do
	m++;
      while (findfaction (m));
      printf (" %d", m);
    }

  puts ("\nFreie Burgen:");
  m=0;
  for (i=0; i!=5; i++)
    {
      do
	m++;
      while (findbuilding (m));
      printf (" %d", m);
    }

  puts ("\nFreie Schiffe:");
  m=0;
  for (i=0; i!=5; i++)
    {
      do
	m++;
      while (findship (m));
      printf (" %d", m);
    }

  puts ("\nFreie Einheiten:");
  m=0;
  for (i=0; i!=5; i++)
    {
      do
	m++;
      while (findunitglobal (m));
      printf (" %d", m);
    }
  putchar ('\n');
}
      
/*Hier wird das Datenfile gelesen, das heisst, hier muss auch fuer
  Abwaertskompatibilitaet gesorgt werden !!*/

void
readgame (int lean)
{
  int i, n, p, error=0;
  faction *f, **fp;
  rfaction *rf,*rf2, **rfp;
  region *r, **rp;
  building *b, **bp;
  ship *sh, **shp;
  unit *u, **up;

  sprintf (buf, "data/%d", turn);
  if (!cfopen (buf, "r"))
    exit (1);
  rc ();

  /* Globale Variablen.  */

  data_version = ri ();
  turn = ri ();
  printf ("- Version: %d.%d, Runde %d.\n",
          data_version / 10, data_version % 10, turn);
  highest_unit_no = 0;


  /* Read factions.  */

  n = ri ();
  fp = &factions;

  /* fflush (stdout); */
  printf ("    Parteien: ");
#ifndef DEBUG_SAVED_GAME
  indicator_reset (n);
#endif      

  while (--n >= 0)
    {
#ifndef DEBUG_SAVED_GAME
      indicator_count_down (n);
#endif      
#ifdef DEBUG_SAVED_GAME
      indicator_tick ();
#endif      

      f = cmalloc (sizeof (faction));
      memset (f, 0, sizeof (faction));

      f->no = ri ();
      f->nunits=0;
      f->nmagicians=0;
      mrs (&f->name);
      mrs (&f->addr);
      mrs (&f->passw);
      f->lastorders = ri ();
      if (data_version > V24)
	{
	  f->newbie = ri ();
	  f->old_value = ri ();
	}
      if (data_version > V50)
	f->language = ri (); 
      f->options = ri ();
      if (data_version < V60) f->options += pow (2,O_MISC);

      if (data_version >= V60) f->peasants_like_faction = ri();
        else f->peasants_like_faction = INIT_PEASANTS_LIKE_FACTION;
      
      if (data_version >= V61) f->effect = ri (); else f->effect = 0;
      if (data_version >= V61) f->enchanted = ri (); else f->enchanted = 0;

      if (data_version >= V60)
      {
        for (i=ri();i!=0;i--)
          f->showdata[ri()] = 1;
      }
      else if (data_version >= V55)
      {
       for (i = 0; i != MAXSPELLS_V59; i++)
        f->showdata[i] = ri ();
      }
      else
      {
       for (i = 0; i != MAXSPELLS_V55; i++)
        f->showdata[i] = ri ();
      }

// HELFE-Status wird gelesen -- als Parteienliste
      if (data_version >= V60)
      {
	for (i = 0; i < MAXHELP; i++)
	  {
	    p = ri ();
	    rfp = &(f->allies[i]);

	    while (--p >= 0)
	      {
		rf = cmalloc (sizeof (rfaction));
		rf->factionno = ri ();
		addlist2 (rfp, rf);
	      }
	    *rfp = 0;
	  }
      } else {
        p = ri ();
        rfp = &(f->allies[0]);

        while (--p >= 0)
          {
            rf = cmalloc (sizeof (rfaction));
            rf->factionno = ri ();
            addlist2 (rfp, rf);
          }
        *rfp = 0;

	for (i = 1; i < MAXHELP; i++)
	  {
            rfp = &(f->allies[i]);
	    for (rf2 = f->allies[0]; rf2; rf2 = rf2->next)
	      {
                rf = cmalloc (sizeof (rfaction));
                rf->factionno = rf2->factionno;
                addlist2 (rfp, rf);
//		addlist(f->allies[i], rf->faction);
	      }
	  }
        *rfp = 0;
//	rf = 0;
      }

      if (lean)
	{
	  skipstrlist (/* &f->mistakes */);
	  skipstrlist (/* &f->warnings */);
	  skipstrlist (/* &f->messages */);
	  skipstrlist (/* &f->battles */);
	  skipstrlist (/* &f->events */);
	  skipstrlist (/* &f->income */);
	  skipstrlist (/* &f->commerce */);
	  skipstrlist (/* &f->production */);
	  skipstrlist (/* &f->movement */);
	  skipstrlist (/* &f->debug */);
	}
      else
	{
	  rstrlist (&f->mistakes);
	  rstrlist (&f->warnings);
	  rstrlist (&f->messages);
	  rstrlist (&f->battles);
	  rstrlist (&f->events);
	  rstrlist (&f->income);
	  rstrlist (&f->commerce);
	  rstrlist (&f->production);
	  rstrlist (&f->movement);
	  rstrlist (&f->debug);
	}
#ifdef DEBUG_SAVED_GAME
      if (findfaction (f->no))
	{
	  printf ("\nPartei Nr. %d kommt doppelt vor.\n-", f->no);
	  error=1;
	}
#endif      
      addlist2 (fp, f);
    }
  putchar ('\n');

  *fp = 0;

  /* Regionen */
  n = ri ();
  assert (n);
  rp = &regions;

  printf ("    Regionen: ");
#ifndef DEBUG_SAVED_GAME
  indicator_reset (n);
#endif      

  while (--n >= 0)
    {

#ifndef DEBUG_SAVED_GAME
      indicator_count_down (n);
#endif      

      r = cmalloc (sizeof (region));
      memset (r, 0, sizeof (region));

      r->x = ri ();
      r->y = ri ();
      mrs (&r->name);
      mrs (&r->display);
      r->terrain = ri ();
      r->trees = ri ();
      r->horses = ri ();
      r->peasants = ri ();
      r->money = ri ();
      r->road = ri ();
      if (data_version >= V57) 
      {
      	r->buekaz = ri ();
      	r->specialresource = ri ();
      	r->touched = ri ();
      	if (data_version >= V58) r->seen = ri (); else r->seen = 0;
      	for (i=0;i!=MAXHERBS;i++) r->herbs[i] = ri ();
      }
      else
      {
      	r->buekaz = INIT_BUEKAZ;
      	r->specialresource = 0;
      	r->touched = 0;
      	r->seen = 0;
      	for (i=0;i!=MAXHERBS;i++) r->herbs[i] = INIT_HERBS;
      }
      if (data_version >= V61) r->effect = ri (); else r->effect = 0;
      if (data_version >= V61) r->enchanted = ri (); else r->enchanted = 0;

#ifdef DEBUG_SAVED_GAME
      if (findregion (r->x, r->y))
	{
	  printf ("\nRegion (%d,%d) kommt doppelt vor.\n-", r->x, r->y);
	  error=1;
	}
#endif      
      addlist2 (rp, r);

      assert (r->x == sphericalx (r->x));
      assert (r->y == sphericaly (r->y));
      assert (r->terrain >= 0);
      assert (r->trees >= 0);
      assert (r->horses >= 0);
      assert (r->peasants >= 0);
      assert (r->money >= 0);
      assert (r->road >= 0);

      for (i = 0; i != MAXLUXURIES; i++)
	{
	  r->demand[i] = ri ();
	  if (data_version < V40)
	    {
	      if (r->demand[i])
		r->demand[i] *= 100;
	      else
		{
		  r->demand[i] = 100;
		  r->produced_good = i;
		}
	    }
	}
      if (data_version >= V40)
	r->produced_good = ri ();

/* Regionendebug wird normalerweise nicht mehr geschrieben, aber eine
   Null als Laenge der strlist, sollte also kein Problem darstellen.*/
      rstrlist (&r->comments);
      rstrlist (&r->debug);

      /* Burgen */
      p = ri ();
      bp = &r->buildings;

      while (--p >= 0)
        {
          b = cmalloc (sizeof (building));
          memset (b, 0, sizeof (building));

          b->no = ri ();
          mrs (&b->name);
          mrs (&b->display);
          b->size = ri ();
          if (data_version >= V55) b->kind = ri ();
          if (data_version <  V56) b->kind=BD_CASTLE;
          if (data_version >= V61) b->effect = ri (); else b->effect = 0;
          if (data_version >= V61) b->enchanted = ri (); else b->enchanted = 0;

#ifdef DEBUG_SAVED_GAME
	  if (findbuilding (b->no))
	    {
	      printf ("\nBurg Nr. %d kommt doppelt vor.\n-", b->no);
	      error=1;
	    }
#endif      
          addlist2 (bp, b);
        }

      *bp = 0;

      /* Schiffe */
      p = ri ();
      shp = &r->ships;

      while (--p >= 0)
        {
          sh = cmalloc (sizeof (ship));
          memset (sh, 0, sizeof (ship));

          sh->no = ri ();
          mrs (&sh->name);
          mrs (&sh->display);
          sh->type = ri ();
          sh->left = ri ();
          if (data_version >= V61) sh->effect = ri (); else sh->effect = 0;
          if (data_version >= V61) sh->enchanted = ri (); else sh->enchanted = 0;

#ifdef DEBUG_SAVED_GAME
	  if (findship (sh->no))
	    {
	      printf ("\nSchiff Nr. %d kommt doppelt vor.\n-", sh->no);
	      error=1;
	    }
#endif      
          addlist2 (shp, sh);
        }

      *shp = 0;

      /* Einheiten */
      p = ri ();
      up = &r->units; /* up zeigt immer auf die letzte Einheit.  */

      while (--p >= 0)
        {
#ifdef DEBUG_SAVED_GAME
	  indicator_tick ();
#endif 
          u = cmalloc (sizeof (unit));
          memset (u, 0, sizeof (unit));

          u->no = ri ();
	  if (u->no > highest_unit_no)
	    highest_unit_no = u->no;
          mrs (&u->name);
          mrs (&u->display);
          if (data_version >= V57) mrs (&u->playercomment); else u->playercomment=0;
#ifdef DEBUG_SAVED_GAME
//	    printf("%d ",u->no);
//	    puts(u->name);
//	    puts(u->display);
//	    puts(u->playercomment);
#endif 
          u->number = ri ();
          u->type = ri ();
          u->money = ri ();
          if (data_version >= V59) {
          	u->mana=ri();
          	u->hits=ri();
          }
          else {
        	u->mana=0;
        	u->hits=0;
          }
        
	  u->effect = ri ();
          if (data_version < V24 && u->type != U_ILLUSION)
	    u->effect = 0;
          if (data_version >= V24)
	    u->enchanted = ri ();
          u->faction = findfaction (ri ());
          u->building = findbuilding (ri ());
          u->ship = findship (ri ());
          u->owner = ri ();
          u->status = ri ();
          u->guard = ri ();

	  /* Default Befehle gibt es nur fuer Menschen!  rs (buf)
             bedeuted, dass dass der gelesene String einfach
             weggeworfen wird.  Dort sollte sowieso nichts stehen (bei
             Monstern).  */
	  if (u->type == U_MAN)
	    mrs (&u->lastorder);
	  else
	    rs (buf);
	  if (data_version >= V32)
	    {
	      if (u->type == U_MAN)
		mrs (&u->thisorder2);
	      else
		rs (buf);
	    }

          if (data_version < V60)
          {
            for (i=0;i!=MAXCOMBATSPELLS;i++) 
            {
            	u->combatspell[i]=-1;
            	u->combatspellmana[i]=0;
            }
            u->combatspell[CS_DURING] = ri ();
            u->combatspellmana[CS_DURING] = 0; // Zero, so all mana will be spent
          }
	  else
	  {
	    for (i=0;i!=MAXCOMBATSPELLS;i++)
	    {
	    	u->combatspell[i] = ri();
	    	u->combatspellmana[i] = ri();
	    }
	  }

#ifdef DEBUG_SAVED_GAME
	  if (findunitglobal (u->no))
	    {
	      printf ("\nEinheit Nr. %d kommt doppelt vor. (%s)\n-", u->no, u->faction->name);
	      error=1;
	    }
#endif      

          assert (u->number >= 0);
          assert (u->money >= 0);
          assert (u->type >= 0);
          assert (u->effect >= 0);

     	if (data_version < V55) 
     	{
          for (i = 0; i != MAXSKILLS_V55; i++) 
          /*Old Version has 7 Skill-slots less*/
	    u->skills[i] = ri ();
          for (i = MAXSKILLS_V55; i != MAXSKILLS; i++)
	    u->skills[i] = 0;
//      printf ("\nSKILLS LOADED: %d\n-",i);
	  
	  /* Items need special attention, since new slots have
	     been added in several places ! */
          for (i = 0; i <= I_STONE; i++)
	    u->items[i] = ri ();
//      printf ("\n RES LOADED: %d\n-",i);
	  for (i = I_STONE+1; i!=I_HORSE;i++)
	    u->items[i] = 0;
//      printf ("\n RES2 LOADED: %d\n-",i);
          for (i = I_HORSE; i <= I_LONGBOW; i++)
	    u->items[i] = ri ();
//      printf ("\n PROD LOADED: %d\n-",i);
	  for (i = I_LONGBOW+1; i!=I_CHAIN_MAIL;i++)
	    u->items[i] = 0;
//      printf ("\n PROD2 LOADED: %d\n-",i);
          for (i = I_CHAIN_MAIL; i <= I_DRAGON_PLATE; i++)
	    u->items[i] = ri ();
//      printf ("\n MAG LOADED: %d\n-",i);
	  for (i = I_DRAGON_PLATE+1; i!=I_4;i++)
	    u->items[i] = 0;
//      printf ("\n MAG2 LOADED: %d\n-",i);
	  u->items[I_4] = ri ();

	  /*Old Version has 10 Spell-slots less*/
          for (i = 0; i != MAXSPELLS_V55; i++)  
            u->spells[i] = ri ();
          for (i = MAXSPELLS_V55; i != MAXSPELLS; i++)  
            u->spells[i] = 0;
	}
	else if (data_version < V58) /* Newer DataFile Version with more slots*/
	{
          for (i = 0; i != MAXSKILLS_V58; i++)
	    u->skills[i] = ri ();
          for (i = 0; i != MAXITEMS_V58; i++)
	    u->items[i] = ri ();
    	 /* TPWORK: spells nur laden, wenn skills[SK_MAGIC] > 0 */
     	  if (is_magician(u))
           for (i = 0; i != MAXSPELLS_V59; i++)  
            u->spells[i] = ri ();
	}
	else /*Newest version has no slots anymore, so I don't need to change the file!*/
	{
	  /*initialisieren - ist das noetig?*/
	  for (i=0;i!=MAXSKILLS;i++) u->skills[i]=0;
          for (i=0;i!=MAXITEMS;i++) u->items[i]=0;
	  for (i=0;i!=MAXSPELLS;i++) u->spells[i]=0;  
	  //skills
	  for (i=ri();i!=0;i--) u->skills[ri()]=ri();
	  //items
	  for (i=ri();i!=0;i--) u->items[ri()]=ri();
	  //spells
	  if (is_magician(u)) for (i=ri();i!=0;i--) u->spells[ri()]=1; //spells hat man genau einmal
	}

          if (data_version < V20 && u->owner && r->units)
            {
              /* Eigentuemer am Anfang, falls sie nicht am Anfang sind */

              u->next = r->units;
              r->units = u;
            }
          else
            addlist2 (up, u);
            
          /*Fuer die Parteienstatistik wollen wir zaehlen, wie viele Einheiten
            eine Partei hat und wie viele Magier sie beschaeftigt*/
            
          u->faction->nunits++;
          if (is_magician(u) ) u->faction->nmagicians+=u->number;
        }

      *up = 0;
    }
  putchar ('\n');

  *rp = 0;

  /* Link rfaction structures */

  puts ("- Daten der Parteien durchgehen...");

  for (f = factions; f; f = f->next)
    for (i = 0; i < MAXHELP; i++)
      {
	for (rf = f->allies[i]; rf; rf = rf->next)
	  rf->faction = findfaction (rf->factionno);
      }

  for (r = regions; r; r = r->next)
    {
      /* Initialize faction seendata values */

      for (u = r->units; u; u = u->next)
        for (i = 0; i != MAXSPELLS; i++)
          if (u->spells[i])
            u->faction->seendata[i] = 1;

      /* Check for alive factions */

      for (u = r->units; u; u = u->next)
        u->faction->alive = 1;

    }

  connectregions ();
  fclose (F);

#ifdef DEBUG_SAVED_GAME
  number_space_free ();
  printf("Letzte Einheit: %d\n",highest_unit_no);
#endif      
  assert (!error);
}

/* ------------------------------------------------------------- */
// Funktionen zum Bewerten der einzelnen Parteien
//
int
value (faction *f)
{
  return (f->number * 50 + f->money);
}

int
value_magic (faction *f)
{
  region *r;
  unit *u;
  int val = 0;
  int i;
  for (r=regions; r; r = r->next)
    for (u = r->units; u; u = u->next)
      if (u->faction == f) {
        val += u->skills[SK_MAGIC];
        for (i = SK_SCHOOL_OF_FIRE; i != SK_SCHOOL_OF_ASTRAL+1; i++)
          val += u->skills[i];
        if (is_magician(u)) val += u->mana;
        for (i = FIRST_MAGIC_ITEM; i != LAST_MAGIC_ITEM+1; i++) 
          val += u->items[i]*20; // Ein Item ist eine Lernrunde = 30 Lerntage - 10 Lerntage (Anwendungsbonus) wert.
      }
  return val;
}

int
value_sea (faction *f)
{
  region *r;
  unit *u;
  ship *sh;
  int val = 0;
  for (r=regions; r; r = r->next) {
    for (u = r->units; u; u = u->next)
      if (u->faction == f) {
        val += u->skills[SK_SAILING]/10;
        val += u->skills[SK_SHIPBUILDING]/10;
        val += u->skills[SK_LUMBERJACK]/100;
      }
    for (sh = r->ships; sh; sh = sh->next)
      for (u = r->units; u; u = u->next)
        if (u->faction == f && u->ship == sh && u->owner) {
          if (!sh->left) val += shipcapacity[sh->type];
          else if (sh->type == SH_RAFT) val += sh->left ;
          else val += (shipcost[sh->type] - sh->left);
        }  
  }
  return val;
}

int
value_battle (faction *f)
{
  region *r;
  unit *u;
  int val = 0;
  for (r=regions; r; r = r->next) {
    for (u = r->units; u; u = u->next)
      if (u->faction == f) {
        val += effskill(u,SK_SWORD) * min(u->number,u->items[I_SWORD]);
        val += effskill(u,SK_SWORD) * 100*min(u->number,u->items[I_RUNESWORD]);
        val += effskill(u,SK_SWORD) * 100*min(u->number,u->items[I_FLAMESWORD]);
        val += effskill(u,SK_CROSSBOW) * min(u->number,u->items[I_CROSSBOW]);
        val += effskill(u,SK_LONGBOW) * min(u->number,u->items[I_LONGBOW]);
        val += effskill(u,SK_SPEAR) * min(u->number,u->items[I_SPEAR]);
        val += effskill(u,SK_CATAPULT) * min(u->number,u->items[I_CATAPULT]);
        val += 4*min(u->number,u->items[I_PLATE_ARMOR]);
        val += 2*min(u->number,u->items[I_CHAIN_MAIL]);
      }
  }
  return val;
}

int
growth (faction *f)
{
  int n;

  n = 100 * (value (f) - f->old_value);
  if (f->old_value)
    return (n / f->old_value);
  else if (value (f))
    return (n / value (f));
  else
    return n;
  }
/* ------------------------------------------------------------- */

void
wc (int c)
{
  fputc (c, F);
}

void
wsn (char *s)
{
  while (*s)
    wc (*s++);
}

void
wnl (void)
{
  wc ('\n');
}

void
wspace (void)
{
  wc (' ');
}

void
ws (char *s)
{
  wc ('"');
  if (s)
    wsn (s);
  wc ('"');
}

void
wi (int n)
{
  sprintf (buf, "%d", n);
  wsn (buf);
}

/* gibt eine Stringliste ins Datenfile aus */
void
wstrlist (strlist * S)
{
  wi (listlen (S));
  wnl ();

  while (S)
    {
      ws (S->s);
      wnl ();
      S = S->next;
    }
}

/* Vor dem Aufruf von writegame muss writesummary () aufgerufen
   werden, damit der Wert value () der Partei berechnet werden kann
   (unter Verwendung von f->money etc).  */
void
writegame (void)
{
  int i;
  int j;
  faction *f;
  rfaction *rf;
  region *r;
  building *b;
  ship *sh;
  unit *u;

  sprintf (buf, "data/%d", turn);
  if (!cfopen (buf, "w"))
    return;

  printf ("Schreibe die %d. Runde...\n", turn);

  /* globale Variablen */
  wi (RELEASE_VERSION);
  wnl ();
  wi (turn);
  wnl ();

  /* Parteien */
  wi (listlen (factions));
  wnl ();

  for (f = factions; f; f = f->next)
    {
      wi (f->no);
      wspace ();
      ws (f->name);
      wspace ();
      ws (f->addr);
      wspace ();
      ws (f->passw);
      wspace ();
      wi (f->lastorders);
      wspace ();
      wi (f->newbie);
      wspace ();
      wi (value(f));
      wspace ();
      wi (f->language);
      wspace ();
      wi (f->options);
      wspace ();
      wi (f->peasants_like_faction);
      wspace ();
      wi (f->effect);
      wspace ();
      wi (f->enchanted);
      wnl ();

      j=0;
      for (i = 0; i != MAXSPELLS; i++) if (f->showdata[i]>0) j++;
      wi(j);
      for (i = 0; i != MAXSPELLS; i++) if (f->showdata[i]>0)
      {
        wspace ();
        wi (i); // spells kann man nur einmal haben, also entfaellt die Anzahl
      }

/*      for (i = 0; i != MAXSPELLS; i++)
        {
          wspace ();
          wi (f->showdata[i]);
        }
*/      wnl ();


      for (i = 0; i < MAXHELP; i++)
	{
	  wi (listlen (f->allies[i]));
	  for (rf = f->allies[i]; rf; rf = rf->next)
	    {
	      wspace ();
	      wi (rf->faction->no);
	    }
	  wnl ();
	}

      wstrlist (f->mistakes);
      wstrlist (f->warnings);
      wstrlist (f->messages);
      wstrlist (f->battles);
      wstrlist (f->events);
      wstrlist (f->income);
      wstrlist (f->commerce);
      wstrlist (f->production);
      wstrlist (f->movement);
      wstrlist (f->debug);

      wnl (); /* plus leerzeile */
    }

  /* Regionen */

  wi (listlen (regions));
  wnl ();

  for (r = regions; r; r = r->next)
    {
      wnl (); /* plus leerzeile */

      wi (r->x);
      wspace ();
      wi (r->y);
      wspace ();
      ws (r->name);
      wspace ();
      ws (r->display);
      wspace ();
      wi (r->terrain);
      wspace ();
      wi (r->trees);
      wspace ();
      wi (r->horses);
      wspace ();
      wi (r->peasants);
      wspace ();
      wi (r->money);
      wspace ();
      wi (r->road);
      wspace ();
      wi (r->buekaz);
      wspace ();
      wi (r->specialresource);
      wspace ();
      wi (r->touched);
      wspace ();
      wi (r->seen);
      for (i=0;i!=MAXHERBS;i++) {
      	wspace();
      	wi(r->herbs[i]);
      }
      wspace ();
      wi (r->effect);
      wspace ();
      wi (r->enchanted);
      wnl ();

      assert (r->terrain >= 0);
      assert (r->trees >= 0);
      assert (r->horses >= 0);
      assert (r->peasants >= 0);
      assert (r->money >= 0);
      assert (r->road >= 0);

      for (i = 0; i != MAXLUXURIES; i++)
        {
          wspace ();
          wi (r->demand[i]);
        }
      wspace ();
      wi (r->produced_good);
      wnl ();

/* Der Regionendebug wird nur noch auf Anforderung geschrieben,
   um das Datenfile klein zu halten TP, 3.1.2000*/
#ifdef DEBUG_DATA_FILE
      wstrlist (r->comments);
      wstrlist (r->debug);
#else
      wi (0);
      wnl ();
      wi (0);
      wnl ();
#endif
      wi (listlen (r->buildings));
      wnl ();
      for (b = r->buildings; b; b = b->next)
        {
          wspace ();
          wi (b->no);
          wspace ();
          ws (b->name);
          wspace ();
          ws (b->display);
          wspace ();
          wi (b->size);
          wspace ();
          wi (b->kind);
          wspace ();
          wi (b->effect);
          wspace ();
          wi (b->enchanted);
          wnl ();
        }

      wi (listlen (r->ships));
      wnl ();
      for (sh = r->ships; sh; sh = sh->next)
        {
          wspace ();
          wi (sh->no);
          wspace ();
          ws (sh->name);
          wspace ();
          ws (sh->display);
          wspace ();
          wi (sh->type);
          wspace ();
          wi (sh->left);
          wspace ();
          wi (sh->effect);
          wspace ();
          wi (sh->enchanted);
          wnl ();
        }

      wi (listlen (r->units));
      wnl ();
      for (u = r->units; u; u = u->next)
        {
          wspace ();
          wi (u->no);
          wspace ();
          ws (u->name);
          wspace ();
          ws (u->display);
          wspace ();
          ws (u->playercomment);
          wspace ();
          wi (u->number);
          wspace ();
          wi (u->type);
          wspace ();
          wi (u->money);
          wspace ();
          wi (u->mana);
          wspace ();
          wi (u->hits);
          wspace ();
          wi (u->effect);
          wspace ();
          wi (u->enchanted);
          wspace ();
          wi (u->faction->no);
          wspace ();
          if (u->building)
            wi (u->building->no);
          else
            wi (0);
          wspace ();
          if (u->ship)
            wi (u->ship->no);
          else
            wi (0);
          wspace ();
          wi (u->owner);
          wspace ();
          wi (u->status);
          wspace ();
          wi (u->guard);
          wspace ();
          ws (u->lastorder);
          wspace ();
          ws (u->thisorder2);
          wspace ();
          for (i=0;i!=MAXCOMBATSPELLS;i++)
          {
            wi (u->combatspell[i]);
            wspace ();
            wi (u->combatspellmana[i]);
            wspace ();
          }  
          wnl ();

          assert (u->number >= 0);
          assert (u->money >= 0);
          assert (u->type >= 0);
          assert (u->effect >= 0);

/*TPWORK: Nur die Skills usw schreiben, die ungleich null sind!*/
	  j=0;
          for (i = 0; i != MAXSKILLS; i++) if (u->skills[i]>0) j++;
	  wi(j);
          for (i = 0; i != MAXSKILLS; i++) if (u->skills[i]>0)
            {
	      wspace ();
              wi (i);
	      wspace ();
              wi (u->skills[i]);
            }
          wnl ();

	  j=0;
          for (i = 0; i != MAXITEMS; i++) if (u->items[i]>0) j++;
	  wi(j);
          for (i = 0; i != MAXITEMS; i++) if (u->items[i]>0)
            {
	      wspace ();
              wi (i);
	      wspace ();
              wi (u->items[i]);
            }
          wnl ();
    /* spells nur schreiben, wenn skills[SK_MAGIC] > 0 , das haelt das Datenfile kleiner*/
          if (is_magician(u))
          {
            j=0;
            for (i = 0; i != MAXSPELLS; i++) if (u->spells[i]>0) j++;
	    wi(j);
            for (i = 0; i != MAXSPELLS; i++) if (u->spells[i]>0)
              {
	        wspace ();
                wi (i); // spells kann man nur einmal haben, also entfaellt die Anzahl
              }
            wnl ();
          }

        }
    }
  fclose (F);
}

/* ------------------------------------------------------------- */

void
output_addresses (void)
{
  faction *f;

  /* adressen liste */

  for (f = factions; f; f = f->next)
    {
      rparagraph (factionid (f), 0, 0);
      if (f->addr)
	rparagraph (f->addr, 4, 0);
      fputs ("\n", F);
    }
}

void
output_netaddresses (void)
{
  faction *f;
  for (f = factions; f; f = f->next)
    if (netaddress (f->addr)) /*kopiert netaddress in buf */
      rparagraph(buf,0,0);
}

void
writenetaddresses ()
{
  sprintf (buf, "%s", ADDRESS_MAILLISTNAME);
  if (!cfopen (buf, "w"))
    return;
  printf ("Schreibe Mailingliste (%s)...\n", buf);
  output_netaddresses ();
  fclose (F);
}

void
showaddresses ()
{
  F = stdout;
  output_addresses ();
}

void
writeaddresses ()
{
  sprintf (buf, "%s.%d", ADDRESS_BASENAME, turn);
  if (!cfopen (buf, "w"))
    return;
  printf ("Schreibe Liste der Adressen (%s)...\n", buf);
  output_addresses ();
  fclose (F);
}

long int
mem_str (char *s)
{
  return s ? strlen (s) : 0;
}

long int
mem_strlist (strlist *S)
{
  long int m=0;
  while (S)
    {
      m += mem_str (S->s);
      S = S->next;
    }
  return m;
}

void
writesummary (void)
{
  int inhabitedregions=0;
  int peasants=0;
  int peasantmoney=0;
  int nunits=0;
  int playerpop=0;
  int playermoney=0;
  int armed_men=0;
  int i, nmrs[ORDERGAP], newbies=0;
  faction *f;
  region *r;
  unit *u;
  faction *fbestvalue[5];
  faction *fbestgrowth[5];
  faction *fbestbattle[5];
  faction *fbestsea[5];
  faction *fbestmagic[5];
  int gotitalready,j;
  int ibestvalue,ibestgrowth;
  int ibestbattle,ibestsea,ibestmagic;
#ifdef DEBUG_MEMORY_USAGE
  long int mem_factions=0;
  long int mem_regions=0;
  long int mem_buildings=0;
  long int mem_ships=0;
  long int mem_units=0;
  long int mem_orders=0;
  runit *ru;
  building *b;
  ship *sh;
  rfaction *rf;
#endif

  sprintf (buf, "%s.%d", SUMMARY_BASENAME, turn);
  if (!cfopen (buf, "w"))
    return;
  printf ("Schreibe Zusammenfassung (%s)... ", buf);

  for (f = factions; f; f = f->next)
    {
      indicator_tick ();
      f->nregions = 0;
      f->nunits = 0;
      f->number = 0;
      f->money = 0;
#ifdef DEBUG_MEMORY_USAGE
      mem_factions += sizeof (faction);
      mem_factions += mem_str (f->name);
      mem_factions += mem_str (f->addr);
      mem_factions += mem_str (f->passw);
      for (i = 0; i<MAXHELP; i++)
	for (rf = f->allies[i]; rf; rf = rf->next)
	  mem_factions += sizeof (rfaction);
      mem_factions += mem_strlist (f->mistakes);
      mem_factions += mem_strlist (f->warnings);
      mem_factions += mem_strlist (f->messages);
      mem_factions += mem_strlist (f->battles);
      mem_factions += mem_strlist (f->events);
      mem_factions += mem_strlist (f->income);
      mem_factions += mem_strlist (f->commerce);
      mem_factions += mem_strlist (f->production);
      mem_factions += mem_strlist (f->movement);
      mem_factions += mem_strlist (f->debug);
#endif
    }
      
  /* Alles zaehlen */
  for (r = regions; r; r = r->next)
    if (r->peasants || r->units)
      {
	indicator_tick ();
        inhabitedregions++;
        peasants += r->peasants;
        peasantmoney += r->money;

#ifdef DEBUG_MEMORY_USAGE
	mem_regions += sizeof (region);
	mem_regions += mem_str (r->name);
	mem_regions += mem_str (r->display);
	mem_regions += mem_strlist (r->comments);
	mem_regions += mem_strlist (r->debug);
	for (sh = r->ships; sh; sh = sh->next)
	  {
	    mem_ships += sizeof (ship);
	    mem_ships += mem_str (sh->name);
	    mem_ships += mem_str (sh->display);
	  }
	for (b = r->buildings; b; b = b->next)
	  {
	    mem_buildings += sizeof (building);
	    mem_buildings += mem_str (b->name);
	    mem_buildings += mem_str (b->display);
	  }
#endif

        /* nregions darf nur einmal pro Partei per Region
           incrementiert werden. */
        for (f = factions; f; f = f->next)
          f->dh = 0;
        for (u = r->units; u; u = u->next)
          {
            nunits++;
            playerpop += u->number;
            playermoney += u->money;
            armed_men += armedmen (u);

            u->faction->nunits++;
            u->faction->number += u->number;
            u->faction->money += u->money;

            if (!u->faction->dh)
              u->faction->nregions++;
            u->faction->dh = 1;

#ifdef DEBUG_MEMORY_USAGE
	mem_units += sizeof (unit);
	mem_units += mem_str (u->name);
	mem_units += mem_str (u->display);
	for (ru = u->contacts; ru; ru = ru->next)
	  mem_units += sizeof (runit);
	mem_orders += mem_strlist (u->orders);
#endif
          }
      }

  fprintf (F, "Zusammenfassung fuer Anduna %s\n\n",
           gamedate (findfaction (0)));

  fprintf (F, "Regionen:\t\t%d\n", listlen (regions));
  fprintf (F, "Bewohnte Regionen:\t%d\n\n", inhabitedregions);

  fprintf (F, "Parteien:\t\t%d\n", listlen (factions));
  fprintf (F, "Einheiten:\t\t%d\n\n", nunits);

  fprintf (F, "Spielerpopulation:\t%d\n", playerpop);
  fprintf (F, " davon bewaffnet:\t%d\n", armed_men);
  fprintf (F, "Bauernpopulation:\t%d\n", peasants);
  fprintf (F, "Population gesamt:\t%d\n\n", playerpop + peasants);

  fprintf (F, "Reichtum Spieler:\t$%d\n", playermoney);
  fprintf (F, "Reichtum Bauern:\t$%d\n", peasantmoney);
  fprintf (F, "Reichtum gesamt:\t$%d\n\n", playermoney + peasantmoney);

#ifdef DEBUG_MEMORY_USAGE
  fputs ("Belegter Speicher\n\n", F);
  fprintf (F, "fuer Parteien:\t\t%ld\n", mem_factions);
  fprintf (F, "fuer Regionen:\t\t%ld\n", mem_regions);
  fprintf (F, "fuer Burgen:\t\t%ld\n", mem_buildings);
  fprintf (F, "fuer Schiffe:\t\t%ld\n", mem_ships);
  fprintf (F, "fuer Einheiten:\t\t%ld\n", mem_units);
  fprintf (F, "fuer Befehle:\t\t%ld\n\n", mem_orders);
#endif

  /* NMRs und Newbies zaehlen. */  
  for (i = 0; i != ORDERGAP; i++)
    nmrs[i] = 0;
  newbies = 0;
  for (f = factions; f; f = f->next)
    {
      if (f->newbie)
	newbies++;
      else
	nmrs[turn - f->lastorders]++;
    }
  for (i = 0; i != ORDERGAP; i++)
    fprintf (F, "%d %s\t\t\t%d\n", i,
             i != 1 ? "NMRs:" : "NMR: ", nmrs[i]);
  fprintf (F, "Newbies:\t\t%d\n", newbies);

/* TP: Ranking automatisiert */
  for (f=factions->next;f;f=f->next) {
     if(f->number) {
    	f->value_magic = (value_magic (f) * 100)/f->number;
    	f->value_sea =   (value_sea (f)   * 1000)/f->number;
    	f->value_battle =(value_battle (f)* 1000)/f->number;
     }
     else {
    	f->value_magic = 0;
    	f->value_sea =   0;
    	f->value_battle =0;
     }
     	
  }

  for (i=0; i<5; i++)
  {
    fbestvalue[i]=0;
    fbestgrowth[i]=0;
    fbestbattle[i]=0;
    fbestsea[i]=0;
    fbestmagic[i]=0;
    ibestvalue=0;
    ibestgrowth=0;
    ibestbattle=0;
    ibestsea=0;
    ibestmagic=0;
    for (f=factions->next;f;f=f->next)
    {
    	gotitalready=0;
    	for (j=0;j<i;j++)
    		if (f==fbestvalue[j]) gotitalready=1;
    	if (value(f)>ibestvalue && !gotitalready)
    	{
    		ibestvalue=value(f);
    		fbestvalue[i]=f;
    	}
    	gotitalready=0;
    	for (j=0;j<i;j++)
    		if (f==fbestgrowth[j]) gotitalready=1;
    	if (growth(f)>ibestgrowth && !gotitalready && !f->newbie)
    	{
    		ibestgrowth=growth(f);
    		fbestgrowth[i]=f;
    	}
    	gotitalready=0;
    	for (j=0;j<i;j++)
    		if (f==fbestbattle[j]) gotitalready=1;
    	if (f->value_battle>ibestbattle && !gotitalready && !f->newbie)
    	{
    		ibestbattle=f->value_battle;
    		fbestbattle[i]=f;
    	}
    	gotitalready=0;
    	for (j=0;j<i;j++)
    		if (f==fbestsea[j]) gotitalready=1;
    	if (f->value_sea>ibestsea && !gotitalready && !f->newbie)
    	{
    		ibestsea=f->value_sea;
    		fbestsea[i]=f;
    	}
    	gotitalready=0;
    	for (j=0;j<i;j++)
    		if (f==fbestmagic[j]) gotitalready=1;
    	if (f->value_magic>ibestmagic && !gotitalready && !f->newbie)
    	{
    		ibestmagic=f->value_magic;
    		fbestmagic[i]=f;
    	}
    }
  }
  fprintf (F, "\nReiche und Grosse Voelker:\n========================\n");
  for (i = 0; i < 5; i++)
    if (fbestvalue[i])
      fprintf (F, "%d. %s (%d)\n",i+1,fbestvalue[i]->name,fbestvalue[i]->no);
  fprintf (F, "\nGroesster Zuwachs:\n========================\n");
  for (i = 0; i < 5; i++)
    if (fbestgrowth[i])
      fprintf (F, "%d. %s (%d)\n",i+1,fbestgrowth[i]->name,fbestgrowth[i]->no);
  fprintf (F, "\nBeruehmte Magier:\n========================\n");
  for (i = 0; i < 5; i++)
    if (fbestmagic[i])
      fprintf (F, "%d. %s (%d)\n",i+1,fbestmagic[i]->name,fbestmagic[i]->no);
  fprintf (F, "\nBekannte Seefahrer:\n========================\n");
  for (i = 0; i < 5; i++)
    if (fbestsea[i])
      fprintf (F, "%d. %s (%d)\n",i+1,fbestsea[i]->name,fbestsea[i]->no);
  fprintf (F, "\nGrosse Kaempfer:\n========================\n");
  for (i = 0; i < 5; i++)
    if (fbestbattle[i])
      fprintf (F, "%d. %s (%d)\n",i+1,fbestbattle[i]->name,fbestbattle[i]->no);
  
/* TP END */
  if (factions)
    fprintf (F, "\nNr.\tReg.\tEinh.\tPers.\tSilber\tWert\tZuwachs\tMagie\tSeef.\tKampf\tNMRs\n\n");
  for (f = factions; f; f = f->next)
    fprintf (F, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
             f->no, f->nregions, f->nunits, f->number,
	     f->money, value (f), growth (f),
	     f->value_magic, f->value_sea, f->value_battle,
	     f->newbie ? -1 : (turn - f->lastorders));

  fclose (F);
  putchar ('\n');
}

/* ------------------------------------------------------------- */

void
initgame (int lean)
{
  int max_turn = -1;
  DIR *dp;
  struct dirent *ep;

  /* Hier werden alle Dateien im DATA directory untersucht: Ihr Name
     wird in eine Nummer umgewandelt und die Variable max_turn enthaelt
     die hoechste dieser Nummern. In readgame () wird im Normalfall die
     Datei mit diesem Namen (der hoechsten Nummer!) geoeffnet und
     verwendet. Wird turn schon gesetzt (turn != -1), dann gibt es nur
     eine Warnung, falls es noch hoehere Nummern gibt. */
  dp = opendir ("data/");
  if (dp != NULL)
    {
      while ((ep = readdir (dp)))
	{
	  if (atoi (ep->d_name) > max_turn && ep->d_name[0] >= '0' && ep->d_name[0] <= '9')
	    max_turn = atoi (ep->d_name);
	}
      closedir (dp);
    }
  else
    mkdir ("data/", S_IRWXU);

  /* Falls turn schon gesetzt wurde (turn != -1), wird versucht, den
     file mit der Nr. turn zu lesen, ansonsten wird file Nr. max_turn
     verwendet, also der Letzte. Bei Fehlern waehrend dem Lesen des
     Spieles in readgame () wird mit exit (1) beendet. Vgl. auch
     Funktionen rs () und ri (), deswegen brechen wir hier bei einem
     Fehler auch mit exit (1) ab. */

  /* Test, ob es ueberhaupt die verlangete Nr. turn geben kann */
  if (turn != -1 && turn > max_turn)
    {
      printf ("Es gibt nur Datenfiles bis Runde %d.\n", max_turn);
      exit (1);
    }

  /* Wird keine Datei gefunden, wird ein neues Spiel erzeugt, und keine
     Datei gelesen. */
  if (max_turn == -1)
    {
      turn = 0;

      puts ("Keine Spieldaten gefunden, erzeuge neues Spiel...\n");
      mkdir ("data", 0);

      createmonsters ();
      srand (time (0));
      makeblock (0, 0);

      /* Die neu erschaffenen Strukturen werden abgespeichert. */
      writesummary ();
      writegame ();
      return;
    }

  /* Das file Nr. turn wird gelesen, aber es gibt hoehere Nummern, also
     warnen wir. */
  if (turn != -1 && max_turn > turn)
    printf ("Lese Datenfile %d, obwohl %d der Letzte ist.\n",
            turn, max_turn);

  /* Nun wird das Datenfile Nr. turn bzw. max_turn gelesen. */
  if (turn == -1)
    turn = max_turn;

  printf ("Lese Datenfile %d ...\n", turn);
  readgame (lean);

  /* Initialisieren des random number generators. */
  srand (time (0));
}


//-----------------------------------------------------------------------
// LOGFILE, damit ominoese Abstuerze besser verfolgt werden koennen

int init_logfile(char* mode)
{
  char buffer[256];
  sprintf (buffer, "%s.%d", LOGFILE_BASENAME, turn);
  LOGF = fopen (buffer, mode);

  if (LOGF == 0)
    {
      printf ("Ich kann die Datei %s nicht im %s-Modus oeffnen.\n", buffer, mode);
      return 0;
    }
  return 1;
}

int close_logfile(void)
{
  fclose (LOGF);
  return 1;
}

int log_to_file(char* buffer)
{
  fprintf(LOGF, buffer);
  fflush(LOGF);
  if(ferror(LOGF)) return 0; else return 1;
}
/* ------------------------------------------------------------- */

int
writeorders (void) /*fuer Debug: schreibt die aktuellen Befehle*/ 
{
  region *r;
  unit *u;

  if (!cfopen ("BEFDEBUG", "w"))
    return 0;

  puts ("- schreibe Befehlszuordnung...");

  for (r = regions; r; r = r->next)
  {
     wsn(parameters[P_REGION]);
     wspace();
     ws(regionid(r));
     wnl();
     for (u = r->units; u; u = u->next)
     {
       wsn(parameters[P_UNIT]);
        wspace();
        ws(unitid(u));
        wspace();
        ws(factionid(u->faction));
        wnl();
        wstrlist(u->orders);
        wsn("->");
        ws(u->thisorder);
        wnl();
     }
  }

  putchar ('\n');

  fclose (F);
  return 1;
}

