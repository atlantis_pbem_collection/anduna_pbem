#! /usr/bin/perl -w

# This Perl Script is used to automatically check mail (*.cnm) Files 
# from the Anduna/Atlantis PBEM Games and perform the adequate actions
#
# All output is written to temporary files and a batch file is written 
# to execute the proper commands. This is done to ensure that nothing
# is lost even if the mailhost gets f***ed up.
#
# The generated Batchfile tries to execute a program beep.exe which
# must be on the system path. If you don't have one, make one with
# your favourite sound.
#
# (C) 2001-2003 by Tim Poepken <ego@bredan.de>
#
#
# TODO: Subject-Line encoding support !!!
#

use FindBin;
use File::Copy;

# Global settings
$acheck='c:\\anduna\\game\\acheck';
$acheckopts='-q -s';
$tempdir='acheckout';
$processeddir='processed';

$maildir='.';
#$maildir='a:\\';

$batfile = "> $maildir\\$tempdir\\mailit.bat";

#$orderfile = ">> .\\testbefehle";
$orderfile = ">> c:\\anduna\\game\\befehle";

# get all files from the $maildir
opendir(DIR,$maildir)|| die "can't opendir $maildir: $!";
rewinddir(DIR);
@files=readdir(DIR);
closedir(DIR);

# get all files with the right extension
@mailfiles = grep(/\.cnm$/i, @files);

if ($#mailfiles < 0) {die "Exiting: nothing to do";}

# make a temporary directory where all the output will be stored
unless (-e "$maildir\\$tempdir") {mkdir("$maildir\\$tempdir","0777");}
unless (-e "$maildir\\$processeddir") {mkdir("$maildir\\$processeddir","0777");}

# make and open the batchfile that will send the mails.
open(BAT,$batfile);
print BAT "echo off\n";

# check each and every mailfile for the right subject
foreach $mailfile (@mailfiles) {
  open(FILE,$mailfile);
  @file = <FILE>;
  close(FILE);

# Fetch the subject line and strip 'Subject: '
  @subject = grep(/^Subject: /i,@file);
  if ($#subject > 0) {print "Strange header in File $mailfile: More than one subject. Using first.\n";};
  $subj = shift(@subject);
  chop($subj);
  $subj =~ s/^Subject: //i;

# This needs to be changed if other options are to be implemented
#  be tolerant with whitespace at the end of the line
  $mode = 999; # this variable finds out what to do, 999 means nothing
  
  if($subj =~ m/^[\s]*anduna befehle[\s]*$/i) { $mode = 0 }; # normal behavior
  if($subj =~ m/^[\s]*anduna vorausbefehle[\s]*\+([\d])+[\s]*$/i) { $mode = $1 };
  # if someone types "anduna vorausbefehle +0" it will be the same as "anduna befehle"

  next if ($mode == 999); # nothing to do, so switch to next mail file

# Fetch the sender from the Mailfile
  @sender = grep(/^From: /i,@file);
  $sender = shift(@sender);
  chop($sender);
  $sender =~ s/^From: //i;
  if ($sender =~ m/([-._0-9a-zA-Z]*@[-._0-9a-zA-Z]*)/) {
  $sender = $1;}
  
# See if there is a reply-to field and honor it  
  @sender = grep(/^Reply-To: /i,@file);
  if ($#sender >= 0) {
    $replyto = shift(@sender);
    chop($replyto);
    $replyto =~ s/^Reply-To: //i;
    if ($replyto =~ m/([-._0-9a-zA-Z]*@[-._0-9a-zA-Z]*)/) {
      print "Found reply-to-address, replacing sender $sender with $1.\n";
    $sender = $1;}
  }  

# print "$0"; # This is the name of the perl file being executed
# print "$FindBin::Bin"; # This ist the path of the perl file being executed

# from here on, orderfile processing takes place 
# simple mail sending should take place before and have modes greater than 99

  next if ($mode > 99);

# Convert the Email to plain text to avoid excess work and errors
# mail2txt.pl appends ".out" to the filename
  $output2 = `perl mail2txt.pl $mailfile`;
# Process the file with ACHECK and fetch the output
# Backticks execute the command and return STDOUT
  $output=`$acheck $acheckopts $FindBin::Bin\\$mailfile.out`;
# move the mail file to the processddir
  move("$maildir\\$mailfile","$maildir\\$processeddir\\$mailfile");
  
# copy the output to the tempdir, filename is $mailfile.txt
  $outputfile="$mailfile";
  $outputfile =~ s/.cnm$/.txt/i;
  open (OUT,"> $tempdir\\$outputfile");
  print OUT "Automatische Antwort des ANDUNA Mailhosts:\n\n$output2\n\n$output\n";
  close (OUT);

# Print out a line in the mailit.bat-File
  print BAT "BLAT $outputfile -t $sender -s \"Re: $subj\"\nif errorlevel 1 goto end\n";

# Append to order file
  if ($mode == 0) {
    open(ORD,$orderfile);
  }
  else {
    my $preorderfile = "$orderfile.+$mode";
    open(ORD,$preorderfile);
  }

# read the modified file and delete it
  open(FILE,"$mailfile.out");
  @file = <FILE>;
  close(FILE);
  unlink("$mailfile.out");
  
# write the output to the appropriate orderfile
  print ORD "\n;Name der Maildatei: $mailfile\n\n";
  print ORD "@file";  
  close ORD;
  
# print some information to the screen  
  print "$mailfile:\n";
  print "$subj\n";
  
  print "$sender\n";
  print "\n";
  

}
# issue beep-command and close the batch file
print BAT "\n:end\nbeep\n";
close BAT;
