#! /usr/bin/perl -w
#
# Converts any mail file into a text file holding the ASCII parts
# of the mail body. Useful for play-by-email-games. Everything, that's
# not text/plain gets thrown away. Multipart messages are properly 
# decoded, even if nesting occurs.
# The basis are the requirements of RFC 2045 to 2049 for MIME-
# compliant internet mail. The quoted-printable decoding is taken
# from the perl MIME::QuotedPrint package, while the 8bit "decoding"
# tries to avoid some common pitfalls of word-processor automatic
# correction (typographic quotes, long dashes and the like. This 
# Part will clearly evolve in the future.
#
# Copyright (C) 2001 by Tim P�pken <ego@bredan.de>
# 

$myname = 'mail2txt v1.1 beta';

# for debugging get the filename from the command line
if ($#ARGV < 0) {die "no args";}
$filename = $ARGV[0];
print " $myname: converting file: $filename\n";

# get the contents into an array
open (INFILE,$filename) || die "cannot open file $filename\n";
# slurp the whole file
undef $/;
# this would read up to the first empty line, i.e. get the header only: $/='';
$infile = <INFILE>;
close (INFILE);

# now comes the fun stuff. The mail will be passed to a recursing subroutine
# which identifies boundaries and chops the mail into parts, in turn to be tested.
# each subroutine call returns either useful DECODED text or an empty string. These are
# then concatenated to give the all-text-only representation of the mail.

$outfile = split_and_decode ($infile);

$filename = '> '.$filename.'.out';
open (OUTFILE,$filename);
print OUTFILE "$outfile";
close (OUTFILE);


# ---------------------------------------------------------------------
# recursive subroutine to split multi-part mails and decode each part properly
sub split_and_decode
{
  my $infile = shift;
#  my $infile = join('',@_);
  my $boundary;
  my $result;

  #header and body are separated by an empty line
  (my $header,my $body)=split(/\n\n/,$infile,2);
  #unfold long header lines (newline followed by whitespace --> space
  $header =~ s/\n\s+/ /g;

  # look for the content-type
  if ($header =~ m/^Content-type: (.+)$/im ) {$contype = $1;} else {die "No Content-Type\n";}

  if ($contype =~ m/multipart/i) {
    # we know that there is a multipart line,
    # let us identify the boundary string to split the text.
    if ($contype =~ m/boundary=["]?([0-9a-zA-Z'()+_,-.\/:=?]+)["]?/i) {
      $boundary = $1;
#      print "$boundary\n";
    }
    else {print " error: no boundary in presumed multipart found\n";return;}
  
    #split the mail body using the boundary information
    my @parts=split(/\n--\Q$boundary/,$body);
    #the first part contains the multipart header and will be useless
    shift(@parts);
    #the last part contains only "--" and will be useless
    pop(@parts);

#    print "$#parts\n\n";
  
 
    foreach $part (@parts) {
      $result .= split_and_decode($part);
    }
  }
  else
  {
    # if there is no multipart shit, we just need to decode the stuff into plain us-ascii
    $result = decode ($infile);
  }
  $result;
}
# ---------------------------------------------------------------------
sub decode {
  my $infile = shift;
#  my $infile = join('',@_);
  my $contype;
  my $enncoding;
  my $result;

  #header and body are separated by an empty line
  (my $header,my $body)=split(/\n\n/,$infile,2);
  #unfold long header lines (newline followed by whitespace --> space
  $header =~ s/\n\s+/ /g;

  # look for the content-type
  if ($header =~ m/^Content-type: (.+)$/im ) {$contype = $1;} else {die "No Content-Type\n";}
  if ($header =~ m/^Content-transfer-encoding: (.+)$/im ) {$encoding = $1;} else {die "No Content-Transfer-Encoding\n";}
  if ($contype =~ m/text\/plain/i) {
    if ($encoding =~ m/7bit/i) {
      $result = $body;
      print "  found plain ascii, nothing to do\n";
    } else {
      if ($encoding =~ m/quoted-printable/i) {
        print "  decoding $encoding ... OK\n";
        $body =~ s/[ \t]+?(\r?\n)/$1/g;  # rule #3 (trailing space must be deleted)
        $body =~ s/=\r?\n//g;            # rule #5 (soft line breaks)
        $body =~ s/=([\da-fA-F]{2})/pack("C", hex($1))/ge;
      }
      print "  decoding 8bit ... OK\n";
      $result = decode_8bit ($body);
    }
  } else {
    print "  skipping section $contype\n";
    $result='';
  }
  $result;
}
# ---------------------------------------------------------------------
# "decodes" some weird characters to 7bit ASCII
sub decode_8bit
{
    my $res = shift;
#    $res =~ s/�/ae/g;
#    $res =~ s/�/oe/g;
#    $res =~ s/�/ue/g;
#    $res =~ s/�/Ae/g;
#    $res =~ s/�/Oe/g;
#    $res =~ s/�/Ue/g;
#    $res =~ s/�/ss/g;
    $res =~ s/\x96/--/g;
    $res =~ s/\x93/"/g;
    $res =~ s/\x84/"/g;
    $res;
}
