/* Anduna PBEM Host 6.1 Copyright (C) 1999-2003 Tim Poepken

   based in large parts on (thanks a whole lot!):

   German Atlantis PB(E)M host 5.3 Copyright (C) 1995-1998   Alexander Schroeder

   in turn based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

#include "atlantis.h"
// #define GIVE_DEBUG
 
void
giveitem (int n, int i, unit *u, region *r, unit *u2, strlist *S)
{
  if (n > u->items[i])
    n = u->items[i];

  if (n == 0)
    {
      mistake2 (u, S, "Die Einheit hat diesen Gegenstand nicht");
      return;
    }

  u->items[i] -= n;

  if (!u2)
    {
      if (i == I_HORSE)
        r->horses += n;

      if (getunitpeasants)
	addcommerce (u->faction, translate (ST_GIVE_PEASANTS_ITEMS, u->faction->language, unitid (u), n, 
					    strings[itemnames[n != 1][i]][u->faction->language]));
      else
	addcommerce (u->faction, translate (ST_GIVE_NOBODY_ITEMS, u->faction->language, unitid (u), n, 
					    strings[itemnames[n != 1][i]][u->faction->language]));
      return;
    }

  u2->items[i] += n;

  if (u->faction != u2->faction)
    {
      addcommerce (u->faction, translate (ST_GIVE_UNIT_ITEMS, u->faction->language, unitid (u), n, 
					  strings[itemnames[n != 1][i]][u->faction->language], unitid2 (u2)));
      addcommerce (u2->faction, translate (ST_GIVE_UNIT_ITEMS, u2->faction->language, unitid (u), n, 
					   strings[itemnames[n != 1][i]][u2->faction->language], unitid2 (u2)));
    }
  else
    addevent (u->faction, translate (ST_GIVE_UNIT_ITEMS, u->faction->language, unitid (u), n, 
				     strings[itemnames[n != 1][i]][u->faction->language], unitid2 (u2)));
}

void
givemen (int n, unit * u, region * r, unit * u2, strlist * S)
{
  int i, j, k;

  if (n > u->number)
    n = u->number;

  if (n == 0)
    {
      mistake2 (u, S, strings[ST_NO_MEN_TO_GIVE][u->faction->language]);
      return;
    }

  /* Falls Magier zu einer existenten Einheit transferiert werden: */
  if (u2 && (is_magician(u)||is_magician(u2)) )
/*    // alte Version mit MAXMAGICIANS Magiern  
    {

      // Zaehle Magier in der Zielpartei:
      k = magicians (u2->faction);

      // Falls die Zieleinheit keine Magier sind, werden sie nun welche.
      if (!is_magician(u2))
        k += u2->number;

      // Falls sie in eine neue Partei kommen, zaehlen sie selber auch mit
         zum Kontigent an Magier der Zielpartei.
      if (u2->faction != u->faction)
        k += n;

      // Falls dies nun zuviele sind gibt es einen Fehler
      if (k > MAXMAGICIANS)
        {
          mistake2 (u, S, translate (ST_MAX_MAGICIANS, u->faction->language,
				     MAXMAGICIANS));
          return;
        }
    }
*/
    {

      if (!can_give_magicians(u2->faction,u,u2,n))
        {
          mistake2 (u, S, "Soviele Magier koennen nicht verwaltet werden");
          return;
        }
      give_magicians(u2->faction,u,u2,n); // sorgt nur fuer die Magierbuchhaltung
    }
    	

  k = u->number - n;

  if (!u2)
  {
      // Noch Magier loeschen, falls jemand seine magier ans Volk gibt
      // muss passieren, BEVOR skills verteilt werden !!!
      if (is_magician(u)) u->faction->nmagicians -=n;
  }

  /* Transfer Talente */
  for (i = 0; i != MAXSKILLS; i++)
    {
      j = distribute (u->number, k, u->skills[i]);
      if (u2) u2->skills[i] += u->skills[i] - j;
      u->skills[i] = j;
    }
  // Transfer Mana
  j=distribute (u->number, k, u->mana);
  if (u2) u2->mana += u->mana-j;
  u->mana = j;

  u->number = k;

  if (u2)
    {
      u2->number += n;

      addevent (u->faction, 
		translate (ST_GIVE_MEN, u->faction->language,
			   unitid (u), n, unitid2 (u2)));
      if (u->faction != u2->faction)
	addevent (u2->faction, 
		  translate (ST_GIVE_MEN, u2->faction->language,
			     unitid (u), n, unitid2 (u2)));
    }
  else
    {
      if (getunitpeasants)
        {
          r->peasants += n;
          if (k)
	    addevent (u->faction, 
		      translate (ST_GIVE_MEN_TO_PEASANTS, 
				 u->faction->language, unitid (u)));
          else
	    addevent (u->faction, 
		      translate (ST_GIVE_ALL_MEN_TO_PEASANTS, 
				 u->faction->language, unitid (u)));
        }
      else
        {
          if (k)
	    addevent (u->faction, 
		      translate (ST_GIVE_MEN_TO_NOTHING, 
				 u->faction->language, unitid (u), n));
          else
	    addevent (u->faction, 
		      translate (ST_GIVE_ALL_MEN_TO_NOTHING, 
				 u->faction->language, unitid (u), n));
        }
    }
}

void
givesilver (int n, unit * u, region * r, unit * u2, strlist * S)
{
  if (n > u->money)
    n = u->money;

  if (n == 0)
    {
      mistake2 (u, S, strings[ST_NO_MONEY][u->faction->language]);
      return;
    }

  u->money -= n;

  if (u2)
    {
      u2->money += n;

      sprintf (buf, "%s zahlte $%d an ", unitid (u), n);
      scat (unitid (u2));
      scat (".");
      if (u->faction != u2->faction)
	{
	  addcommerce (u->faction, buf);
	  addcommerce (u2->faction, buf);
	}
      else
	addevent (u->faction, buf);
    }
  else
    {
      if (getunitpeasants)
        {
          r->money += n;

          sprintf (buf, "%s verteilt $%d unter den Bauern.", unitid (u), n);
        }
      else
        sprintf (buf, "%s wirft $%d weg.", unitid (u), n);
      addcommerce (u->faction, buf);
    }
}

void
giveall (unit * u, region * r, unit * u2, strlist * S)
{
  int i;
  if (u->number)
    givemen (u->number, u, r, u2, S);
  if (u->money)
    givesilver (u->money, u, r, u2, S);
  for (i = 0; i != MAXITEMS; i++)
    if (u->items[i])
      giveitem (u->items[i], i, u, r, u2, S);
  /*Hier wird die Einheit gleich plattgemacht, denn sie ist ja sicher weg.*/
  //remove_unit(u,r,0);
  /*TPWORK010613: Das geht nicht! u wird zurueckgegeben und in giving in 
  einer Schleife verwendet -- die kommt schwer durcheinander, wenn ihr auf 
  einmal eine Einheit abhanden gekommen ist?!?*/
}

void
giveunit (unit * u, region * r, unit * u2, strlist * S)
{
  if (!u2)
    {
      giveall (u, r, u2, S);
      return;
    }
      
  if (u->faction == u2->faction)
    return;
// Alte Beschraenkung: max. MAXMAGICIANS Magier
/*  if (u->skills[SK_MAGIC]
      && magicians (u2->faction) + u->number > MAXMAGICIANS)
    {
      sprintf (buf, "Es kann maximal %d Magier pro Partei geben", MAXMAGICIANS);
      mistake2 (u, S, buf);
      return;
    }
*/  
  if (!can_make_new_unit(u2->faction)) 
    {
      sprintf (buf, "Die Zielpartei kann keine zusaetlichen Einheiten mehr verwalten");
      mistake2 (u, S, buf);
      return;
    }

  if (is_magician(u) && !can_give_magicians(u2->faction,u,0,u->number)) 
    {
      sprintf (buf, "Die Zielpartei kann keine zusaetlichen Magier mehr verwalten");
      mistake2 (u, S, buf);
      return;
    }

  
  sprintf (buf, "%s wechselt zu %s",
           unitid (u), factionid (u2->faction));
  addevent (u->faction, buf);
  addevent (u2->faction, buf);
  /*Effekte auf die Einheiten und Magierzahl der Partei*/
  u->faction->nunits--;
  u2->faction->nunits++;
  if (is_magician(u)) 
  {
  	u->faction->nmagicians 	-= u->number;
  	u2->faction->nmagicians	+= u->number;
  }
  u->faction = u2->faction;
}

/* ------------------------------------------------------------- */

/* Diese Funktion wird auch verwendet, um das fuer den Lebensunterhalt
   noetige Geld (meist $10) einzusammeln.  Dies ist der side-effect.
   Der return Wert wird in der naechsten Funktion collectsomemoney
   gebraucht, um den Text der Message zu machen.  */
int
collectmoney (region * r, unit * collector, int n)
{
 int i, sum=0;
  unit *u;

  /* n gibt an, wieviel Silber noch eingesammelt werden soll.  */

  for (u = r->units; u && n >= 0; u = u->next)
    if (u->faction == collector->faction &&
        u != collector &&
        u->type == U_MAN &&    /* nur menschen geben geld her */
        can_contact (r, collector, u))
      {
        i = min (u->money, n);
	/* u->money wird immer 0, ausser bei der letzten Einheit,
           welche nicht mehr alles hergeben muss.  */
        u->money -= i;
        sum += i;
        n -= i;
      }
  collector->money += sum;
  return sum;
}

void
collectsomemoney (region * r, unit * collector, int n)
{
  sprintf (buf, "%s sammelte $%d ein.", unitid (collector), collectmoney (r, collector, n));
  addevent (collector->faction, buf);
}

void
collectallmoney (region * r, unit *collector)
{
  int sum=0;
  unit *u;

  for (u = r->units; u; u = u->next)
    if (u->faction == collector->faction && 
	u != collector &&
	u->type == U_MAN &&    /* nur menschen geben geld her */
	can_contact (r, collector, u))
      {
	sum += u->money;
	u->money = 0;
      }
  collector->money += sum;
  sprintf (buf, "%s sammelte $%d ein.", unitid (collector), sum);
  addevent (collector->faction, buf);
}

/* ------------------------------------------------------------- */

void
giving (void)
{
  region *r;
  unit *u, *u2;
  strlist *S;
  char *s;
  char new_order[NAMESIZE];
  char locbuf[MAXLINE]; //local buffer
  int i, n, delivering = 0, alias_used = 0;

  puts ("- sammeln...");  
/* Sammel und Gib entkoppelt, d.h. eine eigene Schleife fuer
   Sammel vor GIB, sonst ist Sammel sehr unberechenbar. TP 1.10.99 */

  for (r = regions; r; r = r->next)
    for (u = r->units; u; u = u->next)
      for (S = u->orders; S; S = S->next)
	switch (igetkeyword (S->s)) {
	  case K_COLLECT:
	    
	    s = getstr ();
	    if (findparam (s) == P_ALL)
	      {
		collectallmoney (r, u);
		continue;
	      }
	    
	    n = atoip (s);
	    if (n)
	      {
		collectsomemoney (r, u, n);
		continue;
	      }
	    
	    mistake2 (u, S, "Die Silbermenge wurde nicht angegeben");
	    break;
        }	    

  puts ("- geben...");

  /* GIB vor STIRB!  */
  for (r = regions; r; r = r->next) {
    for (u = r->units; u; u = u->next) {
      for (S = u->orders; S; S = S->next) {
        delivering = 0;
        alias_used = 0;
	switch (igetkeyword (S->s))
	  {
	    
	  case K_DELIVER:
// LIEFERE NICHT wurde schon in setdefaults (laws.c) erledigt und geloescht
// Neuer Default, falls TEMP Einheiten verwendet werden (alias_used).
            delivering = 1;
            strcpy (new_order, keywords[K_DELIVER]); 
	  case K_GIVE:
	    
	    /* Drachen, Illusionen etc geben nichts her */
	    if (u->type != U_MAN)
	      {
		mistake2 (u, S, translate (ST_MONSTERS_DONT_GIVE, u->faction->language,
					   strings[typenames[1][u->type]][u->faction->language]));
		continue;
	      }

	    u2 = getunit (r, u);
	    if (u==u2)
	      {
		mistake2 (u, S, "Das ist dieselbe Einheit");
		continue;
	      }

	    /* Ziel auffinden */
	    if (!u2 && !getunit0 && !getunitpeasants)
	      {
		mistake2 (u, S, "Die Einheit wurde nicht gefunden");
		continue;
	      }

	    /* Drachen, Illusionen etc nehmen auch nichts an (va. keine neuen Leute!).  */
	    if (u2 && u2->type != U_MAN)
	      {
		mistake2 (u, S, translate (ST_MONSTERS_DONT_TAKE, u->faction->language,
					   strings[typenames[1][u2->type]][u->faction->language]));
		continue;
	      }

            if (delivering) {
              if (!getunit0 && !getunitpeasants) {
                sprintf (locbuf," %d", u2->no);
              } else {
              	strcpy (locbuf," 0");
              }
              strcat (new_order, locbuf);
            }
            
           // Falls u2 ein alias hat, ist sie neu, und es wurde ein TEMP verwendet
           if (u2->alias) alias_used = 1;

	    s = getstr ();
	    switch (findparam (s))
	      {
	      case P_CONTROL:
		/* KONTROLLE in enter ()! */
		continue;

	      case P_ALL:
		if (u2 && !contacts (r, u2, u, HL_SILVER) && !contacts(r, u2, u, HL_GIVE))
		  {
		    mistake2 (u, S, "Die Einheit hat keinen Kontakt mit uns aufgenommen");
		    continue;
		  }
		/* ALLES: Mit einer anderen Einheit mischen.  */
#ifdef GIVE_DEBUG
 printf("giveall u %d r %s an u2 %d: %s",u->number,r->name,u2->number,S->s);
#endif
		giveall (u, r, u2, S);
		continue;
		  
/*	      case P_SPELLBOOK:
		// ZAUBERBUCH: uebergibt alle Sprueche.
		// Nicht mehr moeglich wg. Magieschulensystem.
		giveallspells (u, u2, S);
		continue;
*/    
	      case P_UNIT:
//		if (u2 && !contacts_person (r, u2, u))
		if (u2 && !(contacts (r, u2, u, HL_GIVE) || contacts_person (r, u2, u)))
		  {
		    mistake2 (u, S, "Die Einheit hat keinen Kontakt mit uns aufgenommen");
		    continue;
		  }
#ifdef GIVE_DEBUG
 printf("giveunit u %d r %s an u2 %d: %s",u->number,r->name,u2->number,S->s);
#endif
                /* EINHEIT: Einheiten als ganzes an eine andere Partei uebergeben.  */
                giveunit (u, r, u2, S);
		continue;
	      } // end of switch (findparam...

	    /* Falls nicht, dann steht in s vielleicht ein Zauberspruch */
/*Zaubersprueche geben ist nicht mehr erlaubt.*/
/*	    i = findspell (s);
	    if (i >= 0)
	      {
		givespell (i, u, u2, S);
		continue;
	      }
*/
	    /* Falls nicht, dann steht darin eine Anzahl */
	    n = atoip (s);
            if (delivering) {
              sprintf (locbuf, " %d", n);
              strcat (new_order, locbuf);
            }
	    /* und nun folgt ein Gegenstand... */
	    s = getstr ();
            if (delivering) {
              sprintf (locbuf, " %s", s);
              strcat (new_order, locbuf);
            }

	  // neuer default-LIEFERE Befehl falls TEMP verwendet wurde
	  // eventuell wird hier auch Schrott reingeschrieben, da die
	  // folgenden Pruefungen nicht in den Default kommen
	  if (delivering && alias_used)
            mstrcpy (&u->thisorder2, new_order);

	    switch (findparam (s))
	      {
		/* PERSONEN */
	      case P_MAN:
	      case P_PERSON:
		if (u2 && !contacts_person (r, u2, u))
		  {
		    mistake2 (u, S, "Die Einheit hat keinen Kontakt mit uns aufgenommen");
		    continue;
		  }
#ifdef GIVE_DEBUG
 printf("givemen u %d r %s an u2 %d: %s",u->number,r->name,u2->number,S->s);
#endif
		givemen (n, u, r, u2, S);
		continue;
		  
		/* SILBER */
	      case P_SILVER:
		if (u2 && !contacts (r, u2, u, HL_SILVER))
		  {
		    mistake2 (u, S, "Die Einheit hat keinen Kontakt mit uns aufgenommen");
		    continue;
		  }
#ifdef GIVE_DEBUG
 printf("givesilver u %d r %s an u2 %d: %s",u->number,r->name,u2->number,S->s);
#endif
		givesilver (n, u, r, u2, S);
		continue;
	      }  // end of switch
	      
	    /* oder ein allgemeiner Gegenstand */
	    i = finditem (s);
	    if (u2 && !contacts (r, u2, u, HL_GIVE))
	      {
		mistake2 (u, S, "Die Einheit hat keinen Kontakt mit uns aufgenommen");
		continue;
	      }
	    if (i >= 0)
	      {
		giveitem (n, i, u, r, u2, S);
		continue;
	      }
	      
	    /* Wenn es bis hierhin nichts war, dann hat die Einheit so etwas nicht.  */
	    mistake2 (u, S, "So etwas hat die Einheit nicht");
	  } // end of switch
      }
    }
  }
}
