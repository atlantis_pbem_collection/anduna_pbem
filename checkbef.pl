#! /urs/bin/perl
# Dieses Script (C) 2001 von Tim P�pken wird dazu verwendet,
# in der Datei befehle nachzusehen, wer alles drin steht.
# Hierzu werden die Partei-Zeilen gesucht, gefiltert und sortiert.

# Eingabedatei
$bef="<befehle";

#Ausgabedatei
$out=">out.txt";

open (bef,$bef);
open (out,$out);
$zeile=<bef>;
$zaehler=0;
@liste=("* Befehle wurden abgegeben von:\n");
while($zeile)
{
	$uczeile=uc($zeile);
	if($uczeile=~m/^ *PARTEI/i) #case insensitive
	{
#		push(@liste,$zeile);
  		$zeile=~s/^ *PARTEI/PARTEI/i; #Leerzeichen am Anfang plaetten, damit das sortieren klappt
		$zaehler++;
		@liste2=split(/ /,$zeile);
		if (length($liste2[1])<2) {$liste2[1]="0".$liste2[1];}
		$liste2[0]=uc($liste2[0]);
		if ($liste2[2]=~m/\n$/) {chop($liste2[2]);}
		$zeile=$liste2[0]." ".$liste2[1]." ".$liste2[2].",\n";
		push(@liste,$zeile);
	}
	$zeile=<bef>;
}
@test=sort(@liste);
print out "@test";
print out "Das waren $zaehler Befehle.";
die;
